package errorMessages;

import com.google.gson.annotations.Expose;

public class ExcelError extends ErrorMessage {
    public static final int EXCEL_ERROR = 10;
    public static final int EXCEL_ERROR_PRODUCT_NOT_FOUND = 11;
    public static final int EXCEL_ERROR_DISTRIBUTOR_NOT_FOUND = 12;
    public static final int EXCEL_ERROR_DISTRIBUTOR_ALREADY_EXISTS = 13;
    public static final int EXCEL_ERROR_DEALER_NOT_FOUND = 14;
    public static final int EXCEL_ERROR_DEALER_ALREADY_EXISTS = 15;
    public static final int EXCEL_ERROR_OWN_PARENT = 16;

    @Expose private final String line;

    public ExcelError(String line) {
        this(line, EXCEL_ERROR);
    }

    public ExcelError(String line, int error) {
        super(error);
        this.line = line;
    }

    public String getLine() {
        return this.line;
    }

    @Override
    protected void initCodeMessages() {
        super.initCodeMessages();
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR, "Excel error");
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR_PRODUCT_NOT_FOUND, "Product not found");
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR_DISTRIBUTOR_NOT_FOUND, "Distributor not found");
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR_DISTRIBUTOR_ALREADY_EXISTS, "Distributor already exists");
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND, "Dealer not found");
        this.getCodeMessages().put(ExcelError.EXCEL_ERROR_DEALER_ALREADY_EXISTS, "Dealer already exists");
    }
}
