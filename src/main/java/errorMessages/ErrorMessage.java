package errorMessages;

import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

public class ErrorMessage {
    @Expose
    private String message;
    @Expose
    private int code = 0;
    private Map<Integer, String> codeMessages = new HashMap<>();

    public static final int NOT_AUTHORIZED = 1;
    public static final int INCORRECT_PARAMETERS = 2;
    public static final int USER_DOES_NOT_EXIST = 3;
    public static final int USER_ALREADY_EXISTS = 4;
    public static final int RECORD_NOT_FOUND = 5;
    public static final int RECORD_EXISTS = 6;
    public static final int PERMISSIONS_DENIED = 7;
    public static final int SERVER_ERROR = 8;
    public static final int INCORRECT_PASSWORD = 9;

    public ErrorMessage(int code) {
        this.code = code;
        this.initCodeMessages();
    }

    public String getMessage() {
        return this.message;
    }
    public int getCode() {
        return this.code;
    }
    public Map<Integer, String> getCodeMessages() {
        return this.codeMessages;
    }

    protected void initCodeMessages() {
        this.codeMessages.put(1, "Not authorized");
        this.codeMessages.put(2, "Incorrect parameters");
        this.codeMessages.put(3, "User does not exist");
        this.codeMessages.put(4, "User already exists");
        this.codeMessages.put(5, "Record not found");
        this.codeMessages.put(6, "Record already exists");
        this.codeMessages.put(7, "Permissions denied");
        this.codeMessages.put(8, "Server error");
        this.codeMessages.put(9, "Incorrect password");

        this.message = this.codeMessages.get(this.code);
    }
}
