package servlets.dealerProductOrder;

import database.domains.DealerProductOrder;
import database.services.DealerProductOrderService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AcceptServlet extends HttpServlet {
    public static final String URL = "/accept";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerProductOrderService dealerProductOrderService = new DealerProductOrderService();
        String id = request.getParameter("id");

        dealerProductOrderService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            DealerProductOrder dealerProductOrder = dealerProductOrderService.getById(Long.parseLong(id));

            if (dealerProductOrder == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            if (dealerProductOrderService.changeStatus(dealerProductOrder, Constants.STATUSES.ACCEPTED.getValue())) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(dealerProductOrder));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
