package servlets.dealerProductOrder;

import database.domains.DealerProductOrder;
import database.domains.ProductDescription;
import database.services.DealerProductOrderService;
import database.services.ProductDescriptionService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import servlets.ServletWithAuthorTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DealerProductOrderServlet extends ServletWithAuthorTemplate<DealerProductOrder, DealerProductOrderService> {
    public DealerProductOrderServlet() {
        super(DealerProductOrderService.class, DealerProductOrder.class);
    }

    @SuppressWarnings("unchecked")
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        try {
            DealerProductOrderService service = new DealerProductOrderService();
            long id = Long.parseLong(request.getParameter("id"));

            service.setSessionId(request.getHeader("Session"));
            DealerProductOrder dealerProductOrder = service.getById(id);

            if (dealerProductOrder != null) {
                ProductDescriptionService productDescriptionService = new ProductDescriptionService();
                productDescriptionService.setSession(service.getSession());
                for (ProductDescription item : dealerProductOrder.getProducts()) {
                    productDescriptionService.delete(item);
                }
                if (service.delete(dealerProductOrder))
                    response.setStatus(HttpServletResponse.SC_OK);
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
