package servlets.distributor;

import database.domains.Distributor;
import database.domains.Person;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DistributorServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final PersonService<Distributor> distributorService = new PersonService<>(Distributor.class);
        String id = request.getParameter("id");

        distributorService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            Map<String, Object> result = new HashMap<>();

            result.put("data", distributorService.getAll(request.getParameterMap()));
            result.put("totalRecords", "0");

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            try {
                Distributor distributor = distributorService.getById(Long.parseLong(id));
                if (distributor == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(distributor));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        final PersonService<Distributor> distributorService = new PersonService<>(Distributor.class);
        String login = request.getParameter("login");
        String warehouseName = request.getParameter("warehouseName");

        distributorService.setSessionId(request.getHeader("Session"));
        personService.setSession(distributorService.getSession());

        if (login == null || login.trim().isEmpty() ||
                warehouseName == null || warehouseName.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        if (personService.existsByLogin(login)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_ALREADY_EXISTS)));
            return;
        }
        Distributor distributor = new Distributor();
        if (distributor.setParameters(request.getParameterMap())) {
            if (distributorService.save(distributor)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(distributor));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doPut(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        final PersonService<Distributor> distributorService = new PersonService<>(Distributor.class);
        String login = request.getParameter("login");
        String warehouseName = request.getParameter("warehouseName");
        String id = request.getParameter("id");

        distributorService.setSessionId(request.getHeader("Session"));
        personService.setSession(distributorService.getSession());

        if (login == null || login.trim().isEmpty() ||
                warehouseName == null || warehouseName.trim().isEmpty() ||
                id == null || id.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        try {
            Person person = distributorService.getByLogin(login);
            Distributor distributor = distributorService.getById(Long.parseLong(id));
            if (person != null && !person.equals(distributor)) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_ALREADY_EXISTS)));
                return;
            }
            if (distributor.setParameters(request.getParameterMap())) {
                if (distributorService.save(distributor)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(distributor));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Distributor> distributorService = new PersonService<>(Distributor.class);
        distributorService.setSessionId(request.getHeader("Session"));

        try {
            long id = Long.parseLong(request.getParameter("id"));
            Distributor distributor= distributorService.getById(id);
            if (distributor == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
            else {
                if (distributorService.delete(distributor)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
