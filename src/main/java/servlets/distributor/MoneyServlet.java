package servlets.distributor;

import database.domains.DistributorMoney;
import database.services.DistributorMoneyService;
import servlets.ServletWithAuthorTemplate;

public class MoneyServlet extends ServletWithAuthorTemplate<DistributorMoney, DistributorMoneyService> {
    public static final String URL = "/money";

    public MoneyServlet() {
        super(DistributorMoneyService.class, DistributorMoney.class);
    }
}
