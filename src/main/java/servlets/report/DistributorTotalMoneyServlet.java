package servlets.report;

import database.domains.DistributorMoney;
import database.domains.Person;
import database.services.DistributorMoneyService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DistributorTotalMoneyServlet extends HttpServlet {
    public static final String URL = "/distributor-total-money";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        Person currentPerson = AuthenticationServiceSingleton.getInstance()
                .getCurrentPerson(request.getHeader("Session"));

        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        String dateStartStr = request.getParameter("dateStart");
        String dateEndStr = request.getParameter("dateEnd");

        if (dateStartStr == null || dateStartStr.trim().isEmpty() ||
                dateEndStr == null || dateEndStr.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        long dateStart = 0;
        long dateEnd = 0;

        try {
            dateStart = DateHelper.parse(dateStartStr);
            dateEnd = DateHelper.parse(dateEndStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        Map<String, Double> result = new HashMap<>();
        DistributorMoneyService distributorMoneyService = new DistributorMoneyService();
        distributorMoneyService.setSessionId(Constants.ALLOW_READ);

        Filters filters = new Filters();

        filters.and(new Filter("datetime", ComparisonOperator.GREATER_THAN_EQUAL, dateStart));
        filters.and(new Filter("datetime", ComparisonOperator.LESS_THAN_EQUAL, dateEnd));

        Collection<DistributorMoney> distributorMoneyResult = distributorMoneyService.getAll(
                filters, new SortOrder().add("datetime"),
                -1, 1000, 1);

        distributorMoneyResult.forEach(distributorMoney -> {
            result.put(distributorMoney.getType(),
                    result.getOrDefault(distributorMoney.getType(), 0.) + distributorMoney.getMoney());
        });

        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(new GsonHelper(result));
    }
}
