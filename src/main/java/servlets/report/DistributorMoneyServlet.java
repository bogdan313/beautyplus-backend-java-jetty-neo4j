package servlets.report;

import database.DBServiceSingleton;
import database.domains.*;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DistributorMoneyServlet extends HttpServlet {
    public static final String URL = "/distributor-money";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorService distributorService = new DistributorService();

        distributorService.setSessionId(request.getHeader("Session"));

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(distributorService.getSessionId());
        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                        !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                        !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        final long dateStart;
        final long dateEnd;

        String dateStartString = request.getParameter("dateStart");
        String dateEndString = request.getParameter("dateEnd");

        if (dateStartString == null || dateStartString.trim().isEmpty() ||
                dateEndString == null || dateEndString.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        int pageNumber = 1;
        try {
            pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
            if (pageNumber < -1) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

                return;
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            dateStart = DateHelper.parse(dateStartString);
            dateEnd = DateHelper.parse(dateEndString);
        }
        catch (ParseException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        String distributorId = request.getParameter("distributor");
        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            distributorId = currentPerson.getId().toString();
        }

        Map<String, Map<String, Double>> result = new HashMap<>();

        if (distributorId == null || distributorId.trim().isEmpty()) {
            distributorService.getAll(pageNumber)
                    .forEach(distributor -> {
                        Map<String, Double> distributorResult = new HashMap<>();
                        distributorResult.put("incoming", 0.);
                        distributorResult.put("outgoing", 0.);

                        distributor
                                .getWarehouseProductTransfers()
                                .stream()
                                .filter(warehouseProductTransfer -> this.check(warehouseProductTransfer, dateStart, dateEnd))
                                .forEach(warehouseProductTransfer -> {
                                    warehouseProductTransfer.getProducts()
                                            .forEach(productDescription -> {
                                                distributorResult.put("outgoing", distributorResult.getOrDefault("outgoing", 0.) + productDescription.getDealerPrice() * productDescription.getQuantity());
                                            });
                                });

                        distributor
                                .getDistributorMoney()
                                .stream()
                                .filter(distributorMoney -> this.check(distributorMoney, dateStart, dateEnd))
                                .forEach(distributorMoney -> {
                                    distributorResult.put("incoming", distributorResult.getOrDefault("incoming", 0.) + distributorMoney.getMoney());
                                });

                        result.put(distributor.getFormatted(), distributorResult);
                    });
        }
        else {
            try {
                Distributor distributor = distributorService.getById(Long.parseLong(distributorId.trim()), 3);
                if (distributor == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                    return;
                }

                Map<String, Double> distributorResult = new HashMap<>();
                distributorResult.put("incoming", 0.);
                distributorResult.put("outgoing", 0.);

                distributor
                        .getWarehouseProductTransfers()
                        .stream()
                        .filter(warehouseProductTransfer -> this.check(warehouseProductTransfer, dateStart, dateEnd))
                        .forEach(warehouseProductTransfer -> {
                            warehouseProductTransfer.getProducts()
                                    .forEach(productDescription -> {
                                        distributorResult.put("outgoing", distributorResult.getOrDefault("outgoing", 0.) + productDescription.getDealerPrice() * productDescription.getQuantity());
                                    });
                        });

                distributor
                        .getDistributorMoney()
                        .stream()
                        .filter(distributorMoney -> this.check(distributorMoney, dateStart, dateEnd))
                        .forEach(distributorMoney -> {
                            distributorResult.put("incoming", distributorResult.getOrDefault("incoming", 0.) + distributorMoney.getMoney());
                        });

                result.put(distributor.getFormatted(), distributorResult);
            }
            catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }

        if (pageNumber != -1) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(result));
        }
        else {
            Workbook workbook = new HSSFWorkbook();
            Sheet distributorSheet = workbook.createSheet("Дистрибьюторы");
            int rowNum = 0;
            int cellNum = 0;

            Row headerRow = distributorSheet.createRow(rowNum++);

            Cell distributorCell = headerRow.createCell(cellNum++);
            distributorCell.setCellType(CellType.STRING);
            distributorCell.setCellValue("Дистрибьютор");

            Cell incomingCell = headerRow.createCell(cellNum++);
            incomingCell.setCellType(CellType.STRING);
            incomingCell.setCellValue("Поступило средств");

            Cell outgoingCell = headerRow.createCell(cellNum++);
            outgoingCell.setCellType(CellType.STRING);
            outgoingCell.setCellValue("Отгружено товара");

            Cell differenceCell = headerRow.createCell(cellNum);
            differenceCell.setCellType(CellType.STRING);
            differenceCell.setCellValue("Остаток");

            for (Map.Entry<String, Map<String, Double>> entry : result.entrySet()) {
                cellNum = 0;

                Row currentRow = distributorSheet.createRow(rowNum++);

                Cell currentDistributorCell = currentRow.createCell(cellNum++);
                currentDistributorCell.setCellType(CellType.STRING);
                currentDistributorCell.setCellValue(entry.getKey());

                Cell currentIncomingCell = currentRow.createCell(cellNum++);
                currentIncomingCell.setCellType(CellType.STRING);
                currentIncomingCell.setCellValue(entry.getValue().getOrDefault("incoming", 0.));

                Cell currentOutgoingCell = currentRow.createCell(cellNum++);
                currentOutgoingCell.setCellType(CellType.STRING);
                currentOutgoingCell.setCellValue(entry.getValue().getOrDefault("outgoing", 0.));

                Cell currentDifferenceCell = currentRow.createCell(cellNum);
                currentDifferenceCell.setCellType(CellType.STRING);
                currentDifferenceCell.setCellValue(entry.getValue().getOrDefault("incoming", 0.) - entry.getValue().getOrDefault("outgoing", 0.));
            }

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(tmpResult));
            }
            catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }

    private boolean check(WarehouseProductTransfer warehouseProductTransfer, long dateStart, long dateEnd) {
        return warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                warehouseProductTransfer.getDatetime() >= dateStart &&
                warehouseProductTransfer.getDatetime() <= dateEnd;
    }

    private boolean check(DistributorMoney distributorMoney, long dateStart, long dateEnd) {
        return distributorMoney.getDatetime() >= dateStart &&
                distributorMoney.getDatetime() <= dateEnd;
    }
}
