package servlets.report;

import database.DBServiceSingleton;
import database.domains.Dealer;
import database.domains.DealerProductOrder;
import database.domains.Person;
import database.services.DealerService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import helpers.ReportExcelHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class DealerProductOrderServlet extends HttpServlet {
    public static final String URL = "/dealer-product-order";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        String strId = request.getParameter("dealer");
        Map<String, Map<String, Long>> dealersResult = new HashMap<>();
        Map<String, Map<String, Double>> productsResult = new HashMap<>();
        Collection<DealerProductOrder> rowsResult = new ArrayList<>();
        int pageNumber = 1;
        final long dateStart;
        final long dateEnd;

        dealerService.setSessionId(request.getHeader("Session"));
        dealerService.clearSession();

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
        }
        catch (ParseException |NullPointerException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        if (strId == null || strId.trim().isEmpty()) {
            try {
                pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
                if (pageNumber < -1) throw new NumberFormatException();

                Collection<Dealer> dealers = dealerService.getAll(new SortOrder().add("fullName"), pageNumber, 3);
                dealers.forEach(dealer -> {
                    if (!dealersResult.containsKey(dealer.getFormatted()))
                        dealersResult.put(dealer.getFormatted(), new HashMap<>());
                    dealer.getOrders().stream()
                            .filter(dealerProductOrder -> this.checkAccess(dealerService.getSessionId(), dealerProductOrder, dateStart, dateEnd))
                            .forEach(dealerProductOrder -> {
                                rowsResult.add(dealerProductOrder);

                                dealerProductOrder.getProducts().forEach(productDescription -> {
                                    if (!productsResult.containsKey(productDescription.getProduct().getFormatted()))
                                        productsResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                                    productsResult.get(productDescription.getProduct().getFormatted()).put("quantity",
                                            productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("quantity", 0D) + productDescription.getQuantity());
                                    productsResult.get(productDescription.getProduct().getFormatted()).put("price",
                                            productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("price", 0D) + productDescription.getDealerPrice() * productDescription.getQuantity());
                                });

                                dealersResult.get(dealer.getFormatted()).put(dealerProductOrder.getStatus(),
                                        dealersResult.get(dealer.getFormatted()).getOrDefault(dealerProductOrder.getStatus(), 0L) + 1);
                            });
                });
            }
            catch (NumberFormatException|NullPointerException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }
        else {
            try {
                Dealer dealer = dealerService.getById(Long.parseLong(strId), 3);
                HashMap<String, Long> tmpResult = new HashMap<>();
                dealer.getOrders().stream()
                        .filter(dealerProductOrder -> this.checkAccess(dealerService.getSessionId(), dealerProductOrder, dateStart, dateEnd))
                        .forEach(dealerProductOrder -> {
                            rowsResult.add(dealerProductOrder);

                            dealerProductOrder.getProducts().forEach(productDescription -> {
                                if (!productsResult.containsKey(productDescription.getProduct().getFormatted()))
                                    productsResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                                productsResult.get(productDescription.getProduct().getFormatted()).put("quantity",
                                        productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("quantity", 0D) + productDescription.getQuantity());
                                productsResult.get(productDescription.getProduct().getFormatted()).put("price",
                                        productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("price", 0D) + productDescription.getDealerPrice() * productDescription.getQuantity());
                            });

                            tmpResult.put(dealerProductOrder.getStatus(), tmpResult.getOrDefault(dealerProductOrder.getStatus(), 0L) + 1);
                        });
                dealersResult.put(dealer.getFormatted(), tmpResult);
            }
            catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }

        response.setStatus(HttpServletResponse.SC_OK);

        if (pageNumber != -1) {
            Map<String, Object> result = new HashMap<>();

            result.put("dealers", dealersResult);
            result.put("rows", rowsResult);
            result.put("products", productsResult);

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            Workbook workbook = new HSSFWorkbook();

            Sheet dealersSheet = workbook.createSheet("Дилеры");
            ReportExcelHelper.addPersons(dealersSheet, dealersResult, request.getParameter("dateStart"), request.getParameter("dateEnd"));

            Sheet productsSheet = workbook.createSheet("Товары");
            ReportExcelHelper.addProducts(productsSheet, productsResult, request.getParameter("dateStart"), request.getParameter("dateEnd"));

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.getWriter().println(new GsonHelper(tmpResult));
            }
            catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }

    private boolean checkAccess(String sessionId, DealerProductOrder dealerProductOrder, long dateStart, long dateEnd) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        boolean personAccess = false;
        if (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) personAccess = true;
        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) &&
                dealerProductOrder.getDistributor().equals(currentPerson))
            personAccess = true;
        if (currentPerson.getRole().equals(Person.ROLE.DEALER.getValue()) &&
                dealerProductOrder.getDealer().equals(currentPerson))
            personAccess = true;

        return dealerProductOrder.getDatetime() >= dateStart &&
                dealerProductOrder.getDatetime() <= dateEnd &&
                personAccess;
    }
}