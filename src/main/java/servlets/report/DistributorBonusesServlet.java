package servlets.report;

import database.DBServiceSingleton;
import database.domains.Distributor;
import database.domains.Person;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class DistributorBonusesServlet extends HttpServlet {
    public static final String URL = "/distributor-bonuses";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final DistributorService distributorService = new DistributorService();
        Map<String, Map<String, Double>> result = new HashMap<>();
        final long dateStart;
        final long dateEnd;
        int pageNumber = 1;
        String distributorId = request.getParameter("distributor");

        distributorService.setSessionId(request.getHeader("Session"));
        distributorService.clearSession();

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(distributorService.getSessionId());
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            distributorId = currentPerson.getId().toString();
        }

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
        }
        catch (ParseException|NullPointerException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        try {
            pageNumber = Integer.parseInt(request.getParameter("pageNumber"));

            if (pageNumber < -1) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }
        catch (NumberFormatException|NullPointerException e) {}

        if (distributorId == null || distributorId.trim().isEmpty()) {
            distributorService.getAll(new SortOrder().add("fullName"), pageNumber, 3)
                    .forEach(distributor -> {
                        distributor.setBonuses(0.);

                        distributor.getWarehouseProductTransfers().stream()
                                .filter(warehouseProductTransfer ->
                                        warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                                                warehouseProductTransfer.getDatetime() >= dateStart &&
                                                warehouseProductTransfer.getDatetime() <= dateEnd
                                )
                                .forEach(warehouseProductTransfer -> {
                                    warehouseProductTransfer.getProducts().forEach(productDescription -> {
                                        distributor.addBonuses(productDescription.getBonuses() * productDescription.getQuantity());
                                    });
                                });

                        Map<String, Double> tmpResult = new HashMap<>();
                        tmpResult.put("bonuses", distributor.getBonuses());
                        tmpResult.put("salary", distributor.getBonuses() * 0.15 * Constants.BONUS_PRICE);

                        result.put(distributor.getFormatted(), tmpResult);
                    });
        }
        else {
            try {
                Distributor distributor = distributorService.getById(Long.parseLong(distributorId), 3);

                distributor.setBonuses(0.);

                distributor.getWarehouseProductTransfers().stream()
                        .filter(warehouseProductTransfer ->
                                warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                                        warehouseProductTransfer.getDatetime() >= dateStart &&
                                        warehouseProductTransfer.getDatetime() <= dateEnd
                        )
                        .forEach(warehouseProductTransfer -> {
                            warehouseProductTransfer.getProducts().forEach(productDescription -> {
                                distributor.addBonuses(productDescription.getBonuses() * productDescription.getQuantity());
                            });
                        });

                Map<String, Double> tmpResult = new HashMap<>();
                tmpResult.put("bonuses", distributor.getBonuses());
                tmpResult.put("salary", distributor.getBonuses() * 0.15 * Constants.BONUS_PRICE);

                result.put(distributor.getFormatted(), tmpResult);
            }
            catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }

        if (pageNumber != -1) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(result));
        }
        else {
            Workbook workbook = new HSSFWorkbook();
            Sheet distributorsSheet = workbook.createSheet();

            int rowNum = 0;
            int cellNum = 0;

            Row titleRow = distributorsSheet.createRow(rowNum++);

            Cell titleCell = titleRow.createCell(1);
            titleCell.setCellType(CellType.STRING);
            titleCell.setCellValue(String.format("Отчёт по баллам и зарплате за период с %s по %s",
                    request.getParameter("dateStart"), request.getParameter("dateEnd")));

            Row headerRow = distributorsSheet.createRow(rowNum++);

            Cell distributorCell = headerRow.createCell(cellNum++);
            distributorCell.setCellType(CellType.STRING);
            distributorCell.setCellValue("Дистрибьютор");

            Cell bonusesCell = headerRow.createCell(cellNum++);
            bonusesCell.setCellType(CellType.STRING);
            bonusesCell.setCellValue("Бонусы");

            Cell salaryCell = headerRow.createCell(cellNum);
            salaryCell.setCellType(CellType.STRING);
            salaryCell.setCellValue("Зарплата");

            for (Map.Entry<String, Map<String, Double>> entry : result.entrySet()) {
                cellNum = 0;

                Row currentRow = distributorsSheet.createRow(rowNum++);

                Cell tmpDistributorCell = currentRow.createCell(cellNum++);
                tmpDistributorCell.setCellType(CellType.STRING);
                tmpDistributorCell.setCellValue(entry.getKey());

                Cell tmpBonusesCell = currentRow.createCell(cellNum++);
                tmpBonusesCell.setCellType(CellType.NUMERIC);
                tmpBonusesCell.setCellValue(entry.getValue().getOrDefault("bonuses", 0.));

                Cell tmpSalaryCell = currentRow.createCell(cellNum);
                tmpSalaryCell.setCellType(CellType.NUMERIC);
                tmpSalaryCell.setCellValue(entry.getValue().getOrDefault("salary", 0.));
            }

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(tmpResult));
            }
            catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }
}
