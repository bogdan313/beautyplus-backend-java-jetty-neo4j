package servlets.report;

import database.DBServiceSingleton;
import database.domains.Distributor;
import database.domains.Person;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DistributorProductRemainderServlet extends HttpServlet {
    public static final String URL = "/distributor-product-remainder";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorService distributorService = new DistributorService();
        String distributorId = request.getParameter("distributor");

        distributorService.setSessionId(request.getHeader("Session"));
        distributorService.clearSession();


        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(distributorService.getSessionId());
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            distributorId = currentPerson.getId().toString();
        }

        if (distributorId == null || distributorId.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            Distributor distributor = distributorService.getById(Long.parseLong(distributorId.trim()), 3);

            if (distributor == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            Map<String, Map<String, Double>> result = new HashMap<>();

            distributor
                    .getWarehouseProductTransfers()
                    .stream()
                    .filter(warehouseProductTransfer -> warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()))
                    .forEach(warehouseProductTransfer -> {
                        warehouseProductTransfer
                                .getProducts()
                                .forEach(productDescription -> {
                                    if (!result.containsKey(productDescription.getProduct().getFormatted()))
                                        result.put(productDescription.getProduct().getFormatted(), new HashMap<>());

                                    Map<String, Double> distributorProduct = result.get(productDescription.getProduct().getFormatted());
                                    distributorProduct.put("incoming", distributorProduct.getOrDefault("incoming", 0.) + productDescription.getQuantity());
                                });
                    });

            distributor
                    .getTransfers()
                    .stream()
                    .filter(distributorProductTransfer -> distributorProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()))
                    .forEach(distributorProductTransfer -> {
                        distributorProductTransfer
                                .getProducts()
                                .forEach(productDescription -> {
                                    if (!result.containsKey(productDescription.getProduct().getFormatted()))
                                        result.put(productDescription.getProduct().getFormatted(), new HashMap<>());

                                    Map<String, Double> distributorProduct = result.get(productDescription.getProduct().getFormatted());
                                    distributorProduct.put("outgoing", distributorProduct.getOrDefault("outgoing", 0.) + productDescription.getQuantity());
                                });
                    });

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(result));
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
