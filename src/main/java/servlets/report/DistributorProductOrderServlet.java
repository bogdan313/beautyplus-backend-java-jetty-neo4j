package servlets.report;

import database.DBServiceSingleton;
import database.domains.Distributor;
import database.domains.DistributorProductOrder;
import database.domains.Person;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import helpers.ReportExcelHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class DistributorProductOrderServlet extends HttpServlet {
    public static final String URL = "/distributor-product-order";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorService distributorService = new DistributorService();
        String strId = request.getParameter("distributor");
        Map<String, Map<String, Long>> distributorsResult = new HashMap<>();
        Map<String, Map<String, Double>> productsResult = new HashMap<>();
        Collection<DistributorProductOrder> rowsResult = new ArrayList<>();
        int pageNumber = 1;
        final long dateStart;
        final long dateEnd;

        distributorService.setSessionId(request.getHeader("Session"));
        distributorService.clearSession();

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
        }
        catch (ParseException|NullPointerException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        if (strId == null || strId.trim().isEmpty()) {
            try {
                pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
                if (pageNumber < -1) throw new NumberFormatException();

                Collection<Distributor> distributors = distributorService.getAll(new SortOrder().add("fullName"), pageNumber, 3);
                distributors.forEach(distributor -> {
                    if (!distributorsResult.containsKey(distributor.getFormatted()))
                        distributorsResult.put(distributor.getFormatted(), new HashMap<>());
                    distributor.getOrders().stream()
                            .filter(distributorProductOrder -> this.checkAccess(distributorService.getSessionId(), distributorProductOrder, dateStart, dateEnd))
                            .forEach(distributorProductOrder -> {
                                rowsResult.add(distributorProductOrder);

                                distributorProductOrder.getProducts().forEach(productDescription -> {
                                    if (!productsResult.containsKey(productDescription.getProduct().getFormatted()))
                                        productsResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                                    productsResult.get(productDescription.getProduct().getFormatted()).put("quantity",
                                            productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("quantity", 0D) + productDescription.getQuantity());
                                    productsResult.get(productDescription.getProduct().getFormatted()).put("price",
                                            productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("price", 0D) + productDescription.getDealerPrice() * productDescription.getQuantity());
                                });

                                distributorsResult.get(distributor.getFormatted()).put(distributorProductOrder.getStatus(),
                                        distributorsResult.get(distributor.getFormatted()).getOrDefault(distributorProductOrder.getStatus(), 0L) + 1);
                            });
                });
            }
            catch (NumberFormatException|NullPointerException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }
        else {
            try {
                Distributor distributor = distributorService.getById(Long.parseLong(strId), 3);
                HashMap<String, Long> tmpResult = new HashMap<>();
                distributor.getOrders().stream()
                        .filter(distributorProductOrder -> this.checkAccess(distributorService.getSessionId(), distributorProductOrder, dateStart, dateEnd))
                        .forEach(distributorProductOrder -> {
                            rowsResult.add(distributorProductOrder);

                            distributorProductOrder.getProducts().forEach(productDescription -> {
                                if (!productsResult.containsKey(productDescription.getProduct().getFormatted()))
                                    productsResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                                productsResult.get(productDescription.getProduct().getFormatted()).put("quantity",
                                        productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("quantity", 0D) + productDescription.getQuantity());
                                productsResult.get(productDescription.getProduct().getFormatted()).put("price",
                                        productsResult.get(productDescription.getProduct().getFormatted()).getOrDefault("price", 0D) + productDescription.getDealerPrice() * productDescription.getQuantity());
                            });

                            tmpResult.put(distributorProductOrder.getStatus(), tmpResult.getOrDefault(distributorProductOrder.getStatus(), 0L) + 1);
                        });
                distributorsResult.put(distributor.getFormatted(), tmpResult);
            }
            catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }

        response.setStatus(HttpServletResponse.SC_OK);

        if (pageNumber != -1) {
            Map<String, Object> result = new HashMap<>();

            result.put("distributors", distributorsResult);
            result.put("rows", rowsResult);
            result.put("products", productsResult);

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            Workbook workbook = new HSSFWorkbook();

            Sheet dealersSheet = workbook.createSheet("Дистрибьюторы");
            ReportExcelHelper.addPersons(dealersSheet, distributorsResult, request.getParameter("dateStart"), request.getParameter("dateEnd"));

            Sheet productsSheet = workbook.createSheet("Товары");
            ReportExcelHelper.addProducts(productsSheet, productsResult, request.getParameter("dateStart"), request.getParameter("dateEnd"));

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.getWriter().println(new GsonHelper(tmpResult));
            }
            catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }

    private boolean checkAccess(String sessionId, DistributorProductOrder distributorProductOrder, long dateStart, long dateEnd) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        boolean personAccess = false;
        if (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) personAccess = true;
        if (currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                distributorProductOrder.getWarehouse().equals(currentPerson))
            personAccess = true;
        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) &&
                distributorProductOrder.getDistributor().equals(currentPerson))
            personAccess = true;

        return distributorProductOrder.getDatetime() >= dateStart &&
                distributorProductOrder.getDatetime() <= dateEnd &&
                personAccess;
    }
}
