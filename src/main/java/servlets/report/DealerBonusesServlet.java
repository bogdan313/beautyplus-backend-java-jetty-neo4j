package servlets.report;

import database.domains.Dealer;
import database.domains.Person;
import database.services.DealerService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class DealerBonusesServlet extends HttpServlet {
    public static final String URL = "/dealer-bonuses";
    private final double MIN_BONUSES = 50;

    private static final Map<String, String> STATUSES = new HashMap<String, String>() {{
        put(Constants.USER_STATUS.DEALER_1.getValue(), "Дилер");
        put(Constants.USER_STATUS.DEALER_2.getValue(), "Дилер");
        put(Constants.USER_STATUS.DEALER_MANAGER.getValue(), "Менеджер");
        put(Constants.USER_STATUS.DEALER_SILVER_MANAGER.getValue(), "Серебряный менеджер");
        put(Constants.USER_STATUS.DEALER_GOLD_MANAGER.getValue(), "Золотой менеджер");
        put(Constants.USER_STATUS.DEALER_DIAMOND_MANAGER.getValue(), "Бриллиантовый менеджер");
        put(Constants.USER_STATUS.DEALER_DIRECTOR.getValue(), "Директор");
        put(Constants.USER_STATUS.DEALER_SILVER_DIRECTOR.getValue(), "Директор 'Серебро'");
        put(Constants.USER_STATUS.DEALER_GOLD_DIRECTOR.getValue(), "Директор 'Золото'");
        put(Constants.USER_STATUS.DEALER_SAPPHIRE_DIRECTOR.getValue(), "Директор 'Сапфир'");
        put(Constants.USER_STATUS.DEALER_DIAMOND_DIRECTOR.getValue(), "Директор 'Бриллиант'");
    }};

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        final Map<Long, Map<String, Object>> result = new ConcurrentHashMap<>();
        final long dateStart;
        final long dateEnd;
        int pageNumber = 1;
        String dealerId = request.getParameter("dealer");

        dealerService.setSessionId(request.getHeader("Session"));
        dealerService.clearSession();

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(dealerService.getSessionId());
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DEALER.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        if (currentPerson.getRole().equals(Person.ROLE.DEALER.getValue())) {
            dealerId = currentPerson.getId().toString();
        }

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
        } catch (ParseException | NullPointerException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        try {
            pageNumber = Integer.parseInt(request.getParameter("pageNumber"));

            if (pageNumber < -1) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        } catch (NumberFormatException | NullPointerException e) {
        }

        if (dealerId == null || dealerId.trim().isEmpty()) {
            if (pageNumber == -1) {
                dealerService.getAll(-1, 2)
                        .forEach(dealer -> {
                            dealer.setBonuses(0.);
                            dealer.setGroupBonuses(0.);
                            dealer.setStatus("");
                            dealer.setMoney(0.);
                            dealer
                                    .getTransfers()
                                    .stream()
                                    .filter(distributorProductTransfer ->
                                            distributorProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                                                    distributorProductTransfer.getDatetime() >= dateStart &&
                                                    distributorProductTransfer.getDatetime() <= dateEnd
                                    )
                                    .forEach(distributorProductTransfer -> {
                                        distributorProductTransfer.getProducts().forEach(productDescription -> {
                                            dealer.addBonuses(productDescription.getBonuses() * productDescription.getQuantity());
                                        });
                                    });

                            Map<String, Object> tmpResult = new HashMap<>();
                            tmpResult.put("id", dealer.getId());
                            tmpResult.put("code", dealer.getCode());
                            tmpResult.put("fullName", dealer.getFullName());
                            tmpResult.put("parent", dealer.getParentDealer() != null ? dealer.getParentDealer().getCode() : "");
                            tmpResult.put("parent_id", dealer.getParentDealer() != null ? dealer.getParentDealer().getId() : "-1");
                            tmpResult.put("personal", dealer.getBonuses());
                            tmpResult.put("group", 0);
                            tmpResult.put("total", 0);
                            tmpResult.put("status", "");
                            tmpResult.put("level", 0);
                            tmpResult.put("salary", 0);
                            tmpResult.put("calculated_group", false);

                            result.put(dealer.getId(), tmpResult);
                        });
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        } else {
            try {
                this.addAllSubDealersToMap(result, dealerService, Long.parseLong(dealerId), dateStart, dateEnd);
            } catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
        }

        this.makeCompression(result);

        for (Map<String, Object> dealer : result.values()) {
            this.calculateGroupBonuses(result, this.getId(dealer));
            this.calculateStatus(result, this.getId(dealer));
            this.calculateSalary(result, this.getId(dealer));

            dealer.put("total", this.getBonuses(dealer) + this.getGroupBonuses(dealer));
            dealer.put("level", this.calculateLevel(result, this.getId(dealer)));
            dealer.put("salary", this.getBonuses(dealer) >= this.MIN_BONUSES ?
                    Double.parseDouble(dealer.getOrDefault("money", "0").toString()) * Constants.BONUS_PRICE : 0);
        }

        if (pageNumber != -1) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(result));
        } else {
            Workbook workbook = new HSSFWorkbook();

            Sheet dealersSheet = workbook.createSheet("Dealers");

            int rowNum = 0;
            int cellNum = 0;

            Row titleRow = dealersSheet.createRow(rowNum++);

            Cell titleCell = titleRow.createCell(1);
            titleCell.setCellType(CellType.STRING);
            titleCell.setCellValue(
                    String.format("Отчёт по баллам и зарплате за период с %s по %s",
                            request.getParameter("dateStart"), request.getParameter("dateEnd")));

            Row headerRow = dealersSheet.createRow(rowNum++);

            Cell levelCell = headerRow.createCell(cellNum++);
            levelCell.setCellType(CellType.STRING);
            levelCell.setCellValue("Уровень");

            Cell dealerCell = headerRow.createCell(cellNum++);
            dealerCell.setCellType(CellType.STRING);
            dealerCell.setCellValue("Дилер");

            Cell dealerCodeCell = headerRow.createCell(cellNum++);
            dealerCodeCell.setCellType(CellType.STRING);
            dealerCodeCell.setCellValue("Личный код");

            Cell dealerParentCodeCell = headerRow.createCell(cellNum++);
            dealerParentCodeCell.setCellType(CellType.STRING);
            dealerParentCodeCell.setCellValue("Код спонсора");

            Cell dealerBonusesCell = headerRow.createCell(cellNum++);
            dealerBonusesCell.setCellType(CellType.STRING);
            dealerBonusesCell.setCellValue("Личные бонусы");

            Cell dealerGroupBonusesCell = headerRow.createCell(cellNum++);
            dealerGroupBonusesCell.setCellType(CellType.STRING);
            dealerGroupBonusesCell.setCellValue("Групповые бонусы");

            Cell totalBonusesCell = headerRow.createCell(cellNum++);
            totalBonusesCell.setCellType(CellType.STRING);
            totalBonusesCell.setCellValue("Общее количество бонусов");

            Cell statusCell = headerRow.createCell(cellNum++);
            statusCell.setCellType(CellType.STRING);
            statusCell.setCellValue("Статус");

            Cell salaryCell = headerRow.createCell(cellNum);
            salaryCell.setCellType(CellType.STRING);
            salaryCell.setCellValue("Зарплата");

            for (Map.Entry<Long, Map<String, Object>> dealer : result.entrySet()) {
                cellNum = 0;
                Row currentRow = dealersSheet.createRow(rowNum++);

                Cell tmpLevelCell = currentRow.createCell(cellNum++);
                tmpLevelCell.setCellType(CellType.STRING);
                tmpLevelCell.setCellValue(dealer.getValue().getOrDefault("level", "").toString());

                Cell tmpDealerCell = currentRow.createCell(cellNum++);
                tmpDealerCell.setCellType(CellType.STRING);
                tmpDealerCell.setCellValue(dealer.getValue().getOrDefault("fullName", "").toString());

                Cell tmpDealerCodeCell = currentRow.createCell(cellNum++);
                tmpDealerCodeCell.setCellType(CellType.STRING);
                tmpDealerCodeCell.setCellValue(dealer.getValue().getOrDefault("code", "").toString());

                Cell tmpDealerParentCodeCell = currentRow.createCell(cellNum++);
                tmpDealerParentCodeCell.setCellType(CellType.STRING);
                tmpDealerParentCodeCell.setCellValue(dealer.getValue().getOrDefault("parent", "").toString());

                try {
                    Cell tmpBonusesCell = currentRow.createCell(cellNum++);
                    tmpBonusesCell.setCellType(CellType.NUMERIC);
                    tmpBonusesCell.setCellValue(Double.parseDouble(dealer.getValue().getOrDefault("personal", 0.).toString()));

                    Cell tmpGroupBonusesCell = currentRow.createCell(cellNum++);
                    tmpGroupBonusesCell.setCellType(CellType.NUMERIC);
                    tmpGroupBonusesCell.setCellValue(Double.parseDouble(dealer.getValue().getOrDefault("group", 0.).toString()));

                    Cell tmpTotalBonusesCell = currentRow.createCell(cellNum++);
                    tmpTotalBonusesCell.setCellType(CellType.NUMERIC);
                    tmpTotalBonusesCell.setCellValue(Double.parseDouble(dealer.getValue().getOrDefault("total", 0.).toString()));

                    Cell tmpStatusCell = currentRow.createCell(cellNum++);
                    tmpStatusCell.setCellType(CellType.STRING);
                    tmpStatusCell.setCellValue(DealerBonusesServlet.STATUSES.getOrDefault(dealer.getValue().getOrDefault("status", "").toString(), ""));

                    Cell tmpSalaryCell = currentRow.createCell(cellNum);
                    tmpSalaryCell.setCellType(CellType.NUMERIC);
                    tmpSalaryCell.setCellValue(Double.parseDouble(dealer.getValue().getOrDefault("salary", 0.).toString()));
                } catch (NumberFormatException | NullPointerException e) {
                }
            }

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(tmpResult));
            } catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }

    private void addAllSubDealersToMap(Map<Long, Map<String, Object>> dealerMap, DealerService dealerService,
                                       long dealerId, long dateStart, long dateEnd) {
        Dealer dealer = dealerService.getById(dealerId, 2);

        if (dealer != null) {
            dealer.setBonuses(0.);
            dealer.setGroupBonuses(0.);
            dealer.setStatus("");
            dealer.setMoney(0.);
            dealer
                    .getTransfers()
                    .stream()
                    .filter(distributorProductTransfer ->
                            distributorProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                                    distributorProductTransfer.getDatetime() >= dateStart &&
                                    distributorProductTransfer.getDatetime() <= dateEnd
                    )
                    .forEach(distributorProductTransfer -> {
                        distributorProductTransfer.getProducts().forEach(productDescription -> {
                            dealer.addBonuses(productDescription.getBonuses() * productDescription.getQuantity());
                        });
                    });

            dealer.getSubDealers().forEach(subDealer -> {
                this.addAllSubDealersToMap(dealerMap, dealerService, subDealer.getId(), dateStart, dateEnd);
            });

            Map<String, Object> tmpResult = new HashMap<>();
            tmpResult.put("id", dealer.getId());
            tmpResult.put("code", dealer.getCode());
            tmpResult.put("fullName", dealer.getFullName());
            tmpResult.put("parent", dealer.getParentDealer() != null ? dealer.getParentDealer().getCode() : "");
            tmpResult.put("parent_id", dealer.getParentDealer() != null ? dealer.getParentDealer().getId() : "-1");
            tmpResult.put("personal", dealer.getBonuses());
            tmpResult.put("group", 0);
            tmpResult.put("total", 0);
            tmpResult.put("status", "");
            tmpResult.put("level", 0);
            tmpResult.put("salary", 0);
            tmpResult.put("calculated_group", false);

            dealerMap.put(dealer.getId(), tmpResult);
        }
    }

    private void makeCompression(Map<Long, Map<String, Object>> dealerMap) {
        for (Map.Entry<Long, Map<String, Object>> dealerObj : dealerMap.entrySet()) {
            Map<String, Object> dealer = dealerObj.getValue();
            if (getBonuses(dealer) == 0 && getParentDealer(dealer) != -1) {
                for (Map<String, Object> subDealer : getSubDealers(dealerMap, getId(dealer))) {
                    subDealer.put("parent", dealer.getOrDefault("parent", ""));
                    subDealer.put("parent_id", dealer.getOrDefault("parent_id", "-1"));
                }
                dealerMap.remove(dealerObj.getKey());
            }
        }
    }

    private void calculateGroupBonuses(Map<Long, Map<String, Object>> dealerMap, long dealerId) {
        if (dealerMap.containsKey(dealerId)) {
            Map<String, Object> dealer = dealerMap.get(dealerId);
            dealer.put("group", 0);
            for (Map<String, Object> subDealer : this.getSubDealers(dealerMap, dealerId)) {
                this.calculateGroupBonuses(dealerMap, this.getId(subDealer));
                if (this.getBonuses(subDealer) > 0)
                    this.addGroupBonuses(dealer, dealerId, this.getBonuses(subDealer));
                this.addGroupBonuses(dealer, dealerId, this.getGroupBonuses(subDealer));
            }
        }
    }

    private void calculateStatus(Map<Long, Map<String, Object>> dealerMap, long dealerId) {
        if (dealerMap.containsKey(dealerId)) {
            Map<String, Object> dealer = dealerMap.get(dealerId);
            AtomicInteger activeSubDealers = new AtomicInteger(),
                    activeSubDealersForDirector = new AtomicInteger(),
                    diamondManagers = new AtomicInteger(),
                    silverManagers = new AtomicInteger(),
                    goldManagers = new AtomicInteger(),
                    sapphireDirectors = new AtomicInteger();

            for (Map<String, Object> subDealer : this.getSubDealers(dealerMap, dealerId)) {
                if (this.getStatus(subDealer).isEmpty())
                    this.calculateStatus(dealerMap, this.getId(subDealer));

                if (this.getBonuses(subDealer) > 0) {
                    activeSubDealers.incrementAndGet();
                    if (this.getBonuses(subDealer) >= this.MIN_BONUSES)
                        activeSubDealersForDirector.incrementAndGet();
                }

                if (this.getStatus(subDealer).equals(Constants.USER_STATUS.DEALER_DIAMOND_MANAGER.getValue()))
                    diamondManagers.incrementAndGet();
                if (this.getStatus(subDealer).equals(Constants.USER_STATUS.DEALER_SILVER_MANAGER.getValue()))
                    silverManagers.incrementAndGet();
                if (this.getStatus(subDealer).equals(Constants.USER_STATUS.DEALER_GOLD_MANAGER.getValue()))
                    goldManagers.incrementAndGet();
                if (this.getStatus(subDealer).equals(Constants.USER_STATUS.DEALER_SAPPHIRE_DIRECTOR.getValue()))
                    sapphireDirectors.incrementAndGet();
            }

            if (this.getBonuses(dealer) < this.MIN_BONUSES) {
                dealer.put("status", Constants.USER_STATUS.DEALER_MANAGER.getValue());
            } else {
                if (activeSubDealersForDirector.get() >= 6) {
                    if (this.getGroupBonuses(dealer) >= 130_000 && sapphireDirectors.get() >= 2)
                        dealer.put("status", Constants.USER_STATUS.DEALER_DIAMOND_DIRECTOR.getValue());
                    else if (this.getGroupBonuses(dealer) >= 45_000 && diamondManagers.get() >= 3)
                        dealer.put("status", Constants.USER_STATUS.DEALER_SAPPHIRE_DIRECTOR.getValue());
                    else if (this.getGroupBonuses(dealer) >= 20_000 && goldManagers.get() >= 3)
                        dealer.put("status", Constants.USER_STATUS.DEALER_GOLD_DIRECTOR.getValue());
                    else if (this.getGroupBonuses(dealer) >= 10_000 && silverManagers.get() >= 3)
                        dealer.put("status", Constants.USER_STATUS.DEALER_SILVER_DIRECTOR.getValue());
                    else if (this.getGroupBonuses(dealer) >= 3_500 && diamondManagers.get() >= 1)
                        dealer.put("status", Constants.USER_STATUS.DEALER_DIRECTOR.getValue());
                }

                if (activeSubDealers.get() >= 6)
                    dealer.put("status", Constants.USER_STATUS.DEALER_DIAMOND_MANAGER.getValue());
                else if (activeSubDealers.get() >= 5)
                    dealer.put("status", Constants.USER_STATUS.DEALER_GOLD_MANAGER.getValue());
                else if (activeSubDealers.get() >= 4)
                    dealer.put("status", Constants.USER_STATUS.DEALER_SILVER_MANAGER.getValue());
                else dealer.put("status", Constants.USER_STATUS.DEALER_MANAGER.getValue());
            }
        }
    }

    private int calculateLevel(Map<Long, Map<String, Object>> dealerMap, long dealerId) {
        if (dealerMap.containsKey(dealerId)) {
            if (this.getParentDealer(dealerMap.get(dealerId)) != -1) {
                return 1 + this.calculateLevel(dealerMap, this.getParentDealer(dealerMap.get(dealerId)));
            } else return 1;
        }
        return 0;
    }

    private void calculateSalary(Map<Long, Map<String, Object>> dealerMap, long dealerId) {
        if (dealerMap.containsKey(dealerId)) {
            Map<String, Object> dealer = dealerMap.get(dealerId);

            dealer.put("money", 0);

            this.addMoney(dealer, this.getBonuses(dealer) * 0.1);

            for (Map<String, Object> subDealer : this.getSubDealers(dealerMap, dealerId)) {
                this.addMoney(dealer, this.getBonuses(dealerMap, this.getId(subDealer), 1, this.getStatus(dealer)));
            }
        }
    }

    private double getBonuses(Map<Long, Map<String, Object>> dealerMap, long dealerId, int level, String status) {
        if (status.equals(Constants.USER_STATUS.DEALER_DIAMOND_MANAGER.getValue()) && level > 6) return 0;
        if (status.equals(Constants.USER_STATUS.DEALER_GOLD_MANAGER.getValue()) && level > 5) return 0;
        if (status.equals(Constants.USER_STATUS.DEALER_SILVER_MANAGER.getValue()) && level > 4) return 0;
        if (status.equals(Constants.USER_STATUS.DEALER_MANAGER.getValue()) && level > 3) return 0;

        // Расчёт бонусов для директоров
        double additionalPercent = this.getAdditionalPercentByDirectorStatus(status);

        double result = 0,
                currentBonuses;
        double beforeMinPercent = additionalPercent,
                afterMinPercent = additionalPercent;
        int newLevel = level;

        switch (level) {
            case 1:
                beforeMinPercent += 6;
                afterMinPercent += 10;
                break;
            case 2:
                beforeMinPercent += 9;
                afterMinPercent += 6;
                break;
            case 3:
                beforeMinPercent += 6;
                afterMinPercent += 6;
                break;
            case 4:
            case 5:
                beforeMinPercent += 5;
                afterMinPercent += 4;
                break;
            case 6:
                beforeMinPercent += 9;
                afterMinPercent += 10;
                break;
        }

        if (dealerMap.containsKey(dealerId)) {
            Map<String, Object> currentDealer = dealerMap.get(dealerId);

            String currentDealerStatus = this.getStatus(currentDealer);
            double currentDealerAdditionalPercent = this.getAdditionalPercentByDirectorStatus(currentDealerStatus);
            // Если оба дилера - директора. Проврека на статус текущего дилера и исходного дилера (для которого считается зарплата)
            // Если статус текущего дилера выше - не считаем его. Иначе - берём разность
            if (additionalPercent != 0 && currentDealerAdditionalPercent != 0) {
                if (currentDealerAdditionalPercent - additionalPercent >= 0) {
                    return result;
                } else {
                    beforeMinPercent += currentDealerAdditionalPercent - additionalPercent;
                    afterMinPercent += currentDealerAdditionalPercent - additionalPercent;
                }
            }

            currentBonuses = this.getBonuses(currentDealer);

            if (currentBonuses == 0) {
                newLevel--;
            } else {
                if (currentBonuses < this.MIN_BONUSES) {
                    result += currentBonuses * beforeMinPercent / 100;
                } else {
                    result += this.MIN_BONUSES * beforeMinPercent / 100 +
                            (currentBonuses - this.MIN_BONUSES) * afterMinPercent / 100;
                }
            }

            for (Map<String, Object> subDealer : this.getSubDealers(dealerMap, dealerId)) {
                result += this.getBonuses(dealerMap, this.getId(subDealer), newLevel + 1, status);
            }
        }

        return result;
    }

    private List<Map<String, Object>> getSubDealers(Map<Long, Map<String, Object>> dealerMap, long dealerId) {
        return dealerMap.values().stream().filter(item ->
                item.containsKey("parent_id") &&
                        Long.parseLong(item.getOrDefault("parent_id", "-1").toString()) == dealerId)
                .collect(Collectors.toList());
    }

    private long getId(Map<String, Object> dealer) {
        return Long.parseLong(dealer.getOrDefault("id", "-1").toString());
    }

    private double getBonuses(Map<String, Object> dealer) {
        return Double.parseDouble(dealer.getOrDefault("personal", "0").toString());
    }

    private void addGroupBonuses(Map<String, Object> dealerMap, long to, double bonuses) {
        dealerMap.put("group", Double.parseDouble(dealerMap.getOrDefault("group", "0").toString()) + bonuses);
    }

    private double getGroupBonuses(Map<String, Object> dealer) {
        return Double.parseDouble(dealer.getOrDefault("group", "0").toString());
    }

    private String getStatus(Map<String, Object> dealer) {
        return dealer.getOrDefault("status", "").toString();
    }

    private long getParentDealer(Map<String, Object> dealer) {
        return Long.parseLong(dealer.getOrDefault("parent_id", -1).toString());
    }

    private void addMoney(Map<String, Object> dealer, double money) {
        dealer.put("money", Double.parseDouble(dealer.getOrDefault("money", "0").toString()) + money);
    }

    private double getAdditionalPercentByDirectorStatus(String status) {
        double additionalPercent = 0;
        if (status.equals(Constants.USER_STATUS.DEALER_DIRECTOR.getValue())) {
            additionalPercent = 1;
        }
        if (status.equals(Constants.USER_STATUS.DEALER_SILVER_DIRECTOR.getValue())) {
            additionalPercent = 2;
        }
        if (status.equals(Constants.USER_STATUS.DEALER_GOLD_DIRECTOR.getValue())) {
            additionalPercent = 3;
        }
        if (status.equals(Constants.USER_STATUS.DEALER_SAPPHIRE_DIRECTOR.getValue())) {
            additionalPercent = 4;
        }
        if (status.equals(Constants.USER_STATUS.DEALER_DIAMOND_DIRECTOR.getValue())) {
            additionalPercent = 4.5;
        }

        return additionalPercent;
    }
}
