package servlets.report;

import database.DBServiceSingleton;
import database.domains.IncomingToWarehouse;
import database.domains.Person;
import database.domains.WarehouseProductTransfer;
import database.services.IncomingToWarehouseService;
import database.services.WarehouseProductTransferService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import helpers.ReportExcelHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.cypher.query.SortOrder;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class IncomingToWarehouseServlet extends HttpServlet {
    public static final String URL = "/incoming-to-warehouse";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final IncomingToWarehouseService incomingToWarehouseService = new IncomingToWarehouseService();
        final WarehouseProductTransferService warehouseProductTransferService = new WarehouseProductTransferService();
        long dateStart;
        long dateEnd;
        int pageNumber = 1;
        Map<String, Map<String, Double>> productsQuantityResult = new HashMap<>();
        Map<String, Map<String, Double>> productsPriceResult = new HashMap<>();

        incomingToWarehouseService.setSessionId(request.getHeader("Session"));
        warehouseProductTransferService.setSessionId(request.getHeader("Session"));
        incomingToWarehouseService.clearSession();
        warehouseProductTransferService.setSession(incomingToWarehouseService.getSession());

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
        }
        catch (NullPointerException e) {
            dateStart = 0;
            dateEnd = System.currentTimeMillis();
        }
        catch (ParseException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        try {
            pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
        }
        catch (NumberFormatException|NullPointerException e) {}

        Filters dateFilter = new Filters();
        dateFilter.and(new Filter("datetime", ComparisonOperator.GREATER_THAN_EQUAL, dateStart));
        dateFilter.and(new Filter("datetime", ComparisonOperator.LESS_THAN_EQUAL, dateEnd));

        Collection<IncomingToWarehouse> incomings =
                incomingToWarehouseService
                        .getAll(dateFilter, new SortOrder().add("datetime"), -1, 2);
        Collection<WarehouseProductTransfer> transfers =
                warehouseProductTransferService
                        .getAll(new Filters(dateFilter)
                                .and(new Filter("status", ComparisonOperator.EQUALS, Constants.STATUSES.ACCEPTED.getValue())), new SortOrder().add("datetime"), -1, 2);


        incomings
                .stream()
                .filter(incomingToWarehouse -> this.checkAccess(incomingToWarehouseService.getSessionId(), incomingToWarehouse))
                .forEach(incomingToWarehouse -> {
                    incomingToWarehouse.getProducts().forEach(productQuantityPrice -> {
                        if (!productsQuantityResult.containsKey(productQuantityPrice.getProduct().getFormatted()))
                            productsQuantityResult.put(productQuantityPrice.getProduct().getFormatted(), new HashMap<>());
                        productsQuantityResult.get(productQuantityPrice.getProduct().getFormatted()).put("incoming", productsQuantityResult.get(productQuantityPrice.getProduct().getFormatted()).getOrDefault("incoming", 0.) + productQuantityPrice.getQuantity());

                        if (!productsPriceResult.containsKey(productQuantityPrice.getProduct().getFormatted()))
                            productsPriceResult.put(productQuantityPrice.getProduct().getFormatted(), new HashMap<>());
                        productsPriceResult.get(productQuantityPrice.getProduct().getFormatted()).put("incoming", productsPriceResult.get(productQuantityPrice.getProduct().getFormatted()).getOrDefault("incoming", 0.) + productQuantityPrice.getQuantity() * productQuantityPrice.getPrice());
                    });
                });

        transfers
                .stream()
                .filter(warehouseProductTransfer -> this.checkAccess(warehouseProductTransferService.getSessionId(), warehouseProductTransfer))
                .forEach(warehouseProductTransfer -> {
                    warehouseProductTransfer.getProducts().forEach(productDescription -> {
                        if (!productsQuantityResult.containsKey(productDescription.getProduct().getFormatted()))
                            productsQuantityResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                        productsQuantityResult.get(productDescription.getProduct().getFormatted()).put("outgoing", productsQuantityResult.get(productDescription.getProduct().getFormatted()).getOrDefault("outgoing", 0.) + productDescription.getQuantity());

                        if (!productsPriceResult.containsKey(productDescription.getProduct().getFormatted()))
                            productsPriceResult.put(productDescription.getProduct().getFormatted(), new HashMap<>());
                        productsPriceResult.get(productDescription.getProduct().getFormatted()).put("outgoing", productsPriceResult.get(productDescription.getProduct().getFormatted()).getOrDefault("outgoing", 0.) + productDescription.getQuantity() * productDescription.getPrice());
                    });
                });

        response.setStatus(HttpServletResponse.SC_OK);

        if (pageNumber != -1) {
            Map<String, Object> result = new HashMap<>();

            result.put("quantity", productsQuantityResult);
            result.put("price", productsPriceResult);

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            Workbook workbook = new HSSFWorkbook();

            Sheet productsQuantitySheet = workbook.createSheet("Количество товаров");
            ReportExcelHelper.addInOutProducts(productsQuantitySheet, productsQuantityResult,
                    request.getParameter("dateStart"), request.getParameter("dateEnd"));

            Sheet productsPriceSheet = workbook.createSheet("Цены товаров");
            ReportExcelHelper.addInOutProducts(productsPriceSheet, productsPriceResult,
                    request.getParameter("dateStart"), request.getParameter("dateEnd"));

            try {
                FileService fileService = new FileService();
                String filename = UUID.randomUUID().toString() + ".xls";
                FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

                workbook.write(fileOutputStream);

                fileOutputStream.close();
                workbook.close();

                Map<String, String> tmpResult = new HashMap<>();
                tmpResult.put("file", fileService.getRelativePath(filename));

                response.getWriter().println(new GsonHelper(tmpResult));
            }
            catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
    }

    private boolean checkAccess(String sessionId, IncomingToWarehouse incomingToWarehouse) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        return currentPerson != null &&
                (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) ||
                        currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                                incomingToWarehouse.getWarehouse().equals(currentPerson));
    }

    private boolean checkAccess(String sessionId, WarehouseProductTransfer warehouseProductTransfer) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        return currentPerson != null &&
                (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) ||
                        currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                                warehouseProductTransfer.getWarehouse().equals(currentPerson));
    }
}
