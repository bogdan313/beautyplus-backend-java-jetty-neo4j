package servlets.report;

import database.DBServiceSingleton;
import database.domains.Person;
import database.services.DealerService;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GiftServlet extends HttpServlet {
    public static final String URL = "/gift";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        String sessionId = request.getHeader("Session");
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        final long dateStart;
        final long dateEnd;
        final long minBonuses;

        try {
            dateStart = DateHelper.parse(request.getParameter("dateStart"));
            dateEnd = DateHelper.parse(request.getParameter("dateEnd"));
            minBonuses = Long.parseLong(request.getParameter("bonuses"));
        }
        catch (ParseException | NullPointerException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            return;
        }

        DealerService dealerService = new DealerService();
        DistributorService distributorService = new DistributorService();

        dealerService.setSessionId(sessionId);
        distributorService.setSessionId(sessionId);

        Map<Long, Map<String, Object>> result = new HashMap<>();

        dealerService.getAll(-1, 2).forEach(dealer -> {
            dealer.setBonuses(0.);
            dealer.getTransfers()
                    .stream()
                    .filter(distributorProductTransfer ->
                            distributorProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                                    distributorProductTransfer.getDatetime() >= dateStart &&
                                    distributorProductTransfer.getDatetime() <= dateEnd
                    )
                    .forEach(distributorProductTransfer -> {
                        distributorProductTransfer.getProducts().forEach(productDescription -> {
                            dealer.addBonuses(productDescription.getQuantity() * productDescription.getBonuses());
                        });
                    });

            if (dealer.getBonuses() >= minBonuses) {
                Map<String, Object> tmpResult = new HashMap<>();
                tmpResult.put("code", dealer.getCode());
                tmpResult.put("fullName", dealer.getFullName());
                tmpResult.put("bonuses", dealer.getBonuses());
                tmpResult.put("role", dealer.getRole());

                result.put(dealer.getId(), tmpResult);
            }
        });

        distributorService.getAll(-1, 2).forEach(distributor -> {
            distributor.setBonuses(0.);
            distributor.getWarehouseProductTransfers()
                    .stream()
                    .filter(warehouseProductTransfer -> warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                            warehouseProductTransfer.getDatetime() >= dateStart &&
                            warehouseProductTransfer.getDatetime() <= dateEnd
                    )
                    .forEach(warehouseProductTransfer -> {
                        warehouseProductTransfer.getProducts().forEach(productDescription -> {
                            distributor.addBonuses(productDescription.getQuantity() * productDescription.getBonuses());
                        });
                    });

            if (distributor.getBonuses() >= minBonuses) {
                Map<String, Object> tmpResult = new HashMap<>();
                tmpResult.put("code", distributor.getCode());
                tmpResult.put("fullName", distributor.getFullName());
                tmpResult.put("bonuses", distributor.getBonuses());
                tmpResult.put("role", distributor.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) ? "Дистрибьютор" : "Дилер");

                result.put(distributor.getId(), tmpResult);
            }
        });

        Workbook workbook = new HSSFWorkbook();

        Sheet dealersSheet = workbook.createSheet();

        int rowNum = 0;
        int cellNum = 0;

        Row headerRow = dealersSheet.createRow(rowNum++);

        Cell personCell = headerRow.createCell(cellNum++);
        personCell.setCellType(CellType.STRING);
        personCell.setCellValue("Ф.И.О");

        Cell codeCell = headerRow.createCell(cellNum++);
        codeCell.setCellType(CellType.STRING);
        codeCell.setCellValue("Личный код");

        Cell roleCell = headerRow.createCell(cellNum++);
        roleCell.setCellType(CellType.STRING);
        roleCell.setCellValue("Статус");

        Cell bonusesCell = headerRow.createCell(cellNum);
        bonusesCell.setCellType(CellType.STRING);
        bonusesCell.setCellValue("Личные бонусы");

        for (Map.Entry<Long, Map<String, Object>> person : result.entrySet()) {
            cellNum = 0;
            Row currentRow = dealersSheet.createRow(rowNum++);

            Cell tmpPersonCell = currentRow.createCell(cellNum++);
            tmpPersonCell.setCellType(CellType.STRING);
            tmpPersonCell.setCellValue(person.getValue().getOrDefault("fullName", "").toString());

            Cell tmpCodeCell = currentRow.createCell(cellNum++);
            tmpCodeCell.setCellType(CellType.STRING);
            tmpCodeCell.setCellValue(person.getValue().getOrDefault("code", "").toString());

            Cell tmpStatusCell = currentRow.createCell(cellNum++);
            tmpStatusCell.setCellType(CellType.STRING);
            tmpStatusCell.setCellValue(person.getValue().getOrDefault("role", "").toString());

            try {
                Cell tmpBonusesCell = currentRow.createCell(cellNum);
                tmpBonusesCell.setCellType(CellType.NUMERIC);
                tmpBonusesCell.setCellValue(Double.parseDouble(person.getValue().getOrDefault("bonuses", 0.).toString()));
            }
            catch (NumberFormatException|NullPointerException e) {}
        }

        try {
            FileService fileService = new FileService();
            String filename = UUID.randomUUID().toString() + ".xls";
            FileOutputStream fileOutputStream = new FileOutputStream(fileService.getAbsolutePath(filename));

            workbook.write(fileOutputStream);

            fileOutputStream.close();
            workbook.close();

            Map<String, String> tmpResult = new HashMap<>();
            tmpResult.put("file", fileService.getRelativePath(filename));

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(tmpResult));
        }
        catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
        }
    }
}
