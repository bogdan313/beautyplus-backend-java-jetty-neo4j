package servlets.export;

import database.DBServiceSingleton;
import database.domains.Distributor;
import database.domains.Person;
import database.domains.Warehouse;
import database.domains.WarehouseProductTransfer;
import database.services.WarehouseProductTransferService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WarehouseProductTransferServlet extends HttpServlet {
    public static final String URL = "/warehouse-product-transfer";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final WarehouseProductTransferService warehouseProductTransferService =
                new WarehouseProductTransferService();
        warehouseProductTransferService.setSessionId(request.getHeader("Session"));

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(warehouseProductTransferService.getSessionId());
        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        try {
            WarehouseProductTransfer warehouseProductTransfer =
                    warehouseProductTransferService.getById(Long.parseLong(request.getParameter("id")), 3);
            if (warehouseProductTransfer == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            if (warehouseProductTransfer.getDistributor() == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

                return;
            }

            Distributor distributor = warehouseProductTransfer.getDistributor();
            Warehouse warehouse = warehouseProductTransfer.getWarehouse();

            if (distributor == null || warehouse == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

                return;
            }

            if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) && !distributor.equals(currentPerson) ||
                    currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) && !warehouse.equals(currentPerson)) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

                return;
            }

            distributor.setMoney(0.);
            distributor
                    .getDistributorMoney()
                    .stream()
                    .filter(distributorMoney -> distributorMoney.getDatetime() < warehouseProductTransfer.getDatetime())
                    .forEach(distributorMoney -> {
                        distributor.subtractMoney(distributorMoney.getMoney());
                    });
            distributor
                    .getWarehouseProductTransfers()
                    .stream()
                    .filter(transfer -> transfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                            transfer.getDatetime() < warehouseProductTransfer.getDatetime())
                    .forEach(transfer -> {
                        transfer.getProducts().forEach(productDescription -> {
                            distributor.addMoney(productDescription.getQuantity() * productDescription.getDealerPrice());
                        });
                    });

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(warehouseProductTransfer));
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
