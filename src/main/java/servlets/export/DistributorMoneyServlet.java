package servlets.export;

import database.DBServiceSingleton;
import database.domains.*;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DistributorMoneyServlet extends HttpServlet {
    public static final String URL = "/distributor-money";

    @SuppressWarnings("unchecked")
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorService distributorService = new DistributorService();
        distributorService.setSessionId(request.getHeader("Session"));

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(distributorService.getSessionId());
        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        String distributorId = request.getParameter("distributor");

        if (distributorId == null || distributorId.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) distributorId = currentPerson.getId().toString();

        try {
            final Distributor distributor = distributorService.getById(Long.parseLong(distributorId.trim()), 2);
            if (distributor == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            String dateStartString = request.getParameter("dateStart");
            String dateEndString = request.getParameter("dateEnd");

            if (dateStartString == null || dateStartString.trim().isEmpty() ||
                    dateEndString == null || dateEndString.trim().isEmpty()) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

                return;
            }

            final long dateStart = DateHelper.parse(dateStartString);
            final long dateEnd = DateHelper.parse(dateEndString);

            Map<String, Map<String, Object>> datesResult = new HashMap<>();

            long beforeMoney = 0,
                    beforeTransfers = 0;

            for (DistributorMoney distributorMoney : distributor.getDistributorMoney()) {
                if (distributorMoney.getDatetime() < dateStart) {
                    beforeMoney += distributorMoney.getMoney();
                }
                else if (distributorMoney.getDatetime() >= dateStart && distributorMoney.getDatetime() <= dateEnd) {
                    String currentDDMMYY = DateHelper.getDDMMYY(distributorMoney.getDatetime());
                    if (!datesResult.containsKey(currentDDMMYY)) {
                        Map<String, Object> tmp = new HashMap<>();
                        tmp.put("money", 0.);
                        tmp.put("transfers", 0.);
                        tmp.put("comment_money", new ArrayList<String>());
                        tmp.put("comment_transfers", new ArrayList<String>());
                        datesResult.put(currentDDMMYY, tmp);
                    }

                    Map<String, Object> dateResult = datesResult.get(currentDDMMYY);
                    dateResult.put("money", Double.parseDouble(dateResult.get("money").toString()) + distributorMoney.getMoney());
                    List<String> commentMoney = (ArrayList) dateResult.get("comment_money");
                    if (!commentMoney.contains(distributorMoney.getType())) commentMoney.add(distributorMoney.getType());
                }
            }

            for (WarehouseProductTransfer warehouseProductTransfer : distributor.getWarehouseProductTransfers()) {
                if (!warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue())) continue;

                if (warehouseProductTransfer.getDatetime() < dateStart) {
                    for (ProductDescription productDescription : warehouseProductTransfer.getProducts()) {
                        beforeTransfers +=  productDescription.getQuantity() * productDescription.getDealerPrice();
                    }
                }
                else if (warehouseProductTransfer.getDatetime() >= dateStart && warehouseProductTransfer.getDatetime() <= dateEnd) {
                    String currentDDMMYY = DateHelper.getDDMMYY(warehouseProductTransfer.getDatetime());
                    if (!datesResult.containsKey(currentDDMMYY)) {
                        Map<String, Object> tmp = new HashMap<>();
                        tmp.put("money", 0.);
                        tmp.put("transfers", 0.);
                        tmp.put("comment_money", new ArrayList<String>());
                        tmp.put("comment_transfers", new ArrayList<String>());
                        datesResult.put(currentDDMMYY, tmp);
                    }

                    Map<String, Object> dateResult = datesResult.get(currentDDMMYY);

                    for (ProductDescription productDescription : warehouseProductTransfer.getProducts()) {
                        dateResult.put("transfers", Double.parseDouble(dateResult.get("transfers").toString()) +
                                productDescription.getQuantity() * productDescription.getDealerPrice());
                    }

                    List<String> commentTransfers = (ArrayList) dateResult.get("comment_transfers");
                    if (!commentTransfers.contains(String.valueOf(warehouseProductTransfer.getNumber())))
                        commentTransfers.add(String.valueOf(warehouseProductTransfer.getNumber()));
                }
            }

            Map<String, Object> result = new HashMap<>();
            result.put("before_money", beforeMoney);
            result.put("before_transfers", beforeTransfers);
            result.put("dates", datesResult);

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(result));
        }
        catch (NumberFormatException | ParseException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
