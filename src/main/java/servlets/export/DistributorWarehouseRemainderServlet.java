package servlets.export;

import database.DBServiceSingleton;
import database.domains.Distributor;
import database.domains.Person;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class DistributorWarehouseRemainderServlet extends HttpServlet {
    public static final String URL = "/distributor-warehouse-remainder";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorService distributorService = new DistributorService();
        distributorService.setSessionId(request.getHeader("Session"));

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(distributorService.getSessionId());
        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        String distributorId = request.getParameter("distributor");
        String dateStartString = request.getParameter("dateStart");
        String dateEndString = request.getParameter("dateEnd");

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()))
            distributorId = currentPerson.getId().toString();

        if (distributorId == null || distributorId.trim().isEmpty() ||
                dateStartString == null || dateStartString.trim().isEmpty() ||
                dateEndString == null || dateEndString.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            Distributor distributor = distributorService.getById(Long.parseLong(distributorId.trim()), 3);
            final long dateStart = DateHelper.parse(dateStartString);
            final long dateEnd = DateHelper.parse(dateEndString);

            if (distributor == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            Map<String, Map<String, Object>> productsResult = new HashMap<>();

            distributor
                    .getWarehouseProductTransfers()
                    .stream()
                    .filter(warehouseProductTransfer -> warehouseProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()))
                    .forEach(warehouseProductTransfer -> {
                        warehouseProductTransfer.getProducts().forEach(productDescription -> {
                            if (!productsResult.containsKey(productDescription.getProduct().getCode())) {
                                Map<String, Object> tmp = new HashMap<>();
                                tmp.put("product", productDescription.getProduct().getTitle());
                                tmp.put("price", productDescription.getDealerPrice());
                                tmp.put("bonuses_per_one", productDescription.getBonuses());
                                tmp.put("before", 0.);
                                tmp.put("bought", 0.);
                                tmp.put("sold", 0.);
                                tmp.put("remainder", 0.);
                                tmp.put("bonuses", 0.);
                                productsResult.put(productDescription.getProduct().getCode(), tmp);
                            }

                            Map<String, Object> product = productsResult.get(productDescription.getProduct().getCode());

                            if (warehouseProductTransfer.getDatetime() < dateStart) {
                                product.put("before", Double.parseDouble(product.getOrDefault("before", 0.).toString()) + productDescription.getQuantity());
                            }
                            else if (warehouseProductTransfer.getDatetime() >= dateStart &&
                                        warehouseProductTransfer.getDatetime() <= dateEnd) {
                                product.put("bought", Double.parseDouble(product.getOrDefault("bought", 0.).toString()) + productDescription.getQuantity());
                            }
                        });
                    });
            distributor
                    .getTransfers()
                    .stream()
                    .filter(distributorProductTransfer -> distributorProductTransfer.getStatus().equals(Constants.STATUSES.ACCEPTED.getValue()))
                    .forEach(distributorProductTransfer -> {
                        distributorProductTransfer.getProducts().forEach(productDescription -> {
                            if (!productsResult.containsKey(productDescription.getProduct().getCode())) {
                                Map<String, Object> tmp = new HashMap<>();
                                tmp.put("product", productDescription.getProduct().getTitle());
                                tmp.put("price", productDescription.getDealerPrice());
                                tmp.put("bonuses_per_one", productDescription.getBonuses());
                                tmp.put("before", 0.);
                                tmp.put("bought", 0.);
                                tmp.put("sold", 0.);
                                tmp.put("remainder", 0.);
                                tmp.put("bonuses", 0.);
                                productsResult.put(productDescription.getProduct().getCode(), tmp);
                            }

                            Map<String, Object> product = productsResult.get(productDescription.getProduct().getCode());

                            if (distributorProductTransfer.getDatetime() < dateStart) {
                                product.put("before", Double.parseDouble(product.getOrDefault("before", 0.).toString()) - productDescription.getQuantity());
                            }
                            else if (distributorProductTransfer.getDatetime() >= dateStart &&
                                    distributorProductTransfer.getDatetime() <= dateEnd) {
                                product.put("sold", Double.parseDouble(product.getOrDefault("sold", 0.).toString()) + productDescription.getQuantity());
                            }
                        });
                    });
            productsResult
                    .forEach((productCode, productData) -> {
                        productData.put("remainder", Double.parseDouble(productData.get("before").toString()) +
                                Double.parseDouble(productData.get("bought").toString()) -
                                Double.parseDouble(productData.get("sold").toString()));
                        productData.put("bonuses", Double.parseDouble(productData.get("remainder").toString()) *
                                Double.parseDouble(productData.get("bonuses_per_one").toString()));
                    });

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(productsResult));
        }
        catch (NumberFormatException | ParseException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
