package servlets.user;

import database.services.PersonService;
import database.DBServiceSingleton;
import database.domains.Person;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAvatarServlet extends HttpServlet {
    public static final String URL = "/avatar";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        String base64 = request.getParameter("photo");

        personService.setSessionId(request.getHeader("Session"));

        if (base64 == null || base64.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        try {
            long id = Long.parseLong(request.getParameter("id"));
            Person person = personService.getById(id);
            if (person == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
                return;
            }
            FileService fileService = new FileService();
            String newPhoto = fileService.saveImageFromBase64ToPng(base64);
            if (newPhoto != null) {
                person.setPhoto(newPhoto);
                if (personService.save(person)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(person));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
