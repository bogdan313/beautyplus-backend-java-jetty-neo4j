package servlets.user;

import database.domains.Person;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import helpers.ParseParametersHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class UserProfileServlet extends HttpServlet {
    public static final String URL = "/profile";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        personService.setSessionId(request.getHeader("Session"));

        try {
            Long id = Long.parseLong(request.getParameter("id"));
            Person person = personService.getById(id);
            if (person != null) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(person));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(personService.getAll()));
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        personService.setSessionId(request.getHeader("Session"));

        try {
            Map<String, Object> personParameters = ParseParametersHelper.parse(request.getParameterMap());
            long id = Long.parseLong(personParameters.getOrDefault("id", "").toString());

            Person currentPerson = personService.getById(id);
            if (currentPerson == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
                return;
            }

            String fullName = personParameters.getOrDefault("fullName", "").toString().trim();
            if (!fullName.isEmpty()) {
                currentPerson.setFullName(fullName);
            }

            String code = personParameters.getOrDefault("code", "").toString().trim();
            if (!code.isEmpty()) {
                Person tmpPerson = personService.getByCode(code);
                if (tmpPerson == null || tmpPerson.getId() != null && tmpPerson.getId().equals(currentPerson.getId()))
                    currentPerson.setCode(code);
            }

            String login = personParameters.getOrDefault("login", "").toString().trim();
            if (!login.isEmpty()) {
                Person tmpPerson = personService.getByLogin(login);
                if (tmpPerson == null || tmpPerson.getId() != null && tmpPerson.getId().equals(currentPerson.getId()))
                    currentPerson.setLogin(login);
            }

            String phoneNumber = personParameters.getOrDefault("phoneNumber", "").toString().trim();
            currentPerson.setPhoneNumber(phoneNumber);

            if (personService.save(currentPerson)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(currentPerson));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doPut(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);

        try {
            long id = Long.parseLong(request.getParameter("id"));
            personService.setSessionId(request.getHeader("Session"));
            String password = request.getParameter("password");

            if (password == null || password.trim().isEmpty()) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            Person currentPerson = personService.getById(id);
            if (currentPerson != null) {
                currentPerson.setPassword(password.trim());
                if (personService.save(currentPerson)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(currentPerson));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        personService.setSessionId(request.getHeader("Session"));

        try {
            Person person = personService.getById(Long.parseLong(request.getParameter("id")));
            if (person != null) {
                if (personService.delete(person)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
