package servlets.user;

import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserOnlineServlet extends HttpServlet {
    public static final String URL = "/online";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(new GsonHelper(AuthenticationServiceSingleton.getInstance().getOnline()));
    }
}
