package servlets.moderator;

import database.domains.Moderator;
import database.domains.Person;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import org.neo4j.ogm.cypher.query.SortOrder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ModeratorServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final PersonService<Moderator> moderatorService = new PersonService<>(Moderator.class);
        String id = request.getParameter("id");

        moderatorService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            Map<String, Object> result = new HashMap<>();

            result.put("data", moderatorService.getAll(request.getParameterMap()));
            result.put("totalRecords", "0");

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            try {
                Moderator moderator = moderatorService.getById(Long.parseLong(id));
                if (moderator == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(moderator));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Moderator> moderatorService = new PersonService<>(Moderator.class);
        final PersonService<Person> personService = new PersonService<>(Person.class);
        String login = request.getParameter("login");
        String code = request.getParameter("code");

        moderatorService.setSessionId(request.getHeader("Session"));
        personService.setSession(moderatorService.getSession());

        if (login == null || login.trim().isEmpty() ||
                code == null || code.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        if (personService.existsByLoginOrCode(login, code)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_ALREADY_EXISTS)));
            return;
        }
        Moderator moderator = new Moderator();
        if (moderator.setParameters(request.getParameterMap())) {
            if (moderatorService.save(moderator)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(moderator));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Moderator> moderatorService = new PersonService<>(Moderator.class);

        try {
            long id = Long.parseLong(request.getParameter("id"));
            Moderator moderator = moderatorService.getById(id);
            if (moderator == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
            else {
                if (moderatorService.delete(moderator)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
