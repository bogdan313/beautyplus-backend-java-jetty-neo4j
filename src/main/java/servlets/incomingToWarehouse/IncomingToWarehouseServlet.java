package servlets.incomingToWarehouse;

import database.domains.IncomingToWarehouse;
import database.domains.ProductQuantityPrice;
import database.services.IncomingToWarehouseService;
import database.services.ProductQuantityPriceService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import servlets.ServletWithAuthorTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IncomingToWarehouseServlet extends ServletWithAuthorTemplate<IncomingToWarehouse, IncomingToWarehouseService> {
    public IncomingToWarehouseServlet() {
        super(IncomingToWarehouseService.class, IncomingToWarehouse.class);
    }

    @SuppressWarnings("unchecked")
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        try {
            long id = Long.parseLong(request.getParameter("id"));
            final IncomingToWarehouseService incomingToWarehouseService = new IncomingToWarehouseService();

            incomingToWarehouseService.setSessionId(request.getHeader("Session"));
            IncomingToWarehouse incomingToWarehouse = incomingToWarehouseService.getById(id);
            if (incomingToWarehouse != null) {
                ProductQuantityPriceService productQuantityPriceService = new ProductQuantityPriceService();
                productQuantityPriceService.setSession(incomingToWarehouseService.getSession());
                for (ProductQuantityPrice item : incomingToWarehouse.getProducts()) {
                    productQuantityPriceService.delete(item);
                }
                if (incomingToWarehouseService.delete(incomingToWarehouse))
                    response.setStatus(HttpServletResponse.SC_OK);
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
