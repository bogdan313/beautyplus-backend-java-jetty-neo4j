package servlets.gift;

import database.domains.Gift;
import database.services.GiftService;
import servlets.ServletWithAuthorTemplate;

public class GiftServlet extends ServletWithAuthorTemplate<Gift, GiftService> {
    public static final String URL = "/";

    public GiftServlet() {
        super(GiftService.class, Gift.class);
    }
}
