package servlets.warehouseProductTransfer;

import database.domains.ProductDescription;
import database.domains.WarehouseProductTransfer;
import database.services.ProductDescriptionService;
import database.services.WarehouseProductTransferService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import servlets.ServletWithAuthorTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WarehouseProductTransferServlet extends ServletWithAuthorTemplate<WarehouseProductTransfer, WarehouseProductTransferService> {
    public WarehouseProductTransferServlet() {
        super(WarehouseProductTransferService.class, WarehouseProductTransfer.class);
    }

    @SuppressWarnings("unchecked")
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        try {
            long id = Long.parseLong(request.getParameter("id"));
            final WarehouseProductTransferService warehouseProductTransferService =
                    new WarehouseProductTransferService();

            warehouseProductTransferService.setSessionId(request.getHeader("Session"));
            WarehouseProductTransfer warehouseProductTransfer = warehouseProductTransferService.getById(id);
            if (warehouseProductTransfer != null) {
                ProductDescriptionService productDescriptionService = new ProductDescriptionService();
                productDescriptionService.setSession(warehouseProductTransferService.getSession());
                for (ProductDescription item : warehouseProductTransfer.getProducts()) {
                    productDescriptionService.delete(item);
                }
                if (warehouseProductTransferService.delete(warehouseProductTransfer))
                    response.setStatus(HttpServletResponse.SC_OK);
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
