package servlets.warehouseProductTransfer;

import database.domains.WarehouseProductTransfer;
import database.services.WarehouseProductTransferService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeclineServlet extends HttpServlet {
    public static final String URL = "/decline";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final WarehouseProductTransferService warehouseProductTransferService =
                new WarehouseProductTransferService();
        String id = request.getParameter("id");

        warehouseProductTransferService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            WarehouseProductTransfer warehouseProductTransfer =
                    warehouseProductTransferService.getById(Long.parseLong(id));

            if (warehouseProductTransfer == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            if (warehouseProductTransferService.changeStatus(warehouseProductTransfer, Constants.STATUSES.DECLINED.getValue())) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(warehouseProductTransfer));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
