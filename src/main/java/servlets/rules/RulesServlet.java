package servlets.rules;

import database.domains.Rules;
import database.services.RulesService;
import servlets.ServletWithAuthorTemplate;

public class RulesServlet extends ServletWithAuthorTemplate<Rules, RulesService> {
    public RulesServlet() {
        super(RulesService.class, Rules.class);
    }
}
