package servlets.rules;

import database.domains.Person;
import database.domains.Rules;
import database.services.PersonService;
import database.services.RulesService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SetRulesServlet extends HttpServlet {
    public static final String URL = "/set-rules";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final RulesService rulesService = new RulesService();
        final PersonService<Person> personService = new PersonService<>(Person.class);

        rulesService.setSessionId(request.getHeader("Session"));
        personService.setSessionId(request.getHeader("Session"));
        personService.setSession(rulesService.getSession());

        try {
            long rulesId = Long.parseLong(request.getParameter("rules"));
            long personId = Long.parseLong(request.getParameter("person"));
            Person person = personService.getById(personId);
            Rules rules = rulesService.getById(rulesId);
            if (person == null || rules == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
            else {
                person.setRules(rules);
                personService.save(person);
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(person));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
