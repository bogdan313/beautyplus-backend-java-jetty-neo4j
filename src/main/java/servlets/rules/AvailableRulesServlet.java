package servlets.rules;

import database.domains.Rules;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AvailableRulesServlet extends HttpServlet {
    public static final String URL = "/available-rules";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        Map<String, Object> result = new HashMap<>();
        result.put("rules", Rules.RULES);
        result.put("models", Rules.DOMAINS);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(new GsonHelper(result));
    }
}
