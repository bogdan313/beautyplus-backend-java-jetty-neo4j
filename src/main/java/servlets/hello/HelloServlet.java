package servlets.hello;

import database.DBServiceSingleton;
import database.domains.DistributorProductTransfer;
import database.domains.Rules;
import database.services.DealerService;
import helpers.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class HelloServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        DBServiceSingleton.getInstance().getSession().deleteAll(DistributorProductTransfer.class);
        response.getWriter().println("Distributor product transfers were deleted!");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
