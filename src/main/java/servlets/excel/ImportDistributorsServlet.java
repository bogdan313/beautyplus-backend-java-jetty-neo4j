package servlets.excel;

import database.domains.Distributor;
import database.domains.Person;
import database.services.DistributorService;
import errorMessages.ErrorMessage;
import errorMessages.ExcelError;
import helpers.Constants;
import helpers.GsonHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ImportDistributorsServlet extends HttpServlet {
    public static final String URL = "/import-distributors";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final DistributorService distributorService = new DistributorService();

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session"));
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            return;
        }

        Part file = request.getPart("file");
        if (file != null) {
            FileService fileService = new FileService();
            String filename = fileService.saveUploadedFile(file);
            try (FileInputStream excelFile = new FileInputStream(new File(fileService.getAbsolutePath(filename)))) {
                Workbook workbook = new HSSFWorkbook(excelFile);
                Sheet firstSheet = workbook.getSheetAt(0);
                Map<String, Distributor> existDistributors = new HashMap<>();
                distributorService.setSessionId(request.getHeader("Session"));

                distributorService.deleteAll();

                for (int i = 0; i <= firstSheet.getLastRowNum(); i++) {
                    Row row = firstSheet.getRow(i);
                    if (row != null && row.getFirstCellNum() > -1) {
                        Distributor currentDistributor = new Distributor();
                        row.getCell(0).setCellType(CellType.STRING);
                        row.getCell(1).setCellType(CellType.STRING);
                        row.getCell(2).setCellType(CellType.STRING);
                        row.getCell(3).setCellType(CellType.STRING);

                        String code = row.getCell(0).getStringCellValue().trim();
                        String fullName = row.getCell(1).getStringCellValue().trim();
                        String warehouseName = row.getCell(2).getStringCellValue().trim();
                        String phoneNumber = row.getCell(3).getStringCellValue().trim();

                        if (code.isEmpty() || fullName.isEmpty()) {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(String.format("%s", i + 1))));
                                return;
                            }
                            catch (IOException exception) {
                            }
                        }

                        currentDistributor.setCode(code);
                        currentDistributor.setFullName(fullName);
                        currentDistributor.setWarehouseName(warehouseName);
                        currentDistributor.setLogin("distributor_" + currentDistributor.getCode());
                        currentDistributor.setPassword("123");
                        currentDistributor.setPhoneNumber(phoneNumber);

                        if (!existDistributors.containsKey(currentDistributor.getCode())) {
                            existDistributors.put(currentDistributor.getCode(), currentDistributor);
                        }
                        else {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(currentDistributor.getCode(),
                                        ExcelError.EXCEL_ERROR_DISTRIBUTOR_ALREADY_EXISTS)));
                            }
                            catch (IOException exception) {
                            }

                            return;
                        }
                    }
                }
                existDistributors.values().forEach(distributorService::save);
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(existDistributors.values()));
            }
            finally {
                fileService.deleteFile(filename);
            }
        }
    }
}
