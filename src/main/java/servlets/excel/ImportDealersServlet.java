package servlets.excel;

import database.domains.Dealer;
import database.domains.Person;
import database.services.DealerService;
import errorMessages.ErrorMessage;
import errorMessages.ExcelError;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GsonHelper;
import helpers.MyLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ImportDealersServlet extends HttpServlet {
    public static final String URL = "/import-dealers";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        String sessionId = request.getHeader("Session");

        final DealerService dealerService = new DealerService();

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            return;
        }

        response.setStatus(HttpServletResponse.SC_OK);
        dealerService.setSessionId(sessionId);
        Part file = request.getPart("file");

        if (file != null) {
            FileService fileService = new FileService();
            String filename = fileService.saveUploadedFile(file);
            try (FileInputStream excelFile = new FileInputStream(new File(fileService.getAbsolutePath(filename)))) {
                Workbook workbook = new HSSFWorkbook(excelFile);
                Sheet firstSheet = workbook.getSheetAt(0);
                final Map<String, Dealer> existDealers = new HashMap<>();
                final Map<String, String> wantParentDealers = new HashMap<>();
                int totalSize;
                long withoutParentSize;
                AtomicInteger currentStep = new AtomicInteger(1);

                dealerService.deleteAll();

                MyLogger.log("Start parsing file");

                for (int i = 0; i <= firstSheet.getLastRowNum(); i++) {
                    Row row = firstSheet.getRow(i);
                    if (row != null) {
                        Dealer currentDealer = new Dealer();
                        row.getCell(0).setCellType(CellType.STRING);
                        row.getCell(1).setCellType(CellType.STRING);

                        String code = row.getCell(0).getStringCellValue().trim();
                        String fullName = row.getCell(1).getStringCellValue().trim();
                        String parentCode = "";
                        long registrationDate = System.currentTimeMillis();

                        if (row.getCell(2) != null) {
                            row.getCell(2).setCellType(CellType.STRING);
                            parentCode = row.getCell(2).getStringCellValue().trim();
                        }

                        if (row.getCell(3) != null) {
                            row.getCell(3).setCellType(CellType.STRING);
                            try {
                                registrationDate = DateHelper.parse(row.getCell(3).getStringCellValue().trim());
                            }
                            catch (ParseException ignore) {}
                        }

                        if (code.isEmpty() || fullName.isEmpty()) {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(String.format("%s", i + 1))));
                            }
                            catch (IOException exception) {}

                            return;
                        }

                        currentDealer.setCode(code);
                        currentDealer.setFullName(fullName);
                        currentDealer.setLogin("dealer_" + currentDealer.getCode());
                        currentDealer.setPassword("123");
                        currentDealer.setRegistrationDate(registrationDate);

                        if (!parentCode.isEmpty()) {
                            wantParentDealers.put(currentDealer.getCode(), parentCode);
                        }

                        if (!existDealers.containsKey(currentDealer.getCode())) {
                            existDealers.put(currentDealer.getCode(), currentDealer);
                        }
                        else {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(currentDealer.getCode(),
                                        ExcelError.EXCEL_ERROR_DEALER_ALREADY_EXISTS)));
                            }
                            catch (IOException exception) {}

                            return;
                        }
                    }
                }

                totalSize = existDealers.size();
                MyLogger.log(String.format("End parsing file. Total records: %s", totalSize));

                for (Map.Entry<String, String> entry : wantParentDealers.entrySet()) {
                    String dealerCode = entry.getKey();
                    String parentCode = entry.getValue();

                    if (existDealers.containsKey(dealerCode) && existDealers.containsKey(parentCode)) {
                        existDealers.get(dealerCode).setParentDealer(existDealers.get(parentCode));
                        existDealers.get(parentCode).addSubDealer(existDealers.get(dealerCode));
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        try {
                            if (!existDealers.containsKey(dealerCode)) {
                                MyLogger.log(String.format("Dealer not found #%s", dealerCode));
                                response.getWriter().println(new GsonHelper(new ExcelError(dealerCode,
                                        ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));
                            }
                            if (!existDealers.containsKey(parentCode)) {
                                MyLogger.log(String.format("Dealer not found #%s", parentCode));
                                response.getWriter().println(new GsonHelper(new ExcelError(parentCode,
                                        ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));
                            }
                        }
                        catch (IOException e) {}

                        return;
                    }
                }

                withoutParentSize = existDealers.values().stream().filter(dealer -> dealer.getParentDealer() == null).count();

                existDealers.values()
                        .stream()
                        .filter(dealer -> dealer.getParentDealer() == null)
                        .forEach(dealer -> {
                            MyLogger.log(String.format("Current saving: %s. %s of %s (%s)",
                                    dealer.getCode(), currentStep.getAndIncrement(), totalSize, withoutParentSize));
                            dealerService.save(dealer);
                        });

                response.setStatus(HttpServletResponse.SC_OK);
                //response.getWriter().println(new GsonHelper(existDealers.values()));
            }
            finally {
                fileService.deleteFile(filename);
            }
        }
    }

    private boolean hasCircle(Map<String, Dealer> dealerMap, String dealerCode) {
        if (dealerMap.containsKey(dealerCode)) {
            Dealer dealer = dealerMap.get(dealerCode);

            if (dealer.getParentDealer() != null && dealer.getParentDealer().getCode().equals(dealerCode)) {
                return true;
            }

            for (Dealer subDealer : getSubDealers(dealerMap, dealerCode)) {
                if (subDealer.getCode().equals(dealerCode) || hasCircle(dealerMap, subDealer.getCode(), dealerCode)) {
                    return true;
                }

            }
        }
        return false;
    }

    private boolean hasCircle(Map<String, Dealer> dealerMap, String dealerCode, String checkCode) {
        for (Dealer subDealer : getSubDealers(dealerMap, dealerCode)) {
            if (subDealer.getCode().equals(checkCode) || hasCircle(dealerMap, subDealer.getCode(), checkCode)) {
                MyLogger.log(String.format("Current dealer: %s, dealer to check: %s", subDealer.getCode(), checkCode));
                return true;
            }
        }

        return false;
    }


    private List<Dealer> getSubDealers(Map<String, Dealer> dealerMap, String dealerCode) {
        return dealerMap.values().stream()
                .filter(dealer -> dealer.getParentDealer() != null &&
                        dealer.getParentDealer().getCode().equals(dealerCode))
                .collect(Collectors.toList());
    }
}
