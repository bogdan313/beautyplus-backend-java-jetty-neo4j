package servlets.excel;

import database.domains.*;
import database.services.DealerService;
import database.services.DistributorProductTransferService;
import database.services.DistributorService;
import database.services.ProductService;
import errorMessages.ErrorMessage;
import errorMessages.ExcelError;
import helpers.Constants;
import helpers.GsonHelper;
import helpers.MyLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class ImportDealersReportServlet extends HttpServlet {
    public static final String URL = "/import-dealers-report";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        final DistributorService distributorService = new DistributorService();
        final ProductService productService = new ProductService();
        final DistributorProductTransferService distributorProductTransferService =
                new DistributorProductTransferService();
        String distributorId = request.getParameter("distributor");

        if (distributorId == null || distributorId.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session"));
        if (currentPerson == null ||
                !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                !currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));

            return;
        }

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            distributorId = currentPerson.getId().toString();
        }

        dealerService.setSessionId(request.getHeader("Session"));
        distributorService.setSessionId(request.getHeader("Session"));
        productService.setSessionId(request.getHeader("Session"));
        distributorProductTransferService.setSessionId(request.getHeader("Session"));

        distributorService.setSession(dealerService.getSession());
        productService.setSession(dealerService.getSession());
        distributorProductTransferService.setSession(dealerService.getSession());

        response.setStatus(HttpServletResponse.SC_OK);

        try {
            Distributor distributor = distributorService.getById(Long.parseLong(distributorId.trim()));

            if (distributor == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            Part file = request.getPart("file");
            if (file != null) {
                FileService fileService = new FileService();
                String filename = fileService.saveUploadedFile(file);
                try (FileInputStream excelFile = new FileInputStream(new File(fileService.getAbsolutePath(filename)))) {
                    Workbook workbook = new HSSFWorkbook(excelFile);
                    Sheet firstSheet = workbook.getSheetAt(0);
                    Map<Integer, Product> products = new HashMap<>();
                    Map<String, Map<Product, Integer>> result = new HashMap<>();
                    Map<String, Dealer> dealers = new HashMap<>();

                    MyLogger.log("Start parsing file");

                    int rowNum = 1;
                    int cellNum = 6;
                    Row productsRow = firstSheet.getRow(rowNum);
                    if (productsRow == null) {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ExcelError(String.format("%s", rowNum))));
                        return;
                    }
                    while (true) {
                        Cell productCell = productsRow.getCell(cellNum);
                        if (productCell == null) break;

                        productCell.setCellType(CellType.STRING);
                        String productCode = productCell.getStringCellValue();

                        if (productCode == null || productCode.trim().isEmpty()) break;

                        Product currentProduct = productService.getByCode(productCode.trim());

                        if (currentProduct != null) {
                            products.put(cellNum, currentProduct);
                        }
                        else {
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            response.getWriter().println(new GsonHelper(new ExcelError(productCode.trim(),
                                    ExcelError.EXCEL_ERROR_PRODUCT_NOT_FOUND)));
                            return;
                        }

                        cellNum++;
                    }

                    rowNum = 2;
                    while (true) {
                        Row currentRow = firstSheet.getRow(rowNum);
                        if (currentRow == null) break;

                        Cell dealerCell = currentRow.getCell(1);
                        if (dealerCell == null) break;

                        dealerCell.setCellType(CellType.STRING);
                        String dealerCode = dealerCell.getStringCellValue();
                        if (dealerCode == null || dealerCode.trim().isEmpty())
                            break;

                        Dealer dealer = dealerService.getByCode(dealerCode.trim());

                        if (dealer == null) {
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            response.getWriter().println(new GsonHelper(new ExcelError(dealerCode,
                                    ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));

                            return;
                        }

                        if (!result.containsKey(dealerCode.trim())) result.put(dealerCode.trim(), new HashMap<>());
                        if (!dealers.containsKey(dealerCode))
                            dealers.put(dealerCode, dealer);

                        Map<Product, Integer> currentDealerRow = result.get(dealerCode.trim());
                        products.forEach((index, product) -> {
                            Cell productQuantityCell = currentRow.getCell(index);
                            if (productQuantityCell != null) {
                                productQuantityCell.setCellType(CellType.STRING);
                                try {
                                    int productQuantity = Integer.parseInt(productQuantityCell.getStringCellValue());

                                    if (productQuantity > 0)
                                        currentDealerRow.put(product, productQuantity);
                                }
                                catch (NumberFormatException e) {}
                            }
                        });

                        rowNum++;
                    }

                    MyLogger.log("End parsing file");

                    for (Map.Entry<String, Map<Product, Integer>> entry : result.entrySet()) {
                        String dealerCode = entry.getKey();
                        Map<Product, Integer> dealerProducts = entry.getValue();

                        Dealer currentDealer = dealers.get(dealerCode);
                        if (currentDealer != null) {
                            DistributorProductTransfer currentTransfer = new DistributorProductTransfer();

                            currentTransfer.setDealer(currentDealer);
                            currentTransfer.setDatetime(System.currentTimeMillis());
                            currentTransfer.setStatus(Constants.STATUSES.ACCEPTED.getValue());
                            currentTransfer.setCode(UUID.randomUUID().toString().substring(0, 6));
                            currentTransfer.setDistributor(distributor);
                            currentTransfer.setAuthor(currentPerson);
                            currentTransfer.setCreatedDate(System.currentTimeMillis());

                            for (Map.Entry<Product, Integer> productsEntry : dealerProducts.entrySet()) {
                                Product currentProduct = productsEntry.getKey();
                                int productQuantity = productsEntry.getValue();

                                ProductDescription currentDescription = new ProductDescription();
                                currentDescription.setProduct(currentProduct);
                                currentDescription.setQuantity(productQuantity);
                                currentDescription.setBonuses(currentProduct.getBonuses());
                                currentDescription.setPrice(currentProduct.getPrice());
                                currentDescription.setDealerPrice(currentProduct.getDealerPrice());

                                currentTransfer.addProduct(currentDescription);
                            }

                            if (!currentTransfer.getProducts().isEmpty())
                                distributorProductTransferService.save(currentTransfer);
                        }
                        else {
                            MyLogger.log(String.format("Dealer not found #%s (%s)", dealerCode, file.getName()));
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            response.getWriter().println(new GsonHelper(new ExcelError(dealerCode,
                                    ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));

                            return;
                        }
                    }

                    response.setStatus(HttpServletResponse.SC_OK);
                }
                finally {
                    fileService.deleteFile(filename);
                }
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
