package servlets.excel;

import database.DBServiceSingleton;
import database.domains.Dealer;
import database.domains.Person;
import database.services.DealerService;
import errorMessages.ErrorMessage;
import errorMessages.ExcelError;
import helpers.Constants;
import helpers.GsonHelper;
import helpers.MyLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import services.AuthenticationServiceSingleton;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class AddDealersServlet extends HttpServlet {
    public static final String URL = "/add-dealers";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final DealerService dealerService = new DealerService();

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session"));
        if (currentPerson == null || !currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            return;
        }

        Part file = request.getPart("file");
        if (file != null) {
            FileService fileService = new FileService();
            String filename = fileService.saveUploadedFile(file);
            try (FileInputStream excelFile = new FileInputStream(new File(fileService.getAbsolutePath(filename)))) {
                Workbook workbook = new HSSFWorkbook(excelFile);
                Sheet firstSheet = workbook.getSheetAt(0);
                Map<String, Dealer> existDealers = new HashMap<>();
                Map<String, String> wantParentDealers = new HashMap<>();

                dealerService.getAll().forEach(dealer -> {
                    existDealers.put(dealer.getCode(), dealer);
                });

                for (int i = 0; i <= firstSheet.getLastRowNum(); i++) {
                    Row row = firstSheet.getRow(i);
                    if (row != null && row.getFirstCellNum() > -1) {
                        Dealer currentDealer = new Dealer();
                        row.getCell(0).setCellType(CellType.STRING);
                        row.getCell(1).setCellType(CellType.STRING);

                        String code = row.getCell(0).getStringCellValue().trim();
                        String fullName = row.getCell(1).getStringCellValue().trim();
                        String parentCode = "";

                        if (row.getCell(2) != null) {
                            row.getCell(2).setCellType(CellType.STRING);
                            parentCode = row.getCell(2).getStringCellValue().trim();
                        }

                        if (code.isEmpty() || fullName.isEmpty()) {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(String.format("%s", i + 1))));
                            }
                            catch (IOException exception) {}

                            return;
                        }

                        currentDealer.setCode(code);
                        currentDealer.setFullName(fullName);
                        currentDealer.setLogin("dealer_" + currentDealer.getCode());
                        currentDealer.setPassword("123");
                        currentDealer.setRegistrationDate(System.currentTimeMillis());

                        if (!parentCode.isEmpty() && existDealers.containsKey(parentCode)) {
                            wantParentDealers.put(currentDealer.getCode(), parentCode);
                        }

                        if (!existDealers.containsKey(currentDealer.getCode())) {
                            existDealers.put(currentDealer.getCode(), currentDealer);
                        }
                        else {
                            try {
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                response.getWriter().println(new GsonHelper(new ExcelError(currentDealer.getCode(),
                                        ExcelError.EXCEL_ERROR_DEALER_ALREADY_EXISTS)));
                            }
                            catch (IOException exception) {}

                            return;
                        }
                    }
                }

                for (Map.Entry<String, String> entry : wantParentDealers.entrySet()) {
                    String dealerCode = entry.getKey();
                    String parentCode = entry.getValue();

                    if (existDealers.containsKey(dealerCode) && existDealers.containsKey(parentCode)) {
                        existDealers.get(dealerCode).setParentDealer(existDealers.get(parentCode));
                        existDealers.get(parentCode).addSubDealer(existDealers.get(dealerCode));
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        try {
                            if (!existDealers.containsKey(dealerCode)) {
                                response.getWriter().println(new GsonHelper(new ExcelError(dealerCode,
                                        ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));
                            }
                            if (!existDealers.containsKey(parentCode)) {
                                response.getWriter().println(new GsonHelper(new ExcelError(parentCode,
                                        ExcelError.EXCEL_ERROR_DEALER_NOT_FOUND)));
                            }
                        }
                        catch (IOException e) {}

                        return;
                    }
                }

                for (Dealer dealer : existDealers.values()) {
                    if (!hasCircle(existDealers, dealer.getCode())) {
                        dealerService.save(dealer);
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ExcelError(dealer.getCode(),
                                ExcelError.EXCEL_ERROR_OWN_PARENT)));
                        return;
                    }
                }

                response.setStatus(HttpServletResponse.SC_OK);
                //response.getWriter().println(new GsonHelper(existDealers.values()));
            }
            finally {
                fileService.deleteFile(filename);
            }
        }
    }

    private boolean hasCircle(Map<String, Dealer> dealerMap, String dealerCode) {
        if (dealerMap.containsKey(dealerCode)) {
            Dealer dealer = dealerMap.get(dealerCode);

            if (dealer.getParentDealer() != null && dealer.getParentDealer().getCode().equals(dealerCode)) {
                return true;
            }

            for (Dealer subDealer : getSubDealers(dealerMap, dealerCode)) {
                if (subDealer.getCode().equals(dealerCode) || hasCircle(dealerMap, subDealer.getCode(), dealerCode)) {
                    return true;
                }

            }
        }
        return false;
    }

    private boolean hasCircle(Map<String, Dealer> dealerMap, String dealerCode, String checkCode) {
        for (Dealer subDealer : getSubDealers(dealerMap, dealerCode)) {
            if (subDealer.getCode().equals(checkCode) || hasCircle(dealerMap, subDealer.getCode(), checkCode)) {
                MyLogger.log(String.format("Current dealer: %s, dealer to check: %s", subDealer.getCode(), checkCode));
                return true;
            }
        }

        return false;
    }


    private List<Dealer> getSubDealers(Map<String, Dealer> dealerMap, String dealerCode) {
        return dealerMap.values().stream()
                .filter(dealer -> dealer.getParentDealer() != null &&
                        dealer.getParentDealer().getCode().equals(dealerCode))
                .collect(Collectors.toList());
    }
}
