package servlets.auth;

import database.DBServiceSingleton;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogOutServlet extends HttpServlet {
    public static final String URL = "/logout";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        if (AuthenticationServiceSingleton.getInstance().logoutPerson(request.getHeader("Session"))) {
            response.setStatus(HttpServletResponse.SC_OK);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.NOT_AUTHORIZED)));
        }
    }
}
