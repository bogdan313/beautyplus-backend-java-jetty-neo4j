package servlets.auth;

import database.domains.Person;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckUserByLoginServlet extends HttpServlet {
    public static final String URL = "/check-user-by-login";

    public void doPost(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        String login = request.getParameter("login");

        if (login == null || login.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        if (personService.existsByLogin(login.trim())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_ALREADY_EXISTS)));
            return;
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }
}
