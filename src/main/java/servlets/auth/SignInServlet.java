package servlets.auth;

import database.domains.Person;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class SignInServlet extends HttpServlet {
    public static final String URL = "/sign-in";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final PersonService<Person> personService = new PersonService<>(Person.class);
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (login == null || password == null ||
                login.trim().isEmpty() || password.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        Person person = personService.getByLogin(login.trim());
        if (person != null) {
            if (!person.getPassword().equals(password.trim())) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PASSWORD)));
                return;
            }
            String sessionId = UUID.randomUUID().toString();
            if (AuthenticationServiceSingleton.getInstance().signInPerson(sessionId, person)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.addHeader("Session", sessionId);
                response.addHeader("redirect", String.format("/%s/%s", sessionId, person.getRole().toLowerCase()));
                response.getWriter().println(new GsonHelper(person));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.NOT_AUTHORIZED)));
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
        }
    }
}
