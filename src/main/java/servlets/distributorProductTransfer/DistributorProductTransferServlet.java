package servlets.distributorProductTransfer;

import database.domains.DistributorProductTransfer;
import database.domains.ProductDescription;
import database.services.DistributorProductTransferService;
import database.services.ProductDescriptionService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import servlets.ServletWithAuthorTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DistributorProductTransferServlet extends ServletWithAuthorTemplate<DistributorProductTransfer, DistributorProductTransferService> {
    public DistributorProductTransferServlet() {
        super(DistributorProductTransferService.class, DistributorProductTransfer.class);
    }

    @SuppressWarnings("unchecked")
    public void doDelete(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        try {
            long id = Long.parseLong(request.getParameter("id"));
            DistributorProductTransferService distributorProductTransferService =
                    new DistributorProductTransferService();

            distributorProductTransferService.setSessionId(request.getHeader("Session"));
            DistributorProductTransfer distributorProductTransfer = distributorProductTransferService.getById(id);
            if (distributorProductTransfer != null) {
                ProductDescriptionService productDescriptionService = new ProductDescriptionService();
                productDescriptionService.setSession(distributorProductTransferService.getSession());
                for (ProductDescription item : distributorProductTransfer.getProducts()) {
                    productDescriptionService.delete(item);
                }
                if (distributorProductTransferService.delete(distributorProductTransfer))
                    response.setStatus(HttpServletResponse.SC_OK);
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
