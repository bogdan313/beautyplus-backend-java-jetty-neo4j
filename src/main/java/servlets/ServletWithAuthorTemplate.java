package servlets;

import database.DBServiceSingleton;
import database.domains.DomainWithAuthor;
import database.services.BaseService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ServletWithAuthorTemplate<T extends DomainWithAuthor, S extends BaseService<T>> extends HttpServlet {
    public static final String URL = "/";
    private final Class<T> clazz;
    private final Class<S> serviceClazz;

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        try {
            S service = this.serviceClazz.newInstance();
            String id = request.getParameter("id");

            service.setSessionId(request.getHeader("Session"));

            if (id == null || id.trim().isEmpty()) {
                Map<String, Object> result = new HashMap<>();

                result.put("data", service.getAll(request.getParameterMap()));
                result.put("totalRecords", "0");

                response.getWriter().println(new GsonHelper(result));
            }
            else {
                try {
                    T model = service.getById(Long.parseLong(id));
                    if (model != null) {
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().println(new GsonHelper(model));
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                    }
                }
                catch (NumberFormatException exception) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                }
            }
        }
        catch (InstantiationException | IllegalAccessException ignore) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        try {
            S service = this.serviceClazz.newInstance();
            service.setSessionId(request.getHeader("Session"));

            try {
                T model = this.clazz.newInstance();
                if (model.setParameters(request.getParameterMap())) {
                    model.setAuthor(AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session")));
                    model.setCreatedDate();
                    if (service.save(model)) {
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().println(new GsonHelper(model));
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                    }
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                }
            }
            catch (IllegalAccessException|InstantiationException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
            }
        }
        catch (InstantiationException | IllegalAccessException ignore) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
        }
    }

    public void doPut(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        try {
            S service = this.serviceClazz.newInstance();
            String id = request.getParameter("id");

            service.setSessionId(request.getHeader("Session"));

            if (id == null || id.trim().isEmpty()) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            try {
                T model = service.getById(Long.parseLong(id.trim()));
                if (model == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                    return;
                }
                if (model.setParameters(request.getParameterMap())) {
                    model.setUpdatedBy(AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session")));
                    model.setUpdatedDate();
                    if (service.save(model)) {
                        response.setStatus(HttpServletResponse.SC_OK);
                        response.getWriter().println(new GsonHelper(model));
                    }
                    else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                    }
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
        catch (InstantiationException | IllegalAccessException ignore) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        try {
            S service = this.serviceClazz.newInstance();
            String id = request.getParameter("id");

            service.setSessionId(request.getHeader("Session"));

            if (id == null || id.trim().isEmpty()) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            try {
                T model = service.getById(Long.parseLong(id.trim()));
                if (model == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                    return;
                }
                if (service.delete(model)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
        catch (InstantiationException | IllegalAccessException ignore) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.SERVER_ERROR)));
        }
    }

    public ServletWithAuthorTemplate(Class<S> serviceClazz, Class<T> clazz) {
        this.serviceClazz = serviceClazz;
        this.clazz = clazz;
    }
}
