package servlets.distributorProductOrder;

import database.domains.DistributorProductOrder;
import database.services.DistributorProductOrderService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AcceptServlet extends HttpServlet {
    public static final String URL = "/accept";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DistributorProductOrderService distributorProductOrderService = new DistributorProductOrderService();
        String id = request.getParameter("id");

        distributorProductOrderService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));

            return;
        }

        try {
            DistributorProductOrder distributorProductOrder = distributorProductOrderService.getById(Long.parseLong(id));

            if (distributorProductOrder == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));

                return;
            }

            if (distributorProductOrderService.changeStatus(distributorProductOrder, Constants.STATUSES.ACCEPTED.getValue())) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(distributorProductOrder));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
