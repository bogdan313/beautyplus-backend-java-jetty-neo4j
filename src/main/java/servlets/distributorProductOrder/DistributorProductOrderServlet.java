package servlets.distributorProductOrder;

import database.domains.DistributorProductOrder;
import database.domains.ProductDescription;
import database.services.DistributorProductOrderService;
import database.services.ProductDescriptionService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import servlets.ServletWithAuthorTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DistributorProductOrderServlet extends ServletWithAuthorTemplate<DistributorProductOrder, DistributorProductOrderService> {
    public DistributorProductOrderServlet() {
        super(DistributorProductOrderService.class, DistributorProductOrder.class);
    }

    @SuppressWarnings("unchecked")
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        try {
            long id = Long.parseLong(request.getParameter("id"));
            final DistributorProductOrderService distributorProductOrderService = new DistributorProductOrderService();

            distributorProductOrderService.setSessionId(request.getHeader("Session"));
            DistributorProductOrder distributorProductOrder = distributorProductOrderService.getById(id);

            if (distributorProductOrder != null) {
                ProductDescriptionService productDescriptionService = new ProductDescriptionService();
                productDescriptionService.setSession(distributorProductOrderService.getSession());
                for (ProductDescription item : distributorProductOrder.getProducts()) {
                    productDescriptionService.delete(item);
                }
                if (distributorProductOrderService.delete(distributorProductOrder))
                    response.setStatus(HttpServletResponse.SC_OK);
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
