package servlets.product;

import database.services.ProductService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckProductByTitleServlet extends HttpServlet {
    public static final String URL = "/check-product-by-title";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        String title = request.getParameter("title");
        final ProductService productService = new ProductService();

        if (title == null || title.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        if (productService.existsByTitle(title.trim())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_EXISTS)));
            return;
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }
}
