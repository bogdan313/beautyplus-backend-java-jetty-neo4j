package servlets.product;

import database.domains.Product;
import database.services.ProductService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.FileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProductPictureServlet extends HttpServlet {
    public static final String URL = "/product-picture";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();
        String base64 = request.getParameter("picture");

        productService.setSessionId(request.getHeader("Session"));

        if (base64 == null || base64.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        try {
            long id = Long.parseLong(request.getParameter("id"));
            Product product = productService.getById(id);
            if (product == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                return;
            }
            FileService fileService = new FileService();
            String newPhoto = fileService.saveImageFromBase64ToPng(base64);
            if (newPhoto != null) {
                product.setPicture(newPhoto);
                if (productService.save(product)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(product));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
