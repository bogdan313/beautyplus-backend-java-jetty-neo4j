package servlets.product;

import database.DBServiceSingleton;
import database.domains.Person;
import database.domains.Product;
import database.services.ProductService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProductServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final ProductService productService = new ProductService();
        String id = request.getParameter("id");

        productService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            Map<String, Object> result = new HashMap<>();

            result.put("data", productService.getAll(request.getParameterMap()));
            result.put("totalRecords", "0");

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            try {
                Product product = productService.getById(Long.parseLong(id));
                if (product == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(product));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        final ProductService productService = new ProductService();
        String code = request.getParameter("code");
        String title = request.getParameter("title");

        productService.setSessionId(request.getHeader("Session"));
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session"));

        if (code == null || code.trim().isEmpty() ||
                title == null || title.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        if (productService.existsByTitleOrCode(title.trim(), code.trim())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_EXISTS)));
            return;
        }

        Product product = new Product();
        if (!product.setParameters(request.getParameterMap())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        product.setAuthor(currentPerson);
        product.setCreatedDate();
        if (productService.save(product)) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(product));
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
        }
    }

    public void doPut(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();

        try {
            Long id = Long.parseLong(request.getParameter("id"));
            productService.setSessionId(request.getHeader("Session"));
            Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session"));
            if (id < 0) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
            Product product = productService.getById(id);
            if (product == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
            String code = request.getParameter("code");
            String title = request.getParameter("title");
            if (code == null || code.trim().isEmpty() ||
                    title == null || title.trim().isEmpty()) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            Product tmpProduct= productService.getByTitleOrCode(title.trim(), code.trim());
            if (tmpProduct != null && !product.getId().equals(tmpProduct.getId())) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_EXISTS)));
                return;
            }
            if (!product.setParameters(request.getParameterMap())) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            product.setUpdatedBy(currentPerson);
            product.setUpdatedDate();
            if (productService.save(product)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(product));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();

        try {
            Long id = Long.parseLong(request.getParameter("id"));
            productService.setSessionId(request.getHeader("Session"));
            Product product = productService.getById(id);
            if (product == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
            }
            if (productService.delete(product))
                response.setStatus(HttpServletResponse.SC_OK);
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
