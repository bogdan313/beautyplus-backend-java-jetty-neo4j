package servlets.administrator;

import database.domains.Administrator;
import database.domains.Person;
import database.domains.Rules;
import database.services.PersonService;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class GenerateAdminServlet extends HttpServlet {
    public static final String URL = "/generate-admin";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        final PersonService<Administrator> administratorPersonService = new PersonService<>(Administrator.class);

        Collection<Administrator> admins = administratorPersonService.getByRole(Person.ROLE.ADMINISTRATOR.getValue());

        if (admins.isEmpty()) {
            Administrator admin = new Administrator();
            admin.setLogin("admin");
            admin.setPassword("admin");
            admin.setFullName("Admin");
            admin.setCode("001");
            admin.setRole(Person.ROLE.ADMINISTRATOR.getValue());
            Rules rules = new Rules();
            rules.adminRules();
            admin.setRules(rules);
            administratorPersonService.getSession().save(admin);
            response.setStatus(HttpServletResponse.SC_OK);
            Collection<Administrator> newAdmins = new ArrayList<>();
            newAdmins.add(admin);
            response.getWriter().println(new GsonHelper(newAdmins));
        }
        else {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(admins));
        }
    }
}