package servlets.dealer;

import database.domains.Dealer;
import database.domains.Person;
import database.services.DealerService;
import database.services.PersonService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DealerServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final DealerService dealerService = new DealerService();
        String id = request.getParameter("id");

        dealerService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            Map<String, Object> result = new HashMap<>();

            result.put("data", dealerService.getAll(request.getParameterMap()));
            result.put("totalRecords", "0");

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            try {
                Dealer dealer = dealerService.getById(Long.parseLong(id));
                if (dealer == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
                }
                else response.getWriter().println(new GsonHelper(dealer));
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        final PersonService<Person> personService = new PersonService<>(Person.class);
        Dealer dealer = new Dealer();

        if (dealer.setParameters(request.getParameterMap())) {
            if (personService.existsByLoginOrCode(dealer.getLogin(), dealer.getCode())) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_ALREADY_EXISTS)));
                return;
            }

            String parentDealer = request.getParameter("dealer");
            if (parentDealer != null && !parentDealer.trim().isEmpty()) {
                Dealer parent = dealerService.getByCode(parentDealer.trim());
                if (parent != null) dealer.setParentDealer(parent);
            }

            if (dealerService.save(dealer)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(dealer));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();

        try {
            long id = Long.parseLong(request.getParameter("id"));
            dealerService.setSessionId(request.getHeader("Session"));
            Dealer dealer = dealerService.getById(id);

            if (dealer == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            }
            else {
                Dealer parentDealer = dealer.getParentDealer();
                for (Dealer subDealer : dealer.getSubDealers()) {
                    subDealer.setParentDealer(parentDealer);
                    if (!dealerService.save(subDealer)) {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                        return;
                    }
                }

                if (dealerService.delete(dealer)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }
}
