package servlets.dealer;

import database.services.DealerService;
import database.domains.Dealer;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangeParentServlet extends HttpServlet {
    public static final String URL = "/change-parent";

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        String whoCode = request.getParameter("who");
        String toCode = request.getParameter("to");

        dealerService.setSessionId(request.getHeader("Session"));

        if (whoCode == null || toCode == null ||
                whoCode.trim().isEmpty() || toCode.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }

        Dealer whoDealer = dealerService.getByCode(whoCode.trim());
        Dealer toDealer = dealerService.getByCode(toCode.trim());

        if (whoDealer == null || toDealer == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            return;
        }

        if (dealerService.hasSubDealerByCode(whoCode, toCode) ||
                dealerService.hasSubDealerByCode(toCode, whoCode)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
        else {
            whoDealer.setParentDealer(toDealer);
            if (dealerService.save(whoDealer)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(new GsonHelper(whoDealer));
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
    }

    public void doDelete(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final DealerService dealerService = new DealerService();
        dealerService.setSessionId(request.getHeader("Session"));
        String code = request.getParameter("code");

        if (code == null || code.trim().isEmpty()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            return;
        }
        Dealer dealer = dealerService.getByCode(code.trim());
        if (dealer == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.USER_DOES_NOT_EXIST)));
            return;
        }

        dealer.removeParentDealer();
        if (dealerService.save(dealer)) {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(new GsonHelper(dealer));
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
        }
    }
}