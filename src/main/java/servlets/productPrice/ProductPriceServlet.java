package servlets.productPrice;

import database.DBServiceSingleton;
import database.domains.Product;
import database.domains.ProductPrice;
import database.services.ProductPriceService;
import database.services.ProductService;
import errorMessages.ErrorMessage;
import helpers.Constants;
import helpers.GsonHelper;
import helpers.ParseParametersHelper;
import services.AuthenticationServiceSingleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProductPriceServlet extends HttpServlet {
    public static final String URL = "/";

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);

        final ProductPriceService productPriceService = new ProductPriceService();
        String id = request.getParameter("id");

        productPriceService.setSessionId(request.getHeader("Session"));

        if (id == null || id.trim().isEmpty()) {
            Map<String, Object> result = new HashMap<>();

            result.put("data", productPriceService.getAll(request.getParameterMap()));
            result.put("totalRecords", "0");

            response.getWriter().println(new GsonHelper(result));
        }
        else {
            try {
                ProductPrice productPrice = productPriceService.getById(Long.parseLong(id));
                if (productPrice == null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(productPrice));
                }
            }
            catch (NumberFormatException exception) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();
        final ProductPriceService productPriceService = new ProductPriceService();
        ProductPrice productPrice = new ProductPrice();

        productService.setSession(productPriceService.getSession());
        productPriceService.setSessionId(request.getHeader("Session"));
        productService.setSessionId(request.getHeader("Session"));

        try {
            Map<String, Object> productParameters = (Map<String, Object>) ParseParametersHelper.parse(request.getParameterMap()).getOrDefault("product", null);
            if (!productParameters.containsKey("id")) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
                return;
            }
            Product product = productService.getById(Long.parseLong(productParameters.getOrDefault("id", -1).toString()));
            if (product == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                return;
            }
            if (productPrice.setParameters(request.getParameterMap())) {
                productPrice.setProduct(product);
                productPrice.setAuthor(AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session")));
                productPrice.setCreatedDate();
                if (productPriceService.save(productPrice)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(productPrice));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doPut(HttpServletRequest request,
                      HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();
        final ProductPriceService productPriceService = new ProductPriceService();

        productService.setSession(productPriceService.getSession());
        productPriceService.setSessionId(request.getHeader("Session"));
        productService.setSessionId(request.getHeader("Session"));

        try {
            ProductPrice productPrice = productPriceService.getById(Long.parseLong(request.getParameter("id")));
            if (productPrice == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                return;
            }
            if (productPrice.setParameters(request.getParameterMap())) {
                productPrice.setUpdatedBy(AuthenticationServiceSingleton.getInstance().getCurrentPerson(request.getHeader("Session")));
                productPrice.setUpdatedDate();
                if (productPriceService.save(productPrice)) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().println(new GsonHelper(productPrice));
                }
                else {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
                }
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.INCORRECT_PARAMETERS)));
        }
    }

    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {
        response.setContentType(Constants.CONTENT_TYPE_JSON);

        final ProductService productService = new ProductService();
        final ProductPriceService productPriceService = new ProductPriceService();

        productPriceService.setSessionId(request.getHeader("Session"));
        productService.setSessionId(request.getHeader("Session"));

        try {
            ProductPrice productPrice = productPriceService.getById(Long.parseLong(request.getParameter("id")));
            if (productPrice == null) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.RECORD_NOT_FOUND)));
                return;
            }
            if (productPriceService.delete(productPrice)) {
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().println(new GsonHelper(new ErrorMessage(ErrorMessage.PERMISSIONS_DENIED)));
            }
        }
        catch (NumberFormatException exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
