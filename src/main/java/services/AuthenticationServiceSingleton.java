package services;

import database.domains.Person;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AuthenticationServiceSingleton {
    private static AuthenticationServiceSingleton ourInstance = new AuthenticationServiceSingleton();
    private ConcurrentHashMap<String, Person> authenticatedPersons = new ConcurrentHashMap<>();

    public static AuthenticationServiceSingleton getInstance() {
        return ourInstance;
    }

    private AuthenticationServiceSingleton() {
    }

    public boolean signInPerson(String sessionId, Person person) {
        if (sessionId == null || sessionId.trim().isEmpty() || person == null)
            return false;

        this.authenticatedPersons.put(sessionId, person);
        return true;
    }

    public boolean logoutPerson(String sessionId) {
        if (sessionId == null || sessionId.trim().isEmpty()) return false;
        if (this.authenticatedPersons.containsKey(sessionId)) {
            this.authenticatedPersons.remove(sessionId);
            return true;
        }
        return false;
    }

    public Collection<Person> getOnline() {
        return this.authenticatedPersons.values();
    }

    public boolean isAuthenticated(String sessionId) {
        return sessionId != null && !sessionId.isEmpty() &&
                this.authenticatedPersons.containsKey(sessionId);
    }

    public Person getCurrentPerson(String sessionId) throws NullPointerException {
        return this.isAuthenticated(sessionId)
                ? this.authenticatedPersons.get(sessionId)
                : null;
    }
}
