package services;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Properties;
import java.util.UUID;

public class FileService {
    private final String UPLOAD_DIR;
    private final String PRE_FILE_URL;

    public FileService() throws IOException {
        try (InputStream propertiesFile = this.getClass().getClassLoader().getResourceAsStream("fileservice.properties")) {
            Properties properties = new Properties();
            properties.load(propertiesFile);
            this.UPLOAD_DIR = properties.getProperty("upload_dir", "/home/bogdan/WebstormProjects/assets/upload/");
            this.PRE_FILE_URL = properties.getProperty("pre_file_url", "/assets/upload/");
        }
    }

    public String saveImageFromPart(String filename, int width, int height) {
        try {
            BufferedImage originalImage = ImageIO.read(new File(this.UPLOAD_DIR, this.getFileName(filename)));
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            BufferedImage resizedImage = new BufferedImage(width, height, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(resizedImage, 0, 0, width, height, null);
            g.dispose();
            ImageIO.write(resizedImage, this.getExtension(filename), new File(this.UPLOAD_DIR, filename));
            return String.format("%s%s", this.PRE_FILE_URL, filename);
        }
        catch (IOException exception) {
            return null;
        }
    }

    public String saveImageFromPart(Part image) throws IOException {
        if (image.getContentType().contains("image")) {
            String filename = this.saveFile(image);
            return this.saveImageFromPart(filename, 256, 256);
        }
        return null;
    }

    public String saveImageFromPart(Part[] image) throws IOException {
        for (Part img : image) {
            if (img.getContentType().contains("image")) {
                return this.saveImageFromPart(img);
            }
        }
        return null;
    }

    public String saveImageFromBase64ToPng(String base64) {
        String filename = String.format("%s.png", UUID.randomUUID().toString());
        File file = new File(this.UPLOAD_DIR, filename);
        if (!file.exists()) {
            try {
                String[] base64Arr = base64.split(",");
                if (base64Arr.length < 2) return null;
                byte[] base64decoded = Base64.decodeBase64(base64Arr[1]);
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(base64decoded));
                if (ImageIO.write(bufferedImage, "png", file))
                    return String.format("%s%s", this.PRE_FILE_URL, filename);
                else return null;
            }
            catch (IOException exception) {
                return null;
            }
        }
        return null;
    }

    public String saveUploadedFile(Part file) {
        try {
            String filename = this.saveFile(file);
            return String.format("%s%s", this.PRE_FILE_URL, filename);
        }
        catch (IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public boolean deleteFile(String fileName) {
        File file = new File(String.format("%s%s", this.UPLOAD_DIR, this.getFileName(fileName)));
        return file.exists() && file.isFile() && file.delete();
    }

    public String getAbsolutePath(String fileName) {
        return this.UPLOAD_DIR + this.getFileName(fileName);
    }

    public String getRelativePath(String filename) {
        return this.PRE_FILE_URL + this.getFileName(filename);
    }

    private String saveFile(Part file) throws IOException {
        try {
            String fileName = String.format("%s_%s", UUID.randomUUID().toString(), file.getName());
            file.write(fileName);
            Files.move(
                    Paths.get(String.format("/tmp/upload/%s", fileName)),
                    Paths.get(String.format("%s%s", this.UPLOAD_DIR, fileName)),
                    StandardCopyOption.REPLACE_EXISTING
            );
            return fileName;
        }
        catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    private String getExtension(String filename) {
        return FilenameUtils.getExtension(filename);
    }

    private String getFileName(String fileName) {
        if (fileName.contains(this.PRE_FILE_URL)) {
            fileName = fileName.replace(this.PRE_FILE_URL, "");
        }
        return fileName;
    }
}
