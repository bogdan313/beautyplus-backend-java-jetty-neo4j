package database.relationships;

import database.domains.Domain;
import database.domains.Warehouse;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = WarehouseRelationship.TYPE)
public class WarehouseRelationship extends Relationship {
    public static final String TYPE = "WAREHOUSE";
    @StartNode
    private Domain startNode;
    @EndNode
    private Warehouse warehouse;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public Warehouse getWarehouse() {
        return this.warehouse;
    }
    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }
}
