package database.relationships;

import database.domains.Domain;
import database.domains.Person;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = AuthorRelationship.TYPE)
public class AuthorRelationship extends Relationship {
    public static final String TYPE = "AUTHOR";

    @StartNode
    private Domain startNode;

    @EndNode
    private Person author;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public Person getAuthor() {
        return this.author;
    }
    public void setAuthor(Person author) {
        this.author = author;
    }
}
