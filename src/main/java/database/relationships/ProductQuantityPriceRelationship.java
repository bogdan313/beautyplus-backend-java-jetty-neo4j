package database.relationships;

import database.domains.Domain;
import database.domains.ProductQuantityPrice;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = ProductQuantityPriceRelationship.TYPE)
public class ProductQuantityPriceRelationship {
    public static final String TYPE = "PRODUCTQUANTITYPRICE";

    @StartNode
    private Domain startNode;
    @EndNode
    private ProductQuantityPrice productQuantityPrice;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public ProductQuantityPrice getProductQuantityPrice() {
        return this.productQuantityPrice;
    }
    public void setProductQuantityPrice(ProductQuantityPrice productQuantityPrice) {
        this.productQuantityPrice = productQuantityPrice;
    }
}
