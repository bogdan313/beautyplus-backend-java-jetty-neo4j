package database.relationships;

import com.google.gson.annotations.Expose;
import database.domains.DealerProductOrder;
import database.domains.Domain;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = DealerProductOrderRelationship.TYPE)
public class DealerProductOrderRelationship extends Relationship {
    public static final String TYPE = "DEALER_PRODUCT_ORDER";

    @Expose
    @StartNode
    private Domain startNode;

    @Expose
    @EndNode
    private DealerProductOrder dealerProductOrder;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public DealerProductOrder getDealerProductOrder() {
        return this.dealerProductOrder;
    }
    public void setDealerProductOrder(DealerProductOrder dealerProductOrder) {
        this.dealerProductOrder = dealerProductOrder;
    }
}
