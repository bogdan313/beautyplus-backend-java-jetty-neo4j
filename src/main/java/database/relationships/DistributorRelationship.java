package database.relationships;

import database.domains.Domain;
import database.domains.Distributor;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = DistributorRelationship.TYPE)
public class DistributorRelationship extends Relationship {
    public static final String TYPE = "DISTRIBUTOR";

    @StartNode
    private Domain startNode;
    @EndNode
    private Distributor distributor;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public Distributor getDistributor() {
        return this.distributor;
    }
    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }
}
