package database.relationships;

import com.google.gson.annotations.Expose;
import database.domains.Person;
import database.domains.Rules;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = RuleRelationship.TYPE)
public class RuleRelationship extends Relationship {
    public static final String TYPE = "RULE";

    @Expose
    @StartNode
    private Person person;
    @Expose @EndNode
    private Rules rules;

    public Person getPerson() {
        return this.person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }

    public Rules getRules() {
        return this.rules;
    }
    public void setRules(Rules rules) {
        this.rules = rules;
    }
}
