package database.relationships;

import database.domains.DistributorProductOrder;
import database.domains.Domain;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = DistributorProductOrderRelationship.TYPE)
public class DistributorProductOrderRelationship extends Relationship {
    public static final String TYPE = "DISTRIBUTORPRODUCTORDER";

    @StartNode
    private Domain startNode;

    @EndNode
    private DistributorProductOrder distributorProductOrder;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public DistributorProductOrder getDistributorProductOrder() {
        return this.distributorProductOrder;
    }
    public void setDistributorProductOrder(DistributorProductOrder distributorProductOrder) {
        this.distributorProductOrder = distributorProductOrder;
    }
}
