package database.relationships;

import com.google.gson.annotations.Expose;
import database.domains.Dealer;
import database.domains.Person;
import org.neo4j.ogm.annotation.*;

@RelationshipEntity(type = SubDealerRelationship.TYPE)
public class SubDealerRelationship extends Relationship {
    public static final String TYPE = "SUBDEALER";

    @Expose
    @StartNode
    private Dealer subDealer;
    @EndNode
    private Dealer dealer;

    public Dealer getSubDealer() {
        return this.subDealer;
    }
    public void setSubDealer(Dealer dealer) {
        this.subDealer = dealer;
    }

    public Person getDealer() {
        return this.dealer;
    }
    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }
}
