package database.relationships;

import database.domains.Domain;
import database.domains.ProductDescription;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = ProductDescriptionRelationship.TYPE)
public class ProductDescriptionRelationship extends Relationship {
    public static final String TYPE = "PRODUCTDESCRIPTION";

    @StartNode private Domain startNode;
    @EndNode private ProductDescription endNode;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public ProductDescription getEndNode() {
        return this.endNode;
    }
    public void setEndNode(ProductDescription endNode) {
        this.endNode = endNode;
    }
}
