package database.relationships;

import database.domains.Product;
import database.domains.ProductPrice;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = ProductPriceRelationship.TYPE)
public class ProductPriceRelationship {
    public static final String TYPE = "PRODUCTPRICE";

    @StartNode
    private Product product;
    @EndNode
    private ProductPrice price;

    public Product getProduct() {
        return this.product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductPrice getPrice() {
        return this.price;
    }
    public void setPrice(ProductPrice price) {
        this.price = price;
    }
}
