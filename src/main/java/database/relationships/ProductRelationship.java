package database.relationships;

import database.domains.Domain;
import database.domains.Product;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = ProductRelationship.TYPE)
public class ProductRelationship extends Relationship {
    public static final String TYPE = "PRODUCT";

    @StartNode
    private Domain startNode;
    @EndNode
    private Product product;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public Product getProduct() {
        return this.product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
}
