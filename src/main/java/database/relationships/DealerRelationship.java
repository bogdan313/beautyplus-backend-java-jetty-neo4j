package database.relationships;

import database.domains.Dealer;
import database.domains.Domain;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = DealerRelationship.TYPE)
public class DealerRelationship extends Relationship {
    public static final String TYPE = "DEALER";

    @StartNode
    private Domain startNode;
    @EndNode
    private Dealer dealer;

    public Domain getStartNode() {
        return this.startNode;
    }
    public void setStartNode(Domain startNode) {
        this.startNode = startNode;
    }

    public Dealer getDealer() {
        return this.dealer;
    }
    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }
}
