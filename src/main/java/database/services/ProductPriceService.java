package database.services;

import database.domains.ProductPrice;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class ProductPriceService extends BaseService<ProductPrice> {
    public ProductPriceService() {
        super(ProductPrice.class);
    }

    @Override
    public Collection<ProductPrice> getAll() {
        return super.getAll(new SortOrder().add(SortOrder.Direction.DESC, "datetime"));
    }
}
