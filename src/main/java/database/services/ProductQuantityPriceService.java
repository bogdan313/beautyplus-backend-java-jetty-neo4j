package database.services;

import database.domains.ProductQuantityPrice;

public class ProductQuantityPriceService extends BaseService<ProductQuantityPrice> {
    public ProductQuantityPriceService() {
        super(ProductQuantityPrice.class);
    }
}
