package database.services;

import database.domains.Gift;

public class GiftService extends BaseService<Gift> {
    public GiftService() {
        super(Gift.class);
    }
}
