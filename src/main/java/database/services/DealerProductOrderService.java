package database.services;

import database.domains.DealerProductOrder;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class DealerProductOrderService extends BaseService<DealerProductOrder> {
    public DealerProductOrderService() {
        super(DealerProductOrder.class);
    }

    public DealerProductOrderService(String sessionId) {
        this();
        this.setSessionId(sessionId);
    }

    @Override
    public Collection<DealerProductOrder> getAll() {
        return super.getAll(new SortOrder().add("code"), -1, 2);
    }

    @Override
    public DealerProductOrder getById(long id) {
        return this.getById(id, 2);
    }

    public boolean changeStatus(DealerProductOrder dealerProductOrder, String status) {
        if (!dealerProductOrder.availableAccept(this.getSessionId()) ||
                (!status.equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                        !status.equals(Constants.STATUSES.DECLINED.getValue())))
            return false;

        dealerProductOrder.setStatus(status);
        this.getSession().save(dealerProductOrder);
        return true;
    }

    @Override
    public boolean save(DealerProductOrder object) {
        if (object.getNumber() <= 0)
            object.setNumber(Increment.getInstance().getDealerProductOrders().getAndIncrement());

        return super.save(object);
    }
}
