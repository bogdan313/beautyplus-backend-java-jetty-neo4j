package database.services;

import database.domains.Person;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.query.SortOrder;
import org.neo4j.ogm.session.Session;

import java.util.Collection;

public class PersonService<T extends Person> extends BaseService<T> {
    public PersonService(Class<T> clazz) {
        super(clazz);
    }

    public Collection<T> getAll() {
        return super.getAll(new SortOrder().add("name"));
    }

    public T getByCode(String code) {
        Filter filter = new Filter("code", ComparisonOperator.EQUALS, code);
        return this.getOneByFilter(filter);
    }

    public T getByLogin(String login) {
        Filter filter = new Filter("login", ComparisonOperator.EQUALS, login);
        return this.getOneByFilter(filter);
    }

    public T getByLoginOrCode(String login, String code) {
        Filter loginFilter = new Filter("login", ComparisonOperator.EQUALS, login);
        Filter codeFilter = new Filter("code", ComparisonOperator.EQUALS, code);
        return this.getOneByFilter(loginFilter.or(codeFilter));
    }

    public boolean existsByCode(String code) {
        Filter filter = new Filter("code", ComparisonOperator.EQUALS, code);
        return this.isExists(filter);
    }

    public boolean existsByLogin(String login) {
        Filter filter = new Filter("login", ComparisonOperator.EQUALS, login);
        return this.isExists(filter);
    }

    public boolean existsByLoginOrCode(String login, String code) {
        Filter loginFilter = new Filter("login", ComparisonOperator.EQUALS, login);
        Filter codeFilter = new Filter("code", ComparisonOperator.EQUALS, code);
        return this.isExists(loginFilter.or(codeFilter));
    }

    public Collection<T> getByRole(String role) {
        Filter filter = new Filter("role", ComparisonOperator.EQUALS, role);
        return this.getByFilter(filter);
    }
}
