package database.services;

import database.domains.Distributor;

public class DistributorService extends BaseService<Distributor> {
    public DistributorService() {
        super(Distributor.class);
    }
}
