package database.services;

import database.domains.Dealer;
import database.relationships.SubDealerRelationship;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.Filters;
import org.neo4j.ogm.model.Result;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DealerService extends BaseService<Dealer> {
    public DealerService() {
        super(Dealer.class);
    }

    public Dealer getByCode(String code) {
        Filter filter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.getOneByFilter(filter);
    }

    public Dealer getByLoginOrCode(String login, String code) {
        Filter loginFilter = new Filter("login", ComparisonOperator.LIKE, login);
        Filter codeFilter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.getOneByFilter(loginFilter.or(codeFilter));
    }

    public boolean hasSubDealerByCode(String who, String to) {
        String queryString = String.format(
                "MATCH (who:Dealer)-[:%s*]->(to:Dealer) WHERE who.code = '%s' AND to.code = '%s' RETURN COUNT(*) as count",
                SubDealerRelationship.TYPE,
                who, to
        );
        Map<String, Object> result = new HashMap<>();
        Result neoResult = this.getSession().query(queryString, Collections.emptyMap());
        if (neoResult.iterator().hasNext())
            result = neoResult.iterator().next();
        if (result.containsKey("count")) {
            try {
                return Integer.parseInt(result.get("count").toString()) > 0;
            }
            catch (NumberFormatException exception) {
                return true;
            }
        }
        return true;
    }

    public Iterable<Dealer> getDealersWithoutParent() {
        String queryString = "MATCH (d:Dealer) WHERE NOT (d)-[:SUBDEALER]->() return d";
        return this.getSession().query(Dealer.class, queryString, new HashMap<>());
    }
}
