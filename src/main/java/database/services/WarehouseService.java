package database.services;

import database.domains.Warehouse;

public class WarehouseService extends BaseService<Warehouse> {
    public WarehouseService() {
        super(Warehouse.class);
    }
}
