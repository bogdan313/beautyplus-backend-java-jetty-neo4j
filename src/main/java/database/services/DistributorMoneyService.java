package database.services;

import database.domains.DistributorMoney;

public class DistributorMoneyService extends BaseService<DistributorMoney> {
    public DistributorMoneyService() {
        super(DistributorMoney.class);
    }
}
