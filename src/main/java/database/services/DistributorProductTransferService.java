package database.services;

import database.domains.DistributorProductTransfer;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class DistributorProductTransferService extends BaseService<DistributorProductTransfer> {
    public DistributorProductTransferService() {
        super(DistributorProductTransfer.class);
    }

    @Override
    public Collection<DistributorProductTransfer> getAll() {
        return super.getAll(new SortOrder().add("code"), -1, 2);
    }

    @Override
    public DistributorProductTransfer getById(long id) {
        return super.getById(id, 2);
    }

    public boolean changeStatus(DistributorProductTransfer distributorProductTransfer, String status) {
        if (!distributorProductTransfer.availableAccept(this.getSessionId()) ||
                (!status.equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                        !status.equals(Constants.STATUSES.DECLINED.getValue())))
            return false;

        distributorProductTransfer.setStatus(status);
        this.getSession().save(distributorProductTransfer);
        return true;
    }

    @Override
    public boolean save(DistributorProductTransfer object) {
        if (object.getNumber() <= 0)
            object.setNumber(Increment.getInstance().getDistributorProductTransfers().getAndIncrement());

        return super.save(object);
    }
}
