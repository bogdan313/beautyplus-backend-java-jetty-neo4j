package database.services;

import database.domains.DistributorProductOrder;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class DistributorProductOrderService extends BaseService<DistributorProductOrder> {
    public DistributorProductOrderService() {
        super(DistributorProductOrder.class);
    }

    public DistributorProductOrderService(String sessionId) {
        this();
        this.setSessionId(sessionId);
    }

    public Collection<DistributorProductOrder> getAll() {
        return super.getAll(new SortOrder().add("code"), -1, 2);
    }

    public DistributorProductOrder getById(long id) {
        return super.getById(id, 2);
    }

    public boolean changeStatus(DistributorProductOrder distributorProductOrder, String status) {
        if (!distributorProductOrder.availableAccept(this.getSessionId()) ||
                (!status.equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                        !status.equals(Constants.STATUSES.DECLINED.getValue())))
            return false;

        distributorProductOrder.setStatus(status);
        this.getSession().save(distributorProductOrder);
        return true;
    }

    @Override
    public boolean save(DistributorProductOrder object) {
        if (object.getNumber() <= 0)
            object.setNumber(Increment.getInstance().getDistributorProductOrders().getAndIncrement());

        return super.save(object);
    }
}
