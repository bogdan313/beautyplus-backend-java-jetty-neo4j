package database.services;

import database.domains.Product;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.cypher.query.SortOrder;
import org.neo4j.ogm.session.Session;

import java.util.Collection;
import java.util.Map;

public class ProductService extends BaseService<Product> {
    public ProductService() {
        super(Product.class);
    }

    @Override
    public Collection<Product> getAll() {
        return super.getAll(new SortOrder().add("title"));
    }

    public Product getByCode(String code) {
        Filter filter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.getOneByFilter(filter);
    }

    public Product getByTitle(String title) {
        Filter filter = new Filter("title", ComparisonOperator.LIKE, title);
        return this.getOneByFilter(filter);
    }

    public Product getByTitleOrCode(String title, String code) {
        Filter titleFilter = new Filter("title", ComparisonOperator.LIKE, title);
        Filter codeFilter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.getOneByFilter(titleFilter.or(codeFilter));
    }

    public boolean existsByCode(String code) {
        Filter filter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.isExists(filter);
    }

    public boolean existsByTitle(String title) {
        Filter filter = new Filter("title", ComparisonOperator.LIKE, title);
        return this.isExists(filter);
    }

    public boolean existsByTitleOrCode(String title, String code) {
        Filter titleFilter = new Filter("title", ComparisonOperator.LIKE, title);
        Filter codeFilter = new Filter("code", ComparisonOperator.LIKE, code);
        return this.isExists(titleFilter.or(codeFilter));
    }
}
