package database.services;

import database.domains.IncomingToWarehouse;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class IncomingToWarehouseService extends BaseService<IncomingToWarehouse> {
    public IncomingToWarehouseService() {
        super(IncomingToWarehouse.class);
    }

    public Collection<IncomingToWarehouse> getAll() {
        return super.getAll(new SortOrder().add("code"), -1, 2);
    }

    public IncomingToWarehouse getById(long id) {
        return super.getById(id, 2);
    }
}
