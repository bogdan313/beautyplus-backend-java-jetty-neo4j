package database.services;

import database.domains.ProductDescription;

public class ProductDescriptionService extends BaseService<ProductDescription> {
    public ProductDescriptionService() {
        super(ProductDescription.class);
    }
}
