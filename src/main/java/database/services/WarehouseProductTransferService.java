package database.services;

import database.domains.WarehouseProductTransfer;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.cypher.query.SortOrder;

import java.util.Collection;

public class WarehouseProductTransferService extends BaseService<WarehouseProductTransfer> {
    public WarehouseProductTransferService() {
        super(WarehouseProductTransfer.class);
    }

    @Override
    public Collection<WarehouseProductTransfer> getAll() {
        return super.getAll(new SortOrder().add("code"), -1, 2);
    }

    @Override
    public WarehouseProductTransfer getById(long id) {
        return super.getById(id, 2);
    }

    public boolean changeStatus(WarehouseProductTransfer warehouseProductTransfer, String status) {
        if (!warehouseProductTransfer.availableAccept(this.getSessionId()) ||
                (!status.equals(Constants.STATUSES.ACCEPTED.getValue()) &&
                        !status.equals(Constants.STATUSES.DECLINED.getValue())))
            return false;

        warehouseProductTransfer.setStatus(status);
        this.getSession().save(warehouseProductTransfer);
        return true;
    }

    @Override
    public boolean save(WarehouseProductTransfer object) {
        if (object.getNumber() <= 0)
            object.setNumber(Increment.getInstance().getWarehouseProductTransfers().getAndIncrement());

        return super.save(object);
    }
}
