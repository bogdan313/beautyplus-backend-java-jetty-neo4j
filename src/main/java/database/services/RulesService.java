package database.services;

import database.domains.Rules;
import org.neo4j.ogm.cypher.query.SortOrder;
import org.neo4j.ogm.session.Session;

import java.util.Collection;

public class RulesService extends BaseService<Rules> {
    public RulesService() {
        super(Rules.class);
    }

    public Collection<Rules> getAllRules() {
        return this.getAll();
    }

    @Override
    public Collection<Rules> getAll() {
        return super.getAll(new SortOrder().add("title"));
    }
}
