package database.domains;

import database.DBServiceSingleton;
import helpers.Constants;
import org.neo4j.ogm.annotation.PostLoad;
import services.AuthenticationServiceSingleton;

public class Administrator extends Person {
    private Rules rules;

    @Override
    public boolean availableCreate(String sessionId) {
        return false;
    }

    @Override
    public boolean availableRead(String sessionId) {
        return true;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        return currentPerson.getId().equals(this.getId());
    }

    @Override
    public boolean availableDelete(String sessionId) {
        return false;
    }

    public Administrator() {
        this.setRole(ROLE.ADMINISTRATOR.getValue());
    }

    @PostLoad
    public void appendInformation() {
        this.setStatus(Constants.USER_STATUS.ADMINISTRATOR.getValue());
        this.setRules(Rules.getAdminRules());
    }
}
