package database.domains;

import com.google.gson.annotations.Expose;
import database.relationships.AuthorRelationship;
import database.relationships.RuleRelationship;
import database.relationships.UpdatedByRelationship;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class Rules extends DomainWithAuthor {

    public static final long RULE_DENY = 0;
    public static final long RULE_R_OWN = 1;
    public static final long RULE_RW_OWN = 2;
    public static final long RULE_RWD_OWN = 3;
    public static final long RULE_RWD_OWN_R_ALL = 4;
    public static final long RULE_RWD_OWN_RW_ALL = 5;
    public static final long RULE_RWD_OWN_RWD_ALL = 6;
    public static final long RULE_RWD_OWN_RWD_ALL_CANCEL = 7;

    public static final long[] RULES = new long[] {
            Rules.RULE_DENY, Rules.RULE_R_OWN, Rules.RULE_RW_OWN,
            Rules.RULE_RWD_OWN, Rules.RULE_RWD_OWN_R_ALL,
            Rules.RULE_RWD_OWN_RW_ALL, Rules.RULE_RWD_OWN_RWD_ALL,
            Rules.RULE_RWD_OWN_RWD_ALL_CANCEL,
    };

    public static final String[] DOMAINS = new String[] {
            Dealer.class.getSimpleName(), DealerProductOrder.class.getSimpleName(),
            File.class.getSimpleName(), Person.class.getSimpleName(),
            Product.class.getSimpleName(), Rules.class.getSimpleName(),
            Distributor.class.getSimpleName(), DistributorProductTransfer.class.getSimpleName(),
            Moderator.class.getSimpleName(), Warehouse.class.getSimpleName(),
            ProductPrice.class.getSimpleName(), IncomingToWarehouse.class.getSimpleName(),
            DistributorProductOrder.class.getSimpleName(), WarehouseProductTransfer.class.getSimpleName(),
            DealerProductOrder.class.getSimpleName(), DistributorProductTransfer.class.getSimpleName(),
            DistributorMoney.class.getSimpleName(), Gift.class.getSimpleName(),
    };

    @Expose private String title;
    @Relationship(type = RuleRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<Person> persons = new HashSet<>();
    @Properties
    @Expose private Map<String, Long> rules = new HashMap<>();
    @Relationship(type = AuthorRelationship.TYPE, direction = Relationship.OUTGOING)
    private Person author;
    @Relationship(type = UpdatedByRelationship.TYPE, direction = Relationship.OUTGOING)
    private Person updatedBy;

    public Set<Person> getPersons() { return this.persons; }
    public void addPerson(Person person) { this.persons.add(person); }
    public void removePerson(Person person) { this.persons.remove(person); }

    public void setRule(String rule, long value) {
        this.rules.put(rule, value);
    }
    public long getRule(String rule) {
        return this.rules.getOrDefault(rule, Rules.RULE_DENY);
    }
    public Map<String, Long> getRules() { return this.rules; }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        if (!parameters.containsKey("title") || parameters.get("title").length == 0) return false;
        String title = parameters.get("title")[0].trim();
        if (title.trim().isEmpty()) return false;
        else this.setTitle(title);
        try {
            for (String domain : Rules.DOMAINS) {
                if (!parameters.containsKey(domain) || parameters.get(domain).length == 0)
                    return false;
                int currentRule = Integer.parseInt(parameters.get(domain)[0]);
                if (currentRule < Rules.RULE_DENY || currentRule > Rules.RULE_RWD_OWN_RWD_ALL_CANCEL)
                    return false;
                this.setRule(domain, Long.parseLong(parameters.get(domain)[0]));
            }
            return true;
        }
        catch (NumberFormatException exception) {
            return false;
        }
    }

    public void adminRules() {
        this.setTitle("Admin");
        for (String domain : Rules.DOMAINS) {
            this.setRule(domain, Rules.RULE_RWD_OWN_RWD_ALL_CANCEL);
        }
    }

    public static Rules getAdminRules() {
        Rules rules = new Rules();
        rules.adminRules();

        return rules;
    }
}
