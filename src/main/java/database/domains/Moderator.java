package database.domains;

import database.DBServiceSingleton;
import helpers.Constants;
import services.AuthenticationServiceSingleton;

public class Moderator extends Person {
    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        if (!currentPerson.getRole().equals(ROLE.ADMINISTRATOR.getValue())) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    @Override
    public boolean availableRead(String sessionId) {
        return true;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return currentPerson.getId().equals(this.getId()) && rule >= Rules.RULE_RW_OWN ||
                rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        if (!currentPerson.getRole().equals(ROLE.ADMINISTRATOR.getValue())) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RWD_ALL;
    }

    public Moderator() {
        this.setRole(ROLE.MODERATOR.getValue());
        this.setStatus(Constants.USER_STATUS.MODERATOR.getValue());
    }
}
