package database.domains;

import com.google.gson.annotations.Expose;
import database.relationships.ProductRelationship;
import helpers.DateHelper;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.Relationship;

import java.text.ParseException;
import java.util.Map;

public class ProductPrice extends DomainWithAuthor {
    @Relationship(type = ProductRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Product product;
    @Expose private double price;
    @Expose private double dealerPrice;
    @Expose private double bonuses;
    @Expose private long datetime;
    @Expose private String productCode;

    public Product getProduct() {
        return this.product;
    }
    public void setProduct(Product product) {
        if (product != null) {
            this.product = product;
            product.addProductPrice(this);
            setProductCode(product.getCode());
        }
    }

    public double getPrice() {
        return this.price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public double getDealerPrice() {
        return this.dealerPrice;
    }
    public void setDealerPrice(double dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    public long getDatetime() {
        return this.datetime;
    }
    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
        try {
            setPrice(Double.parseDouble(parsedParameters.getOrDefault("price", "").toString().trim()));
            setDealerPrice(Double.parseDouble(parsedParameters.getOrDefault("dealerPrice", "").toString().trim()));
            setBonuses(Double.parseDouble(parsedParameters.getOrDefault("bonuses", "").toString().trim()));
            setDatetime(DateHelper.parse(parsedParameters.getOrDefault("datetime", "").toString().trim()));

            return this.price >= 0 && this.dealerPrice >= 0 && this.bonuses >= 0;
        }
        catch (NumberFormatException exception) {
            return false;
        }
        catch (ParseException exception) {
            this.setDateTime();
            return true;
        }
    }
}
