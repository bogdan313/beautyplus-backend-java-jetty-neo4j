package database.domains;

import database.relationships.WarehouseRelationship;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Warehouse extends Person {
    @Relationship(type = WarehouseRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<WarehouseProductTransfer> transfers = new HashSet<>();

    public Set<WarehouseProductTransfer> getTransfers() {
        return this.transfers;
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        if (getCode() == null || getCode().trim().isEmpty())
            setCode(String.format("%s", Increment.getInstance().getWarehouseCode().getAndIncrement()));

        return super.setParameters(parameters);
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    public Warehouse() {
        this.setRole(ROLE.WAREHOUSE.getValue());
        this.setStatus(Constants.USER_STATUS.WAREHOUSE.getValue());
    }
}
