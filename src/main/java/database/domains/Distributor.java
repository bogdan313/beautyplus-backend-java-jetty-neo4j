package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.DistributorRelationship;
import helpers.Constants;
import helpers.Increment;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class Distributor extends Person {
    @Expose private double bonuses;
    @Expose private double money;
    @Expose private String warehouseName;
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<DistributorProductOrder> orders = new HashSet<>();
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<DistributorProductTransfer> transfers = new HashSet<>();
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<WarehouseProductTransfer> warehouseProductTransfers = new HashSet<>();
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<DistributorMoney> distributorMoney = new HashSet<>();

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }
    public void addBonuses(double bonuses) {
        this.bonuses += bonuses;
    }
    public void subtractBonuses(double bonuses) {
        this.bonuses -= bonuses;
    }

    public double getMoney() {
        return this.money;
    }
    public void setMoney(double money) {
        this.money = money;
    }
    public void addMoney(double money) {
        this.money += money;
    }
    public void subtractMoney(double money) {
        this.money -= money;
    }

    public Set<DistributorProductOrder> getOrders() {
        return this.orders;
    }
    public Set<DistributorProductTransfer> getTransfers() {
        return this.transfers;
    }
    public Set<WarehouseProductTransfer> getWarehouseProductTransfers() {
        return this.warehouseProductTransfers;
    }
    public Set<DistributorMoney> getDistributorMoney() {
        return this.distributorMoney;
    }

    public String getWarehouseName() {
        return this.warehouseName;
    }
    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Override
    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);

        if (!parsedParameters.containsKey("warehouseName")) return false;

        this.setWarehouseName(parsedParameters.getOrDefault("warehouseName", "").toString());

        if (getCode() == null || getCode().trim().isEmpty())
            setCode(String.format("%s", Increment.getInstance().getDistributorCode().getAndIncrement()));

        return super.setParameters(parameters);
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    public Distributor() {
        this.setRole(ROLE.DISTRIBUTOR.getValue());
        this.setStatus(Constants.USER_STATUS.DISTRIBUTOR.getValue());
    }
}
