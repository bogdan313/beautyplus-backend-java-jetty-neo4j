package database.domains;

import com.google.gson.annotations.Expose;
import database.relationships.ProductRelationship;
import database.services.ProductService;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Map;

@NodeEntity
public class ProductDescription extends Domain {
    @Relationship(type = ProductRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Product product;
    @Expose private double quantity;
    @Expose private double price;
    @Expose private double dealerPrice;
    @Expose private double bonuses;

    public Product getProduct() {
        return this.product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return this.quantity;
    }
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return this.price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public double getDealerPrice() {
        return this.dealerPrice;
    }
    public void setDealerPrice(double dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    @Override
    public boolean availableRead(String sessionId) {
        return true;
    }

    @Override
    public boolean availableCreate(String sessionId) {
        return true;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        return true;
    }

    @Override
    public boolean availableDelete(String sessionId) {
        return true;
    }

    @SuppressWarnings("unchecked")
    public static ProductDescription fromMap(Map<String, Object> parameters) {
        try {
            ProductDescription result = new ProductDescription();
            if (parameters.get("product") instanceof Map) {
                Product product = new ProductService().getById(Long.parseLong(((Map) parameters.get("product")).getOrDefault("id", 0).toString().trim()));
                if (product != null) {
                    if (!parameters.getOrDefault("id", "").toString().trim().isEmpty())
                        result.setId(Long.parseLong(parameters.getOrDefault("id", "").toString().trim()));
                    result.setProduct(product);
                    result.setBonuses(product.getBonuses());
                    result.setPrice(product.getPrice());
                    result.setDealerPrice(product.getDealerPrice());
                }
                else return null;
            }
            else return null;

            result.setQuantity(Double.parseDouble(parameters.getOrDefault("quantity", 0).toString().trim()));
            return result;
        }
        catch (NumberFormatException exception) {
            return null;
        }
    }
}
