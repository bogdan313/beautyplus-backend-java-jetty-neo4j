package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.Properties;
import services.AuthenticationServiceSingleton;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

@NodeEntity
public class Domain {
    @Id @GeneratedValue
    @Expose private Long id;

    private boolean isDeleted;
    private boolean isCancelled;
    @Expose private boolean updatable;
    @Expose private boolean deletable;
    @Expose private boolean readable;
    @Properties
    @Expose private Map<String, Object> properties = new HashMap<>();

    public Long getId() { return this.id; }
    public void setId(long id) {
        this.id = id;
    }

    public boolean getIsDeleted() { return this.isDeleted; }
    public void setIsDeleted(boolean isDeleted) { this.isDeleted = isDeleted; }

    public boolean getIsCancelled() { return this.isCancelled; }
    public void setIsCancelled(boolean isCancelled) { this.isCancelled = isCancelled; }

    public Map<String, Object> getProperties() { return this.properties; }
    public void setProperty(String key, Object value) { this.properties.put(key, value); }
    public Object getProperty(String key) { return this.properties.get(key); }
    public void deleteProperty(String key) { this.properties.remove(key); }
    public boolean hasProperty(String key) { return this.properties.containsKey(key); }
    public boolean hasPropertyObject(Object object) { return this.properties.containsValue(object); }

    public Domain() {}

    public boolean hasAttribute(String attribute) {
        List<Field> fields = new ArrayList<>();
        Domain.getAllFields(fields, this.getClass());
        return fields.stream().anyMatch(item -> item.getName().equals(attribute));
    }

    public Class<?> getAttributeType(String attribute) {
        List<Field> fields = new ArrayList<>();
        Domain.getAllFields(fields, this.getClass());
        Field field = fields.stream().filter(item -> item.getName().equals(attribute)).findFirst().orElse(null);
        return field != null ? field.getType() : null;
    }

    public void setAttribute(String attribute, Object value) {
        try {
            Field field = this.getClass().getField(attribute);
            field.setAccessible(true);
            field.set(this, value);
        }
        catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void setAttributes(Map<String, Object> attributes) {
        attributes.forEach((key, value) -> {
            if (this.hasAttribute(key))
                this.setAttribute(key, value);
        });
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        return false;
    }

    public void setDateTime() {
        if (this.hasAttribute("datetime")) {
            this.setAttribute("datetime", System.currentTimeMillis());
        }
    }

    public boolean availableCreate(String sessionId) {
        return false;
    }

    public boolean availableRead(String sessionId) {
        return false;
    }

    public boolean availableUpdate(String sessionId) {
        return false;
    }

    public boolean availableDelete(String sessionId) {
        return false;
    }

    public boolean availableCancel(String sessionId) {
        return false;
    }

    protected Rules getRules(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        return currentPerson != null
                ? currentPerson.getRules()
                : null;
    }

    protected long getRule(String sessionId) {
        Rules rules = this.getRules(sessionId);
        return rules != null
                ? rules.getRule(this.getClass().getSimpleName())
                : Rules.RULE_DENY;
    }

    public void setAvailability(String sessionId) {
        this.readable = this.availableRead(sessionId);
        this.updatable = this.availableUpdate(sessionId);
        this.deletable = this.availableDelete(sessionId);
    }

    public boolean equals(Domain domain) {
        return this.getId() != null && domain.getId() != null && this.getId().equals(domain.getId());
    }

    private static void getAllFields(List<Field> oldFields, Class<?> type) {
        if (!Domain.class.isAssignableFrom(type)) return;
        oldFields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null && Domain.class.isAssignableFrom(type.getSuperclass()))
            Domain.getAllFields(oldFields, type.getSuperclass());
    }
}
