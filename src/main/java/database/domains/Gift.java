package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import helpers.Constants;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import services.AuthenticationServiceSingleton;

import java.util.Map;

@NodeEntity
public class Gift extends DomainWithAuthor {
    @Expose private String title;
    @Expose private double bonuses;
    @Expose private String month;
    @Expose private String status;

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    public String getMonth() {
        return this.month;
    }
    public void setMonth(String month) {
        this.month = month;
    }

    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
        String title = parsedParameters.getOrDefault("title", "").toString(),
                month = parsedParameters.getOrDefault("month", "").toString(),
                status = parsedParameters.getOrDefault("status", Constants.STATUSES.ACTIVE.getValue()).toString();
        double bonuses = 0;
        try {
            bonuses = Double.parseDouble(parsedParameters.getOrDefault("bonuses", "0").toString());
        }
        catch (NumberFormatException ignore) {}

        if (title.trim().isEmpty() || month.trim().isEmpty() || status.trim().isEmpty() || bonuses < 0)
            return false;

        if (!status.equals(Constants.STATUSES.ACTIVE.getValue()) && !status.equals(Constants.STATUSES.DISABLED.getValue()))
            return false;

        if (!Constants.MONTHS.contains(month)) return false;

        this.title = title;
        this.bonuses = bonuses;
        this.month = month;
        this.status = status;
        return true;
    }

    @Override
    public boolean availableRead(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        return currentPerson != null && currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) ||
                this.getStatus().equals(Constants.STATUSES.ACTIVE.getValue());
    }
}
