package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.RuleRelationship;
import helpers.Increment;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.util.Map;

@NodeEntity
public class Person extends Domain {
    public enum ROLE {
        ADMINISTRATOR("administrator"),
        MODERATOR("moderator"),
        DEALER("dealer"),
        DISTRIBUTOR("distributor"),
        WAREHOUSE("warehouse");

        private final String value;

        ROLE(String role) {
            this.value = role;
        }

        public String getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }
    }

    @Expose private String fullName;
    @Index(unique = true)
    @Expose private String login;
    private String password;
    @Index(unique = true)
    @Expose private String code;
    @Expose private String status;
    @Expose private String role;
    @Relationship(type = RuleRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Rules rules;
    @Expose private String photo;
    @Expose private String phoneNumber;

    public String getFullName() {
        return this.fullName;
    }
    public String getLogin() {
        return this.login;
    }
    public String getPassword() {
        return this.password;
    }
    public String getCode() {
        return this.code;
    }
    public String getStatus() {
        return this.status;
    }
    public String getRole() {
        return  this.role;
    }
    public Rules getRules() {
        return this.rules;
    }
    public String getPhoto() { return this.photo; }
    public String getFormatted() {
        return String.format("%s %s", this.getCode(), this.getFullName());
    }

    public void setFullName(String fullName) { this.fullName = fullName; }
    public void setLogin(String login) { this.login = login; }
    public void setPassword(String password) { this.password = password; }
    public void setCode(String code) { this.code = code; }
    public void setStatus(String status) { this.status = status; }
    public void setRole(String role) { this.role = role; }
    public void setRules(Rules rules) { this.rules = rules; }
    public void setPhoto(String photo) { this.photo = photo; }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
        String fullName = parsedParameters.getOrDefault("fullName", "").toString().trim();
        String login = parsedParameters.getOrDefault("login", "").toString().trim();
        String password = parsedParameters.getOrDefault("password", "").toString().trim();
        String phoneNumber = parsedParameters.getOrDefault("phoneNumber", "").toString().trim();

        if (fullName.isEmpty() || login.isEmpty())
            return false;

        this.setFullName(fullName);
        this.setLogin(login);
        if (!password.isEmpty()) this.setPassword(password);
        this.setPhoneNumber(phoneNumber);

        return true;
    }

    @Override
    public boolean availableCreate(String sessionId) {
        return true;
    }

    @Override
    public boolean availableRead(String sessionId) {
        return true;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        if (currentPerson.equals(this)) return true;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        if (currentPerson.getId().equals(this.getId())) return true;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return rule >= Rules.RULE_RWD_OWN_RWD_ALL;
    }
}
