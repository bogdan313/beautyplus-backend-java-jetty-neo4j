package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.ProductPriceRelationship;
import helpers.Constants;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.PostLoad;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class Product extends DomainWithAuthor {
    @Expose private String picture;
    @Expose private String title;
    @Index(unique = true)
    @Expose private String code;
    @Relationship(type = ProductPriceRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<ProductPrice> productPrices = new HashSet<>();
    @Expose private double price = 0;
    @Expose private double dealerPrice = 0;
    @Expose private double bonuses = 0;
    @Expose private String status;
    @Expose private boolean isSale;

    public static final Set<String> STATUSES = new HashSet<String>() {{
        add(Constants.STATUSES.ACTIVE.getValue());
        add(Constants.STATUSES.DISABLED.getValue());
    }};

    public String getFormatted() {
        return String.format("%s %s", this.getCode(), this.getTitle());
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public Set<ProductPrice> getProductPrices() {
        return this.productPrices;
    }
    public void setProductPrices(Set<ProductPrice> productPrices) {
        this.productPrices = productPrices;
    }
    public boolean addProductPrice(ProductPrice productPrice) {
        if (this.productPrices.contains(productPrice)) return false;
        this.productPrices.add(productPrice);
        return true;
    }
    public boolean removeProductPrice(ProductPrice productPrice) {
        if (!this.productPrices.contains(productPrice)) return false;
        this.productPrices.remove(productPrice);
        return true;
    }

    public String getPicture() {
        return this.picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }

    public double getPrice() {
        return this.price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public double getDealerPrice() {
        return this.dealerPrice;
    }
    public void setDealerPrice(double dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public void setCurrentPrice() {
        ProductPrice newPrice = this.productPrices.stream()
                .max(Comparator.comparingLong(ProductPrice::getDatetime))
                .orElse(null);
        if (newPrice != null) {
            this.bonuses = newPrice.getBonuses();
            this.dealerPrice = newPrice.getDealerPrice();
            this.price = newPrice.getPrice();
        }
        else {
            this.bonuses = 0;
            this.dealerPrice = 0;
            this.price = 0;
        }
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
        String title = parsedParameters.getOrDefault("title", "").toString().trim(),
                code = parsedParameters.getOrDefault("code", "").toString().trim(),
                status = parsedParameters.getOrDefault("status", Constants.STATUSES.ACTIVE).toString().trim();
        boolean isSale = Boolean.parseBoolean(parsedParameters.getOrDefault("isSale", "false").toString());

        if (title.isEmpty() || code.isEmpty() || status.isEmpty())
            return false;

        this.title = title;
        this.code = code;
        this.isSale = isSale;
        if (Product.STATUSES.contains(status))
            this.setStatus(status);
        else this.setStatus(Constants.STATUSES.ACTIVE.getValue());

        return true;
    }

    @PostLoad
    public void postLoad() {
        this.setCurrentPrice();
    }

    @Override
    public boolean availableRead(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);

        return currentPerson != null &&
                (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue()) ||
                        currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue())) ||
                this.getStatus().equals(Constants.STATUSES.ACTIVE.getValue());
    }
}
