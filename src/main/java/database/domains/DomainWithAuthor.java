package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.AuthorRelationship;
import database.relationships.UpdatedByRelationship;
import helpers.Constants;
import helpers.DateHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.text.ParseException;

@NodeEntity
public class DomainWithAuthor extends Domain {
    @Relationship(type = AuthorRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Person author;
    @Relationship(type = UpdatedByRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Person updatedBy;
    @Expose private long createdDate;
    @Expose private long updatedDate;

    public Person getAuthor() {
        return this.author;
    }
    public void setAuthor(Person author) { this.author = author; }

    public Person getUpdatedBy() {
        return this.updatedBy;
    }
    public void setUpdatedBy(Person updatedBy) {
        this.updatedBy = updatedBy;
    }

    public long getCreatedDate() {
        return this.createdDate;
    }
    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }
    public void setCreatedDate(String createdDate) {
        try {
            this.createdDate = DateHelper.parse(createdDate);
        }
        catch (ParseException exception) {
            this.createdDate = System.currentTimeMillis();
        }
    }
    public void setCreatedDate() {
        this.createdDate = System.currentTimeMillis();
    }

    public long getUpdatedDate() {
        return this.updatedDate;
    }
    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }
    public void setUpdatedDate(String updatedDate) {
        try {
            this.updatedDate = DateHelper.parse(updatedDate);
        }
        catch (ParseException exception) {
            this.updatedDate = System.currentTimeMillis();
        }
    }
    public void setUpdatedDate() {
        this.updatedDate = System.currentTimeMillis();
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Rules rules = this.getRules(sessionId);
        long rule = rules.getRule(this.getClass().getSimpleName());
        return  rule >= Rules.RULE_RW_OWN;
    }

    @Override
    public boolean availableRead(String sessionId) {
        if (sessionId.equals(Constants.ALLOW_READ)) return true;
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return this.getAuthor() != null && this.getAuthor().getId().equals(currentPerson.getId()) &&
                rule >= Rules.RULE_R_OWN || rule >= Rules.RULE_RWD_OWN_R_ALL;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return this.getAuthor() != null && this.getAuthor().getId().equals(currentPerson.getId()) &&
                rule >= Rules.RULE_RW_OWN || rule >= Rules.RULE_RWD_OWN_RW_ALL;
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;
        Rules rules = currentPerson.getRules();
        if (rules == null) return false;
        long rule = rules.getRule(this.getClass().getSimpleName());
        return this.getAuthor() != null && this.getAuthor().getId().equals(currentPerson.getId()) &&
                rule >= Rules.RULE_RWD_OWN || rule >= Rules.RULE_RWD_OWN_RWD_ALL;
    }

    @Override
    public boolean availableCancel(String sessionId) {
        long rule = this.getRule(sessionId);
        return rule == Rules.RULE_RWD_OWN_RWD_ALL_CANCEL;
    }
}
