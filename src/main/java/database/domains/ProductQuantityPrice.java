package database.domains;

import com.google.gson.annotations.Expose;
import database.relationships.ProductRelationship;
import database.services.ProductService;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Map;

@NodeEntity
public class ProductQuantityPrice extends Domain {
    @Relationship(type = ProductRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Product product;
    @Expose private double quantity;
    @Expose private double price;

    public Product getProduct() {
        return this.product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return this.quantity;
    }
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return this.price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @SuppressWarnings("unchecked")
    public static ProductQuantityPrice fromMap(Map<String, Object> parameters) {
        try {
            ProductQuantityPrice result = new ProductQuantityPrice();
            result.setPrice(Double.parseDouble(parameters.getOrDefault("price", "").toString().trim()));
            result.setQuantity(Double.parseDouble(parameters.getOrDefault("quantity", "").toString().trim()));
            if (!parameters.getOrDefault("id", "").toString().trim().isEmpty())
                result.setId(Long.parseLong(parameters.getOrDefault("id", "").toString().trim()));
            if (parameters.get("product") instanceof Map) {
                Map<String, String> productMap = (Map<String, String>) parameters.get("product");
                long id = Long.parseLong(productMap.get("id"));
                Product product = new ProductService().getById(id);
                if (product != null)
                    result.setProduct(product);
                else return null;
            }
            else return null;
            return result;
        }
        catch (NumberFormatException exception) {
            return null;
        }
    }

    @Override
    public boolean availableCreate(String sessionId) {
        return true;
    }

    @Override
    public boolean availableRead(String sessionId) {
        return true;
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        return true;
    }

    @Override
    public boolean availableDelete(String sessionId) {
        return true;
    }
}
