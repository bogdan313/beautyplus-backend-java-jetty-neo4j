package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.DistributorRelationship;
import database.services.DistributorService;
import helpers.DateHelper;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.text.ParseException;
import java.util.Map;

@NodeEntity
public class DistributorMoney extends DomainWithAuthor {
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Distributor distributor;
    @Expose private double money;
    @Expose private String type;
    @Expose private long datetime;
    @Expose private String distributorCode;

    public enum TYPE {
        CASH("Cash"),
        TRANSFER("Transfer"),
        TERMINAL("Terminal"),
        CLICKUZ("ClickUz");

        private final String type;

        TYPE(String type) {
            this.type = type;
        }

        public String getType() {
            return this.type;
        }

        @Override
        public String toString() {
            return this.getType();
        }

        public static boolean availableType(String type) {
            return type.equals(DistributorMoney.TYPE.CASH.getType()) ||
                    type.equals(DistributorMoney.TYPE.TRANSFER.getType()) ||
                    type.equals(TYPE.TERMINAL.getType()) ||
                    type.equals(TYPE.CLICKUZ.getType());
        }
    }

    public Distributor getDistributor() {
        return this.distributor;
    }
    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
        if (distributor != null) {
            setDistributorCode(distributor.getCode());
        }
        else setDistributorCode("");
    }

    public double getMoney() {
        return this.money;
    }
    public void setMoney(double money) {
        this.money = money;
    }

    public long getDatetime() {
        return this.datetime;
    }
    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
    public void setDatetime(String datetime) {
        try {
            this.setDatetime(DateHelper.parse(datetime));
        }
        catch (ParseException e) {
            this.setDatetime(System.currentTimeMillis());
        }
    }
    public void setDatetime() {
        this.setDatetime(System.currentTimeMillis());
    }

    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getDistributorCode() {
        return distributorCode;
    }
    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean setParameters(Map<String, String[]> parameters) {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
        final DistributorService distributorService = new DistributorService();

        if (parsedParameters.containsKey("distributor") && parsedParameters.get("distributor") instanceof Map) {
            Map<String, Object> distributorMap = (Map<String, Object>) parsedParameters.get("distributor");

            if (!distributorMap.containsKey("id")) return false;

            try {
                Distributor distributor = distributorService.getById(Long.parseLong(distributorMap.get("id").toString()));

                if (distributor == null) return false;

                this.setDistributor(distributor);
            }
            catch (NumberFormatException e) {
                return false;
            }
        }
        else return false;

        try {
            this.money = Double.parseDouble(parsedParameters.getOrDefault("money", "0").toString());
        }
        catch (NumberFormatException e) {
            return false;
        }

        String type = parsedParameters.getOrDefault("type", TYPE.CASH.getType()).toString();
        if (DistributorMoney.TYPE.availableType(type)) {
            this.setType(type);
        }
        else return false;

        this.setDatetime(parsedParameters.getOrDefault("datetime", "").toString());

        return true;
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) || super.availableCreate(sessionId);
    }

    @Override
    public boolean availableRead(String sessionId) {
        if (super.availableRead(sessionId)) return true;

        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                (this.getAuthor() != null && this.getAuthor().equals(currentPerson) ||
                        this.getUpdatedBy() != null && this.getUpdatedBy().equals(currentPerson)) ||
                currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) &&
                        this.getDistributor() != null && this.getDistributor().equals(currentPerson);
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                (this.getAuthor() != null && this.getAuthor().equals(currentPerson) ||
                        this.getUpdatedBy() != null && this.getUpdatedBy().equals(currentPerson)) ||
                super.availableUpdate(sessionId);
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                (this.getAuthor() != null && this.getAuthor().equals(currentPerson) ||
                        this.getUpdatedBy() != null && this.getUpdatedBy().equals(currentPerson)) ||
                super.availableDelete(sessionId);
    }
}
