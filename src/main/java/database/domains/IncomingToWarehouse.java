package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.ProductQuantityPriceRelationship;
import database.relationships.WarehouseRelationship;
import database.services.WarehouseService;
import helpers.DateHelper;
import helpers.GenerateHelper;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class IncomingToWarehouse extends DomainWithAuthor {
    @Relationship(type = ProductQuantityPriceRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Set<ProductQuantityPrice> products = new HashSet<>();
    @Relationship(type = WarehouseRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Warehouse warehouse;
    @Expose private long datetime;
    @Expose private String code;

    public Set<ProductQuantityPrice> getProducts() {
        return this.products;
    }
    public void setProducts(Set<ProductQuantityPrice> products) {
        this.products = products;
    }
    public void addProduct(ProductQuantityPrice productQuantityPrice) {
        this.products.add(productQuantityPrice);
    }
    public void removeProduct(ProductQuantityPrice productQuantityPrice) {
        this.products.remove(productQuantityPrice);
    }

    public Warehouse getWarehouse() {
        return this.warehouse;
    }
    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public long getDatetime() {
        return this.datetime;
    }
    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
    public void setDatetime(String datetime) {
        try {
            this.datetime = DateHelper.parse(datetime);
        }
        catch (ParseException exception) {
            this.datetime = 0;
        }
    }

    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @SuppressWarnings("unchecked")
    public boolean setParameters(Map<String, String[]> parameters) {
        try {
            Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
            String code = parsedParameters.getOrDefault("code", GenerateHelper.generateString()).toString().trim();
            this.setDatetime(parsedParameters.getOrDefault("datetime", "").toString().trim());
            this.setCode(code);

            if (parsedParameters.get("products") instanceof Map) {
                ((Map<String, Map<String, Object>>) parsedParameters.get("products"))
                        .forEach((String key, Map<String, Object> item) -> {
                    ProductQuantityPrice tmpItem = ProductQuantityPrice.fromMap(item);
                    if (tmpItem != null) {
                        if (tmpItem.getId() != null) {
                            ProductQuantityPrice currentProductQuantityPrice =
                                    this.products.stream().filter(tmpItem::equals).findFirst().orElse(null);
                            if (currentProductQuantityPrice != null) {
                                currentProductQuantityPrice.setQuantity(tmpItem.getQuantity());
                                currentProductQuantityPrice.setPrice(tmpItem.getPrice());
                                currentProductQuantityPrice.setProduct(tmpItem.getProduct());
                            }
                        }
                        else this.addProduct(tmpItem);
                    }
                });
            }
            else return false;

            if (parsedParameters.get("warehouse") instanceof Map) {
                Map<String, String> warehouseMap = (Map<String, String>) parsedParameters.get("warehouse");
                WarehouseService warehouseService = new WarehouseService();
                Warehouse warehouse =
                        warehouseService.getById(Long.parseLong(warehouseMap.getOrDefault("id", "").trim()));
                if (warehouse != null)
                    this.setWarehouse(warehouse);
                else return false;
            }
            else return false;

            return true;
        }
        catch (NumberFormatException exception) {
            return false;
        }
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue())) {
            this.setAuthor(currentPerson);
            this.setWarehouse((Warehouse) currentPerson);

            return true;
        }

        return super.availableCreate(sessionId);
    }

    @Override
    public boolean availableRead(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return super.availableRead(sessionId) ||
                this.getWarehouse() != null && this.getWarehouse().equals(currentPerson);
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.WAREHOUSE.getValue()) &&
                this.getWarehouse() != null && this.getWarehouse().equals(currentPerson)) {
            this.setUpdatedBy(currentPerson);
            this.setWarehouse((Warehouse) currentPerson);

            return true;
        }

        return super.availableUpdate(sessionId);
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return super.availableDelete(sessionId) ||
                this.getWarehouse() != null && this.getWarehouse().equals(currentPerson);
    }
}
