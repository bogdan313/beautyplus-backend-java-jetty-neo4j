package database.domains;

import com.google.gson.annotations.Expose;
import database.DBServiceSingleton;
import database.relationships.*;
import database.services.DealerProductOrderService;
import database.services.DealerService;
import database.services.DistributorService;
import helpers.Constants;
import helpers.DateHelper;
import helpers.GenerateHelper;
import helpers.ParseParametersHelper;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import services.AuthenticationServiceSingleton;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class DistributorProductTransfer extends DomainWithAuthor {
    @Expose private int number;
    @Relationship(type = ProductDescriptionRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Set<ProductDescription> products = new HashSet<>();
    @Relationship(type = DealerRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Dealer dealer;
    @Relationship(type = DistributorRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private Distributor distributor;
    @Expose private long datetime;
    @Expose private String status;
    @Expose private String code;
    @Relationship(type = DealerProductOrderRelationship.TYPE, direction = Relationship.OUTGOING)
    @Expose private DealerProductOrder dealerProductOrder;
    @Expose private boolean acceptable;
    @Expose private String distributorCode;
    @Expose private String dealerCode;

    public Dealer getDealer() {
        return this.dealer;
    }
    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
        if (dealer != null) {
            setDealerCode(dealer.getCode());
        }
        else setDealerCode("");
    }

    public Distributor getDistributor() {
        return this.distributor;
    }
    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
        if (distributor != null) {
            setDistributorCode(distributor.getCode());
        }
        else setDistributorCode("");
    }

    public long getDatetime() {
        return this.datetime;
    }
    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }
    public void setDatetime(String datetime) {
        try {
            this.datetime = DateHelper.parse(datetime);
        }
        catch (ParseException exception) {
            this.datetime = 0;
        }
    }

    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public Set<ProductDescription> getProducts() {
        return this.products;
    }
    public void setProducts(Set<ProductDescription> products) {
        this.products = products;
    }
    public void addProduct(ProductDescription productDescription) {
        this.products.add(productDescription);
    }
    public void removeProduct(ProductDescription productDescription) {
        this.products.remove(productDescription);
    }

    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public DealerProductOrder getDealerProductOrder() {
        return this.dealerProductOrder;
    }
    public void setDealerProductOrder(DealerProductOrder dealerProductOrder) {
        this.dealerProductOrder = dealerProductOrder;
    }

    public int getNumber() {
        return this.number;
    }
    public void setNumber(int number) {
        this.number = number;
    }

    public String getDistributorCode() {
        return distributorCode;
    }
    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }

    public String getDealerCode() {
        return dealerCode;
    }
    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    @SuppressWarnings("unchecked")
    public boolean setParameters(Map<String, String[]> parameters) {
        try {
            Map<String, Object> parsedParameters = ParseParametersHelper.parse(parameters);
            if (parsedParameters.get("distributor") instanceof Map) {
                Distributor distributor = new DistributorService()
                        .getById(Long.parseLong(((Map) parsedParameters.get("distributor")).get("id").toString().trim()));
                if (distributor != null)
                    this.setDistributor(distributor);
                else return false;
            }
            else return false;

            if (parsedParameters.get("dealer") instanceof Map) {
                Dealer dealer = new DealerService()
                        .getById(Long.parseLong(((Map) parsedParameters.get("dealer")).get("id").toString().trim()));
                if (dealer != null)
                    this.setDealer(dealer);
                else return false;
            }
            else return false;

            this.setCode(parsedParameters.getOrDefault("code", GenerateHelper.generateString()).toString().trim());
            this.setDatetime(parsedParameters.getOrDefault("datetime", "").toString().trim());

            String status = parsedParameters.getOrDefault("status", Constants.STATUSES.WAITING).toString().trim();
            if (Constants.STATUSES.isInShortList(status))
                this.setStatus(status);
            else this.setStatus(Constants.STATUSES.WAITING.getValue());

            if (parsedParameters.get("products") instanceof Map) {
                ((Map<String, Map<String, Object>>) parsedParameters.get("products"))
                        .forEach((String key, Map<String, Object> item) -> {
                            ProductDescription tmpItem = ProductDescription.fromMap(item);
                            if (tmpItem != null) {
                                if (tmpItem.getId() != null) {
                                    ProductDescription currentDescription =
                                            this.products.stream().filter(tmpItem::equals).findFirst().orElse(null);
                                    if (currentDescription != null) {
                                        currentDescription.setQuantity(tmpItem.getQuantity());
                                        currentDescription.setPrice(tmpItem.getPrice());
                                        currentDescription.setDealerPrice(tmpItem.getDealerPrice());
                                        currentDescription.setBonuses(tmpItem.getBonuses());
                                        currentDescription.setProduct(tmpItem.getProduct());
                                    }
                                }
                                else this.addProduct(tmpItem);
                            }
                        });
            }
            else return false;

            if (parsedParameters.get("dealerProductOrder") instanceof Map) {
                try {
                    DealerProductOrder dealerProductOrder =
                            new DealerProductOrderService(Constants.ALLOW_READ)
                                    .getById(Long.parseLong(((Map) parsedParameters.get("dealerProductOrder"))
                                            .getOrDefault("id", "").toString().trim()));
                    if (dealerProductOrder != null)
                        this.setDealerProductOrder(dealerProductOrder);
                }
                catch (NumberFormatException exception) {

                }
            }

            return true;
        }
        catch (NumberFormatException exception) {
            return false;
        }
    }

    @Override
    public boolean availableCreate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue())) {
            this.setDistributor((Distributor) currentPerson);
            this.setAuthor(currentPerson);
            this.setStatus(Constants.STATUSES.WAITING.getValue());

            return true;
        }

        return super.availableCreate(sessionId);
    }

    @Override
    public boolean availableRead(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        return super.availableRead(sessionId) ||
                this.getDistributor() != null && this.getDistributor().equals(currentPerson) ||
                this.getDealer() != null && this.getDealer().equals(currentPerson);
    }

    @Override
    public boolean availableUpdate(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) return true;
        if (!this.getStatus().equals(Constants.STATUSES.WAITING.getValue())) return false;

        if (currentPerson.getRole().equals(Person.ROLE.DISTRIBUTOR.getValue()) &&
                this.getDistributor() != null && this.getDistributor().equals(currentPerson)) {
            this.setUpdatedBy(currentPerson);
            this.setDistributor((Distributor) currentPerson);
            this.setStatus(Constants.STATUSES.WAITING.getValue());

            return true;
        }

        return super.availableUpdate(sessionId);
    }

    @Override
    public boolean availableDelete(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) return true;
        if (!this.getStatus().equals(Constants.STATUSES.WAITING.getValue())) return false;

        return super.availableDelete(sessionId) ||
                this.getDistributor() != null && this.getDistributor().equals(currentPerson);
    }

    public boolean availableAccept(String sessionId) {
        Person currentPerson = AuthenticationServiceSingleton.getInstance().getCurrentPerson(sessionId);
        if (currentPerson == null) return false;

        if (currentPerson.getRole().equals(Person.ROLE.ADMINISTRATOR.getValue())) return true;
        if (!this.getStatus().equals(Constants.STATUSES.WAITING.getValue())) return false;

        return super.availableUpdate(sessionId) ||
                this.getDealer() != null && this.getDealer().equals(currentPerson);
    }

    @Override
    public void setAvailability(String sessionId) {
        super.setAvailability(sessionId);
        this.acceptable = this.availableAccept(sessionId);
    }
}
