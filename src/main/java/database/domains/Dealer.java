package database.domains;

import com.google.gson.annotations.Expose;
import database.relationships.DealerRelationship;
import database.relationships.SubDealerRelationship;
import helpers.Constants;
import helpers.Increment;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@NodeEntity
public class Dealer extends Person {
    @Relationship(type = SubDealerRelationship.TYPE, direction = Relationship.INCOMING)
    @Expose private Set<Dealer> subDealers = new HashSet<>();
    @Relationship(type = SubDealerRelationship.TYPE, direction = Relationship.OUTGOING)
    private Dealer parentDealer;
    @Expose private double bonuses;
    private double groupBonuses;
    @Expose private double money;
    @Relationship(type = DealerRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<DistributorProductTransfer> transfers = new HashSet<>();
    @Relationship(type = DealerRelationship.TYPE, direction = Relationship.INCOMING)
    private Set<DealerProductOrder> orders = new HashSet<>();
    @Expose private long registrationDate;
    @Expose private String parentDealerCode;
    @Expose private String parentDealerFullName;

    public double getBonuses() {
        return this.bonuses;
    }
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }
    public void addBonuses(double bonuses) {
        this.bonuses += bonuses;
    }
    public void subtractBonuses(double bonuses) {
        this.bonuses -= bonuses;
    }

    public double getGroupBonuses() {
        return this.groupBonuses;
    }
    public void setGroupBonuses(double groupBonuses) {
        this.groupBonuses = groupBonuses;
    }
    public void addGroupBonuses(double groupBonuses) { this.groupBonuses += groupBonuses; }
    public void subtractGroupBonuses(double groupBonuses) { this.groupBonuses -= groupBonuses; }

    public double getMoney() {
        return this.money;
    }
    public void setMoney(double money) {
        this.money = money;
    }
    public void addMoney(double money) {
        this.money += money;
    }
    public void subtractMoney(double money) {
        this.money -= money;
    }

    public Set<Dealer> getSubDealers() {
        return this.subDealers;
    }
    public void setSubDealers(Set<Dealer> subDealers) {
        this.subDealers = subDealers;
    }
    public void addSubDealer(Dealer dealer) {
        this.subDealers.add(dealer);
    }
    public void removeSubDealer(Dealer dealer) {
        this.subDealers.remove(dealer);
    }

    public Dealer getParentDealer() {
        return this.parentDealer;
    }
    public void setParentDealer(Dealer dealer) {
        this.parentDealer = dealer;
        if (dealer != null) {
            setParentDealerCode(dealer.getCode());
            setParentDealerFullName(dealer.getFullName());
        }
        else {
            setParentDealerFullName("");
            setParentDealerCode("");
        }
    }
    public void removeParentDealer() {
        setParentDealer(null);
    }

    public long getRegistrationDate() {
        return this.registrationDate;
    }
    public void setRegistrationDate(long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getParentDealerCode() {
        return parentDealerCode;
    }
    public void setParentDealerCode(String parentDealerCode) {
        this.parentDealerCode = parentDealerCode;
    }

    public String getParentDealerFullName() {
        return parentDealerFullName;
    }
    public void setParentDealerFullName(String parentDealerFullName) {
        this.parentDealerFullName = parentDealerFullName;
    }

    public Set<DistributorProductTransfer> getTransfers() {
        return this.transfers;
    }

    public Set<DealerProductOrder> getOrders() {
        return this.orders;
    }

    public boolean setParameters(Map<String, String[]> parameters) {
        if (getCode() == null || getCode().trim().isEmpty())
            setCode(String.format("%s", Increment.getInstance().getDealerCode().getAndIncrement()));

        return super.setParameters(parameters);
    }

    public Dealer() {
        this.setRole(ROLE.DEALER.getValue());
        this.setStatus(Constants.USER_STATUS.DEALER_1.getValue());
    }
}
