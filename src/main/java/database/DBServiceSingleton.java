package database;

import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

import java.io.InputStream;
import java.util.Properties;

public class DBServiceSingleton {
    private static DBServiceSingleton dbServiceSingleton = new DBServiceSingleton();

    private SessionFactory sessionFactory;

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    private void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private DBServiceSingleton() {
        try (InputStream propertiesStream = getClass().getClassLoader().getResourceAsStream("database.properties")) {
            Properties properties = new Properties();
            Configuration configuration;

            properties.load(propertiesStream);
            configuration = new Configuration.Builder()
                    .uri(String.format("bolt://%s:%s",
                            properties.getProperty("host", "localhost"),
                            properties.getProperty("port", "7687")))
                    .credentials(properties.getProperty("user", "root"),
                            properties.getProperty("password", "12345"))
                    .build();

            setSessionFactory(new SessionFactory(configuration, "database.domains"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DBServiceSingleton getInstance() {
        return dbServiceSingleton;
    }

    public Session getSession() {
        return getSessionFactory().openSession();
    }
}
