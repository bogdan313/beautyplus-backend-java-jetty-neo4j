package main;

import database.DBServiceSingleton;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import servletContexts.*;
import servlets.hello.HelloServlet;

public class Main {
    public static void main(String[] args) {
        Server server = new Server(8080);
        ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();
        contextHandlerCollection.setHandlers(new Handler[] {
                new HelloServletContext("/hello").getHandler(),
                new AuthServletContext("/auth").getHandler(),
                new RulesServletContext("/rules").getHandler(),
                new UserServletContext("/user").getHandler(),
                new DealerServletContext("/dealer").getHandler(),
                new ProductServletContext("/product").getHandler(),
                new AdministratorServletContext("/administrator").getHandler(),
                new ModeratorServletContext("/moderator").getHandler(),
                new DistributorServletContext("/distributor").getHandler(),
                new WarehouseServletContext("/warehouse").getHandler(),
                new ProductPriceServletContext("/product-price").getHandler(),
                new IncomingToWarehouseServletContext("/incoming-to-warehouse").getHandler(),
                new DistributorProductOrderServletContext("/distributor-product-order").getHandler(),
                new WarehouseProductTransferServletContext("/warehouse-product-transfer").getHandler(),
                new DealerProductOrderServletContext("/dealer-product-order").getHandler(),
                new DistributorProductTransferServletContext("/distributor-product-transfer").getHandler(),
                new ReportServletContext("/report").getHandler(),
                new ExcelServletContext("/excel").getHandler(),
                new ExportServletContext("/export").getHandler(),
                new GiftServletContext("/gift").getHandler(),
        });
        server.setHandler(contextHandlerCollection);

        try {
            server.start();
            server.join();
        }
        catch (Exception e) {}
    }
}
