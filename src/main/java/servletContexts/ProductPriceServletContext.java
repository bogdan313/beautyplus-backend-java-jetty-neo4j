package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.productPrice.ProductPriceServlet;

public class ProductPriceServletContext extends BaseServletContext {
    public ProductPriceServletContext(String baseUrl) {
        super(baseUrl);
    }

    protected void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new ProductPriceServlet()), ProductPriceServlet.URL);
    }
}
