package servletContexts;

import org.eclipse.jetty.servlet.ServletHolder;
import servlets.hello.HelloServlet;

public class HelloServletContext extends BaseServletContext {
    public HelloServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        this.getHandler()
                .addServlet(new ServletHolder(new HelloServlet()), HelloServlet.URL);
    }
}
