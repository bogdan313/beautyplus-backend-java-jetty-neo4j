package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.gift.GiftServlet;

public class GiftServletContext extends BaseServletContext {
    public GiftServletContext(String baseUrl) {
        super(baseUrl);
    }

    @Override
    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new GiftServlet()), GiftServlet.URL);
    }
}
