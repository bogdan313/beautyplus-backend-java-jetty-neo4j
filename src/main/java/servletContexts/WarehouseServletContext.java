package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.warehouse.WarehouseServlet;

public class WarehouseServletContext extends BaseServletContext {
    public WarehouseServletContext(String baseUrl) {
        super(baseUrl);
    }

    protected void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new WarehouseServlet()), WarehouseServlet.URL);
    }
}
