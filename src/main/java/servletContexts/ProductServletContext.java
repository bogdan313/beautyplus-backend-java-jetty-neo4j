package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.product.CheckProductByCodeServlet;
import servlets.product.CheckProductByTitleServlet;
import servlets.product.ProductPictureServlet;
import servlets.product.ProductServlet;

public class ProductServletContext extends BaseServletContext {
    public ProductServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new ProductServlet()), ProductServlet.URL);
        handler.addServlet(new ServletHolder(new ProductPictureServlet()), ProductPictureServlet.URL);
        handler.addServlet(new ServletHolder(new CheckProductByTitleServlet()), CheckProductByTitleServlet.URL);
        handler.addServlet(new ServletHolder(new CheckProductByCodeServlet()), CheckProductByCodeServlet.URL);
    }
}
