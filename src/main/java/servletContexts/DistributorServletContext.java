package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.distributor.DistributorServlet;
import servlets.distributor.MoneyServlet;

public class DistributorServletContext extends BaseServletContext {
    public DistributorServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new DistributorServlet()), DistributorServlet.URL);
        handler.addServlet(new ServletHolder(new MoneyServlet()), MoneyServlet.URL);
    }
}
