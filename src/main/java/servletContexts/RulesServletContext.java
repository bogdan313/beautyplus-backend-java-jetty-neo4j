package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.rules.AvailableRulesServlet;
import servlets.rules.RulesServlet;
import servlets.rules.SetRulesServlet;

public class RulesServletContext extends BaseServletContext {
    public RulesServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new RulesServlet()), RulesServlet.URL);
        handler.addServlet(new ServletHolder(new AvailableRulesServlet()), AvailableRulesServlet.URL);
        handler.addServlet(new ServletHolder(new SetRulesServlet()), SetRulesServlet.URL);
    }
}
