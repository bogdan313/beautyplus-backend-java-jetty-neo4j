package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.excel.AddDealersServlet;
import servlets.excel.ImportDealersReportServlet;
import servlets.excel.ImportDealersServlet;
import servlets.excel.ImportDistributorsServlet;

import javax.servlet.MultipartConfigElement;

public class ExcelServletContext extends BaseServletContext {
    public ExcelServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();

        ServletHolder importDealersServletHolder = new ServletHolder(new ImportDealersServlet());
        importDealersServletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp/upload"));
        handler.addServlet(importDealersServletHolder, ImportDealersServlet.URL);

        ServletHolder addDealersServletHolder = new ServletHolder(new AddDealersServlet());
        addDealersServletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp/upload"));
        handler.addServlet(addDealersServletHolder, AddDealersServlet.URL);

        ServletHolder importDistributorsServletHolder = new ServletHolder(new ImportDistributorsServlet());
        importDistributorsServletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp/upload"));
        handler.addServlet(importDistributorsServletHolder, ImportDistributorsServlet.URL);

        ServletHolder importDealersReportServletHolder = new ServletHolder(new ImportDealersReportServlet());
        importDealersReportServletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp/upload"));
        handler.addServlet(importDealersReportServletHolder, ImportDealersReportServlet.URL);
    }
}
