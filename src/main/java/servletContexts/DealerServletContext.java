package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.dealer.ChangeParentServlet;
import servlets.dealer.DealerServlet;

public class DealerServletContext extends BaseServletContext {
    public DealerServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new ChangeParentServlet()), ChangeParentServlet.URL);
        handler.addServlet(new ServletHolder(new DealerServlet()), DealerServlet.URL);
    }
}
