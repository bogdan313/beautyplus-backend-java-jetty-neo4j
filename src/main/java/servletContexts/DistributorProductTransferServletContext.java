package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.distributorProductTransfer.AcceptServlet;
import servlets.distributorProductTransfer.DeclineServlet;
import servlets.distributorProductTransfer.DistributorProductTransferServlet;

public class DistributorProductTransferServletContext extends BaseServletContext {
    public DistributorProductTransferServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new DistributorProductTransferServlet()), DistributorProductTransferServlet.URL);
        handler.addServlet(new ServletHolder(new AcceptServlet()), AcceptServlet.URL);
        handler.addServlet(new ServletHolder(new DeclineServlet()), DeclineServlet.URL);
    }
}
