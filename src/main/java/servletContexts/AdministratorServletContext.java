package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.administrator.GenerateAdminServlet;

public class AdministratorServletContext extends BaseServletContext {
    public AdministratorServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new GenerateAdminServlet()), GenerateAdminServlet.URL);
    }
}
