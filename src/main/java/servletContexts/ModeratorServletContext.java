package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.moderator.ModeratorServlet;

public class ModeratorServletContext extends BaseServletContext {
    public ModeratorServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new ModeratorServlet()), ModeratorServlet.URL);
    }
}
