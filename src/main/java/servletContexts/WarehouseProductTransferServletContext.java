package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.warehouseProductTransfer.AcceptServlet;
import servlets.warehouseProductTransfer.DeclineServlet;
import servlets.warehouseProductTransfer.WarehouseProductTransferServlet;

public class WarehouseProductTransferServletContext extends BaseServletContext {
    public WarehouseProductTransferServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new WarehouseProductTransferServlet()), WarehouseProductTransferServlet.URL);
        handler.addServlet(new ServletHolder(new AcceptServlet()), AcceptServlet.URL);
        handler.addServlet(new ServletHolder(new DeclineServlet()), DeclineServlet.URL);
    }
}
