package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.auth.CheckUserByCodeServlet;
import servlets.auth.CheckUserByLoginServlet;
import servlets.auth.LogOutServlet;
import servlets.auth.SignInServlet;

public class AuthServletContext extends BaseServletContext {
    public AuthServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new CheckUserByCodeServlet()), CheckUserByCodeServlet.URL);
        handler.addServlet(new ServletHolder(new CheckUserByLoginServlet()), CheckUserByLoginServlet.URL);
        handler.addServlet(new ServletHolder(new LogOutServlet()), LogOutServlet.URL);
        handler.addServlet(new ServletHolder(new SignInServlet()), SignInServlet.URL);
    }
}
