package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.report.*;

public class ReportServletContext extends BaseServletContext {
    public ReportServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new DealerBonusesServlet()), DealerBonusesServlet.URL);
        handler.addServlet(new ServletHolder(new DealerProductOrderServlet()), DealerProductOrderServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorBonusesServlet()), DistributorBonusesServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorProductOrderServlet()), DistributorProductOrderServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorProductRemainderServlet()), DistributorProductRemainderServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorProductTransferServlet()), DistributorProductTransferServlet.URL);
        handler.addServlet(new ServletHolder(new IncomingToWarehouseServlet()), IncomingToWarehouseServlet.URL);
        handler.addServlet(new ServletHolder(new WarehouseProductTransferServlet()), WarehouseProductTransferServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorMoneyServlet()), DistributorMoneyServlet.URL);
        handler.addServlet(new ServletHolder(new GiftServlet()), GiftServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorTotalMoneyServlet()), DistributorTotalMoneyServlet.URL);
    }
}
