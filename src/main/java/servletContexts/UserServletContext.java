package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.user.*;

public class UserServletContext extends BaseServletContext {
    public UserServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new UserProfileServlet()), UserProfileServlet.URL);
        handler.addServlet(new ServletHolder(new UserOnlineServlet()), UserOnlineServlet.URL);
        handler.addServlet(new ServletHolder(new UserAvatarServlet()), UserAvatarServlet.URL);
    }
}
