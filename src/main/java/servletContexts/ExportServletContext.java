package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.export.DistributorMoneyServlet;
import servlets.export.DistributorWarehouseRemainderServlet;
import servlets.export.WarehouseProductTransferServlet;

public class ExportServletContext extends BaseServletContext {
    public ExportServletContext(String baseUrl) {
        super(baseUrl);
    }

    @Override
    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new WarehouseProductTransferServlet()), WarehouseProductTransferServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorMoneyServlet()), DistributorMoneyServlet.URL);
        handler.addServlet(new ServletHolder(new DistributorWarehouseRemainderServlet()), DistributorWarehouseRemainderServlet.URL);
    }
}
