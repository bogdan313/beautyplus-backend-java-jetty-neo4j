package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.distributorProductOrder.AcceptServlet;
import servlets.distributorProductOrder.DeclineServlet;
import servlets.distributorProductOrder.DistributorProductOrderServlet;

public class DistributorProductOrderServletContext extends BaseServletContext {
    public DistributorProductOrderServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new DistributorProductOrderServlet()), DistributorProductOrderServlet.URL);
        handler.addServlet(new ServletHolder(new AcceptServlet()), AcceptServlet.URL);
        handler.addServlet(new ServletHolder(new DeclineServlet()), DeclineServlet.URL);
    }
}
