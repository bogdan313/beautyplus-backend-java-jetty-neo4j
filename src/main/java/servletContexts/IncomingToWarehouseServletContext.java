package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.incomingToWarehouse.IncomingToWarehouseServlet;

public class IncomingToWarehouseServletContext extends BaseServletContext {
    public IncomingToWarehouseServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new IncomingToWarehouseServlet()),
                IncomingToWarehouseServlet.URL);
    }
}
