package servletContexts;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.dealerProductOrder.AcceptServlet;
import servlets.dealerProductOrder.DealerProductOrderServlet;
import servlets.dealerProductOrder.DeclineServlet;

public class DealerProductOrderServletContext extends BaseServletContext {
    public DealerProductOrderServletContext(String baseUrl) {
        super(baseUrl);
    }

    void addServlets() {
        ServletContextHandler handler = this.getHandler();
        handler.addServlet(new ServletHolder(new DealerProductOrderServlet()), DealerProductOrderServlet.URL);
        handler.addServlet(new ServletHolder(new AcceptServlet()), AcceptServlet.URL);
        handler.addServlet(new ServletHolder(new DeclineServlet()), DeclineServlet.URL);
    }
}
