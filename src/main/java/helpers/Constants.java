package helpers;

import java.util.ArrayList;

public class Constants {
    public static final String CONTENT_TYPE_JSON = "application/json;charset=utf8";
    public static final String ALLOW_READ = "Allow read";
    public static final String ALLOW_ACCEPT = "Allow accept";
    public static final int ELEMENTS_PER_PAGE = 10;
    public static final int BONUS_PRICE = 5300;
    public static final ArrayList<String> MONTHS = new ArrayList<String>() {{
        add("January");
        add("February");
        add("March");
        add("April");
        add("May");
        add("June");
        add("July");
        add("August");
        add("September");
        add("October");
        add("November");
        add("December");
    }};

    public enum STATUSES {
        WAITING("Waiting"),
        ACCEPTED("Accepted"),
        DECLINED("Declined"),
        ACTIVE("Active"),
        DISABLED("Disabled"),
        CLOSED("Closed"),
        CANCELLED("Cancelled");

        private final String value;

        STATUSES(String status) {
            this.value = status;
        }

        public String getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }

        public static boolean isInShortList(String status) {
            return status.equals(STATUSES.WAITING.getValue()) ||
                    status.equals(STATUSES.ACCEPTED.getValue()) ||
                    status.equals(STATUSES.DECLINED.getValue());
        }
    }

    public enum USER_STATUS {
        ADMINISTRATOR("administrator"),
        MODERATOR("moderator"),
        WAREHOUSE("warehouse"),
        DISTRIBUTOR("distributor"),
        DEALER_1("Dealer1"),
        DEALER_2("Dealer2"),
        DEALER_MANAGER("Manager"),
        DEALER_SILVER_MANAGER("Silver manager"),
        DEALER_GOLD_MANAGER("Gold manager"),
        DEALER_DIAMOND_MANAGER("Diamond manager"),
        DEALER_DIRECTOR("Director"),
        DEALER_SILVER_DIRECTOR("Silver director"),
        DEALER_GOLD_DIRECTOR("Gold director"),
        DEALER_SAPPHIRE_DIRECTOR("Sapphire director"),
        DEALER_DIAMOND_DIRECTOR("Diamond director");

        private final String value;

        USER_STATUS(String status) {
            this.value = status;
        }

        public String getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }
    }
}
