package helpers;

import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

public class Increment {
    private static Increment ourInstance = new Increment();
    private final AtomicInteger dealerProductOrders;
    private final AtomicInteger distributorProductOrders;
    private final AtomicInteger distributorProductTransfers;
    private final AtomicInteger warehouseProductTransfers;
    private final AtomicInteger dealerCode;
    private final AtomicInteger distributorCode;
    private final AtomicInteger warehouseCode;

    public static Increment getInstance() {
        return ourInstance;
    }

    private Increment() {
        int dealerProductOrdersStart = 0,
                distributorProductOrdersStart = 0,
                distributorProductTransfersStart = 0,
                warehouseProductTransfersStart = 0,
                dealerCode = 0,
                distributorCode = 0,
                warehouseCode = 0;

        try (InputStream initCollections = this.getClass().getClassLoader().getResourceAsStream("collections.properties")) {
            Properties initCollectionsProperties = new Properties();
            initCollectionsProperties.load(initCollections);

            dealerProductOrdersStart = Integer.parseInt(initCollectionsProperties.getProperty("dealer_product_order", "0"));
            distributorProductOrdersStart = Integer.parseInt(initCollectionsProperties.getProperty("distributor_product_order", "0"));
            distributorProductTransfersStart = Integer.parseInt(initCollectionsProperties.getProperty("distributor_product_transfer", "0"));
            warehouseProductTransfersStart = Integer.parseInt(initCollectionsProperties.getProperty("warehouse_product_transfer", "0"));
            dealerCode = Integer.parseInt(initCollectionsProperties.getProperty("dealer_code", "0"));
            distributorCode = Integer.parseInt(initCollectionsProperties.getProperty("distributor_code", "0"));
            warehouseCode = Integer.parseInt(initCollectionsProperties.getProperty("warehouse_code", "0"));
        }
        catch (Exception e) {}
        finally {
            this.dealerProductOrders = new AtomicInteger(dealerProductOrdersStart);
            this.distributorProductOrders = new AtomicInteger(distributorProductOrdersStart);
            this.distributorProductTransfers = new AtomicInteger(distributorProductTransfersStart);
            this.warehouseProductTransfers = new AtomicInteger(warehouseProductTransfersStart);
            this.dealerCode = new AtomicInteger(dealerCode);
            this.distributorCode = new AtomicInteger(distributorCode);
            this.warehouseCode = new AtomicInteger(warehouseCode);
        }
    }

    public AtomicInteger getDealerProductOrders() {
        return this.dealerProductOrders;
    }

    public AtomicInteger getDistributorProductOrders() {
        return this.distributorProductOrders;
    }

    public AtomicInteger getDistributorProductTransfers() {
        return this.distributorProductTransfers;
    }

    public AtomicInteger getWarehouseProductTransfers() {
        return this.warehouseProductTransfers;
    }

    public AtomicInteger getDealerCode() {
        return dealerCode;
    }

    public AtomicInteger getDistributorCode() {
        return distributorCode;
    }

    public AtomicInteger getWarehouseCode() {
        return warehouseCode;
    }
}
