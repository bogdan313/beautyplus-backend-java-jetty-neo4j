package helpers;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Map;

public class ReportExcelHelper {
    public static void addPersons(Sheet sheet, Map<String, Map<String, Long>> persons,
                                  String dateStart, String dateEnd) {
        int rowNum = 0;
        int cellNum = 0;

        Row headerRow = sheet.createRow(rowNum++);

        Cell titleCell = headerRow.createCell(cellNum++);
        titleCell.setCellType(CellType.STRING);
        titleCell.setCellValue("Человек");

        Cell dateStartCell = headerRow.createCell(cellNum++);
        dateStartCell.setCellType(CellType.STRING);
        dateStartCell.setCellValue(dateStart);

        Cell dateEndCell = headerRow.createCell(cellNum);
        dateEndCell.setCellType(CellType.STRING);
        dateEndCell.setCellValue(dateEnd);

        cellNum = 0;
        Row titleRow = sheet.createRow(rowNum++);

        Cell personFormatted = titleRow.createCell(cellNum++);
        personFormatted.setCellType(CellType.STRING);
        personFormatted.setCellValue("Код и Ф.И.О.");

        Cell waitingLabel = titleRow.createCell(cellNum++);
        waitingLabel.setCellType(CellType.STRING);
        waitingLabel.setCellValue(Constants.STATUSES.WAITING.getValue());

        Cell acceptedLabel = titleRow.createCell(cellNum++);
        acceptedLabel.setCellType(CellType.STRING);
        acceptedLabel.setCellValue(Constants.STATUSES.ACCEPTED.getValue());

        Cell declinedLabel = titleRow.createCell(cellNum);
        declinedLabel.setCellType(CellType.STRING);
        declinedLabel.setCellValue(Constants.STATUSES.DECLINED.getValue());

        for (Map.Entry<String, Map<String, Long>> entry : persons.entrySet()) {
            cellNum = 0;
            Row currentRow = sheet.createRow(rowNum++);

            Cell personCell = currentRow.createCell(cellNum++);
            personCell.setCellType(CellType.STRING);
            personCell.setCellValue(entry.getKey());

            Cell waitingCell = currentRow.createCell(cellNum++);
            waitingCell.setCellType(CellType.NUMERIC);
            waitingCell.setCellValue(entry.getValue().getOrDefault(Constants.STATUSES.WAITING.getValue(), 0L));

            Cell acceptedCell = currentRow.createCell(cellNum++);
            acceptedCell.setCellType(CellType.NUMERIC);
            acceptedCell.setCellValue(entry.getValue().getOrDefault(Constants.STATUSES.ACCEPTED.getValue(), 0L));

            Cell declinedCell = currentRow.createCell(cellNum);
            declinedCell.setCellType(CellType.NUMERIC);
            declinedCell.setCellValue(entry.getValue().getOrDefault(Constants.STATUSES.DECLINED.getValue(), 0L));
        }
    }

    public static void addProducts(Sheet sheet, Map<String, Map<String, Double>> products,
                                  String dateStart, String dateEnd) {
        int rowNum = 0;
        int cellNum = 0;

        Row headerRow = sheet.createRow(rowNum++);

        Cell titleCell = headerRow.createCell(cellNum++);
        titleCell.setCellType(CellType.STRING);
        titleCell.setCellValue("Товар");

        Cell dateStartCell = headerRow.createCell(cellNum++);
        dateStartCell.setCellType(CellType.STRING);
        dateStartCell.setCellValue(dateStart);

        Cell dateEndCell = headerRow.createCell(cellNum);
        dateEndCell.setCellType(CellType.STRING);
        dateEndCell.setCellValue(dateEnd);

        cellNum = 0;
        Row titleRow = sheet.createRow(rowNum++);

        Cell productFormatted = titleRow.createCell(cellNum++);
        productFormatted.setCellType(CellType.STRING);
        productFormatted.setCellValue("Код товара и наименование");

        Cell quantityCell = titleRow.createCell(cellNum++);
        quantityCell.setCellType(CellType.STRING);
        quantityCell.setCellValue("Количество");

        Cell priceCell = titleRow.createCell(cellNum);
        priceCell.setCellType(CellType.STRING);
        priceCell.setCellValue("Сумма");

        for (Map.Entry<String, Map<String, Double>> entry : products.entrySet()) {
            cellNum = 0;
            Row currentRow = sheet.createRow(rowNum++);

            Cell productCell = currentRow.createCell(cellNum++);
            productCell.setCellType(CellType.STRING);
            productCell.setCellValue(entry.getKey());

            Cell tmpQuantityCell = currentRow.createCell(cellNum++);
            tmpQuantityCell.setCellType(CellType.NUMERIC);
            tmpQuantityCell.setCellValue(entry.getValue().getOrDefault("quantity", 0.));

            Cell tmpPriceCell = currentRow.createCell(cellNum);
            tmpPriceCell.setCellType(CellType.NUMERIC);
            tmpPriceCell.setCellValue(entry.getValue().getOrDefault("price", 0.));
        }
    }

    public static void addInOutProducts(Sheet sheet, Map<String, Map<String, Double>> products,
                                  String dateStart, String dateEnd) {
        int rowNum = 0;
        int cellNum = 0;

        Row headerRow = sheet.createRow(rowNum++);

        Cell titleCell = headerRow.createCell(cellNum++);
        titleCell.setCellType(CellType.STRING);
        titleCell.setCellValue("Товар");

        Cell dateStartCell = headerRow.createCell(cellNum++);
        dateStartCell.setCellType(CellType.STRING);
        dateStartCell.setCellValue(dateStart);

        Cell dateEndCell = headerRow.createCell(cellNum);
        dateEndCell.setCellType(CellType.STRING);
        dateEndCell.setCellValue(dateEnd);

        cellNum = 0;
        Row titleRow = sheet.createRow(rowNum++);

        Cell productFormatted = titleRow.createCell(cellNum++);
        productFormatted.setCellType(CellType.STRING);
        productFormatted.setCellValue("Код товара и наименование");

        Cell incomingCell = titleRow.createCell(cellNum++);
        incomingCell.setCellType(CellType.STRING);
        incomingCell.setCellValue("Входящие");

        Cell outgoingCell = titleRow.createCell(cellNum);
        outgoingCell.setCellType(CellType.STRING);
        outgoingCell.setCellValue("Исходящие");

        for (Map.Entry<String, Map<String, Double>> entry : products.entrySet()) {
            cellNum = 0;
            Row currentRow = sheet.createRow(rowNum++);

            Cell productCell = currentRow.createCell(cellNum++);
            productCell.setCellType(CellType.STRING);
            productCell.setCellValue(entry.getKey());

            Cell tmpIncomingCell = currentRow.createCell(cellNum++);
            tmpIncomingCell.setCellType(CellType.NUMERIC);
            tmpIncomingCell.setCellValue(entry.getValue().getOrDefault("incoming", 0.));

            Cell tmpOutgoingCell = currentRow.createCell(cellNum);
            tmpOutgoingCell.setCellType(CellType.NUMERIC);
            tmpOutgoingCell.setCellValue(entry.getValue().getOrDefault("outgoing", 0.));
        }
    }
}
