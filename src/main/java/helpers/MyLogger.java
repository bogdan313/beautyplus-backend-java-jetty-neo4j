package helpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MyLogger {
    public static void log(String message) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(String.format("%s: %s",
                dateTimeFormatter.format(now), message));
    }
}
